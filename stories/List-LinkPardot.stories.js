import { createListLinkPardot } from './List-LinkPardot';

export default {
  title: 'Pardot Tags/List',
};

const Template = () => createListLinkPardot();

export const ListLinkComponent = Template.bind({});
