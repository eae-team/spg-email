import {createHighlight} from './Highlight';

export default {
  title: 'Blocks/Highlight',
};

const Template = () => createHighlight();

export const HighlightComponent = Template.bind({});
