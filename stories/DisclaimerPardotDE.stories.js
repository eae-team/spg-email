import { createDisclaimerPardotDE } from './DisclaimerPardotDE';

export default {
  title: 'Pardot Tags/Disclaimer/Disclaimer DE',
};

const Template = () => createDisclaimerPardotDE();

export const DisclaimerComponent = Template.bind({});