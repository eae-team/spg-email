import {createFullCTADarkBluePardot} from './FullCTADarkBluePardot';

export default {
  title: 'Pardot Tags/Full CTA/Dark blue',
};

const Template = () => createFullCTADarkBluePardot();

export const FullCTAComponent = Template.bind({});
