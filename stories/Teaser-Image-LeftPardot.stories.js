import { createTeaserLeftPardot } from './Teaser-Image-LeftPardot';

export default {
  title: 'Pardot Tags/Teaser',
};

const Template = () => createTeaserLeftPardot();

export const TeaserLeftComponent = Template.bind({});

