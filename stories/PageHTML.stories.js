import { createPageHTML } from './PageHTML';

export default {
  title: 'Pages/Page HTML',
};

const Template = () => createPageHTML();

export const PageComponent = Template.bind({});

