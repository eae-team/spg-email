import {createHeroInfo} from './HeroInfo';

export default {
  title: 'Blocks/Hero/Info',
};

const Template = () => createHeroInfo();

export const HeroComponent = Template.bind({});

