import './main.css';

import {createSubHeadingGrayPardot} from './SubHeadingGrayPardot';

export const createSpeakerPardot = () => {
    const subHeadingGray = createSubHeadingGrayPardot();
    const header = `
    ${subHeadingGray}
    <!-- SPEAKER -->
<table
        cellpadding="0"
        cellspacing="0"
        align="center"
        border="0"
        width="100%"
        pardot-repeatable=""
        style="max-width: 640px; width: 100%; margin: auto; background-color: #FFFFFF;"
        class="speaker wrapper"
      >
    <tr>
        <td valign="top" style="padding: 20px; vertical-align: top;" width="107">
            <img
                    valign="top"
                    src="https://res.cloudinary.com/eae-pt/image/upload/v1625576775/speaker_q7vx7g.png"
                    style="
                      width: 107px;
                      height: 107px;
                      max-width: 100%;
                      margin: auto;
                      display: block;
                      -webkit-border-radius: 50%; 
                      -moz-border-radius: 50%; 
                      border-radius: 50%;
                    "
                  />
        </td>
        <td valign="top" style="padding: 20px; vertical-align: top;">
            <h2 pardot-region="" style="
                    font-size: 18px;
                    line-height: 21px;
                    font-family: Arial, Helvetica, sans-serif;
                    font-weight: bold;
                    margin: 0;
                    color: #005091;
                  ">Stephen Breslin</h2>
            <p pardot-region="" style="
                    font-size: 14px;
                    line-height: 18px;
                    font-family: Arial, Helvetica, sans-serif;
                    font-weight: bold;
                    margin: 0;
                    margin-top: 3px;
                    color: #019CDB;
                  "
                  class="speaker__sub-title">Website & Digital Manager</p>
            <p pardot-region="" style="
                    font-size: 16px;
                    line-height: 22px;
                    font-family: Arial, Helvetica, sans-serif;
                    margin: 0;
                    color: #485B7B;
                  ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam iaculis porttitor auctor. Praesent ornare feugiat auctor.</p>
            <a pardot-region=""
                href="https://www.southpole.com/"
                style="
                  font-size: 16px;
                  font-weight: bold;
                  color: #005091;
                  text-decoration: none;
                  font-family: Arial, Helvetica, sans-serif;
                  margin-top: 5px;
                  display: block;
                "
            >
                See Contact Here
            </a>
                  
        </td>
    </tr>
</table>
<!-- END SPEAKER -->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->


`;

    return header;
};
