import './main.css';

import {createSubHeadingPardot} from './SubHeadingPardot';

export const createPartnersPardot = () => {
    const subHeading = createSubHeadingPardot();
    const partnersBlock = `
    ${subHeading}
<!-- PARTNERS BLOCK-->
<table cellpadding="20"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #FFFFFF;"
       class="wrapper text-block"
       pardot-repeatable=""
       align="center">
    
    <tr>      
        <td>
            <!-- PARTNER ITEM-->
            <table cellpadding="15"
               cellspacing="0"
               border="0"
               style="max-width: 640px; width: 100%; background: #98C21F;"
               class="partner-item"
               pardot-repeatable=""
               align="center">
        
               <tr>
      
                    <td width="117" align="center">
                        <img
                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625751217/natural_carton_fzsusu.png"
                            width="117"
                            height="117"
                            pardot-region=""
                            alt=""
                            border="0"
                            style="display: inline-block"
                            class=""
                        />
                    
                    </td> 
                    <td valign="top">
                        <p pardot-region="" style="
                            margin: 0;
                            color: #1E355C;
                            font-size: 14px;
                            line-height: 19px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                     
                        >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Phasellus maximus sem at tortor venenatis, a dictum arcu facilisis. 
                        Suspendisse magna dolor, condimentum vel semper ac, efficitur eget justo. 
                        Duis in Leo a nunc fringilla elementum. 
                        </p>
                    </td> 
                             
               </tr>
            </table>
            <!-- space -->
            <table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
                <tr>
                    <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                </tr>
            </table>
            <!-- END space -->
            
            <!-- PARTNER ITEM-->
            <table cellpadding="15"
               cellspacing="0"
               border="0"
               style="max-width: 640px; width: 100%; background: #F39200;"
               class="partner-item"
               pardot-repeatable=""
               align="center">
        
               <tr>
      
                    <td width="117" align="center">
                        <img
                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625751217/natural_carton_fzsusu.png"
                            width="117"
                            height="117"
                            pardot-region=""
                            alt=""
                            border="0"
                            style="display: inline-block"
                            class=""
                        />
                    
                    </td> 
                    <td valign="top">
                        <p pardot-region="" style="
                            margin: 0;
                            color: #1E355C;
                            font-size: 14px;
                            line-height: 19px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                    
                        >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Phasellus maximus sem at tortor venenatis, a dictum arcu facilisis. 
                        Suspendisse magna dolor, condimentum vel semper ac, efficitur eget justo. 
                        Duis in Leo a nunc fringilla elementum. 
                        </p>
                    </td> 
                             
               </tr>
            </table>
            <!-- space -->
            <table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
                <tr>
                    <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                </tr>
            </table>
            <!-- END space -->
            
            <!-- PARTNER ITEM-->
            <table cellpadding="15"
               cellspacing="0"
               border="0"
               style="max-width: 640px; width: 100%; background: #FDC300;"
               class="partner-item"
               pardot-repeatable=""
               align="center">
        
               <tr>
      
                    <td width="117" align="center">
                        <img
                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625751217/natural_carton_fzsusu.png"
                            width="117"
                            height="117"
                            pardot-region=""
                            alt=""
                            border="0"
                            style="display: inline-block"
                            class=""
                        />
                    
                    </td> 
                    <td valign="top">
                        <p pardot-region="" style="
                            margin: 0;
                            color: #1E355C;
                            font-size: 14px;
                            line-height: 19px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                          
                        >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Phasellus maximus sem at tortor venenatis, a dictum arcu facilisis. 
                        Suspendisse magna dolor, condimentum vel semper ac, efficitur eget justo. 
                        Duis in Leo a nunc fringilla elementum. 
                        </p>
                    </td> 
                             
               </tr>
            </table>
            <!-- space -->
            <table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
                <tr>
                    <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                </tr>
            </table>
            <!-- END space -->
            
        </td>
        
    
    </tr>

</table>
<!-- END PARTNERS BLOCK-->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
`;

    return partnersBlock
}
