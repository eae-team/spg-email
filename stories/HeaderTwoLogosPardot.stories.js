import { createHeaderTwoLogosPardot } from './HeaderTwoLogosPardot';

export default {
  title: 'Pardot Tags/Header/Two Logos',
};

const Template = () => createHeaderTwoLogosPardot();

export const HeaderComponent = Template.bind({});

