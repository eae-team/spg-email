import {createHeroInfoPardot} from './HeroInfoPardot';

export default {
  title: 'Pardot Tags/Hero/Info',
};

const Template = () => createHeroInfoPardot();

export const HeroComponent = Template.bind({});

