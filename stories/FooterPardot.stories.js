import { createFooterPardot } from './FooterPardot';

export default {
  title: 'Pardot Tags/Footer',
};

const Template = () => createFooterPardot();

export const FooterComponent = Template.bind({});

