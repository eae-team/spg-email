import { createFooterPardotDE } from './FooterPardotDE';

export default {
  title: 'Pardot Tags/Footer/Footer DE',
};

const Template = () => createFooterPardotDE();

export const FooterComponent = Template.bind({});
