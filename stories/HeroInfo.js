import './main.css';

export const createHeroInfo = () => {
    const header = `
    <!-- HERO INFO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                          max-width: 640px;
                          width: 100%;
                          margin: auto;
                          text-align: center;
                        "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr>
                                <td
                                    background="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621857015/logo_dbsewx_d7b0sc.jpg"
                                    width="640"
                                    height="355"
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                    
                                    <!--[if mso]>
    
                            <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 640px; height:355px;" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621857015/logo_dbsewx_d7b0sc.jpg" />                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 640px; height:355px;">
                            <v:fill  opacity="0%" color="#005091"  />
                            <v:textbox inset="0,0,0,0">
                            <![endif]-->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="center"
                                            width="100%"
                                            style="max-width: 500px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="center"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="center"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                      text-align: center;
                                      padding: 78px 40px 0;
                                      text-align: center;
                                    "
                                                                        class="header__title"
                                                                    >
                                                                        <h1
                                                                            style="
                                        margin: 0;
                                        font-size: 40px;
                                        line-height: 42px;
                                        color: #ffffff;
                                        font-weight: bold;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                                                                        >
                                                                            How can
                                                                            we
                                                                            support?
                                                                        </h1>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                      text-align: center;
                                      padding: 5px 60px 77px 60px;
                                      font-size: 22px;
                                      line-height: 28px;
                                      color: #ffffff;
                                      font-family: Arial, Helvetica,
                                        sans-serif;
                                      text-align: center;
                                    "
                                                                        class="header__text"
                                                                    >
                                                                        <div
                                                                            style="display: inline-block; padding-bottom: 22px;"
                                                                        >
                                                                            <p
                                                                                style="margin: 0; padding-top: 15px; padding-bottom: 3px;"
                                                                            >
                                                                                Your
                                                                                climate
                                                                                journey?
                                                                            </p>
                                                                            <!--[if !mso]>-->
                                                                            <div
                                                                                style="height: 2px; display: block; background-color: #bcc2cb"
                                                                            ></div>
                                                                            <!--<![endif]-->
                                                                        </div>
                                                                        <br />
                                                                        <!--[if !mso]>-->
                                                                        <a
                                                                            href="http://"
                                                                            style="background-color:#FFFFFF;
                                                           border-radius:25px;color:#005091;
                                                           display:inline-block;font-family:sans-serif;
                                                           font-size:13px;font-weight:bold;line-height:40px;
                                                           text-align:center;text-decoration:none;width:175px;
                                                           -webkit-text-size-adjust:none;"
                                                                        >
                                                                            Register
                                                                            Today
                                                                            &gt;
                                                                        </a>
                                                                        <!--<![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </v:textbox>
                            </v:fill>
                            </v:rect>
                            </v:image>
                             <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="https://www.google.com.au" style="height:40px;width:175px;position:absolute;top:185px;left:232px;" arcsize="63%" fillcolor="#FFFFFF" stroke="false">
                            <center style="color:#005091;-webkit-text-size-adjust:none;font-family:sans-serif;font-size:13px;font-weight:bold;">Register Today &gt;</center>
                            </v:roundrect>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table> 
<!-- END space -->
`;

    return header;
};
