import {createListBulletsIconPardot} from './List-Bullets-IconPardot';

export default {
  title: 'Pardot Tags/Bullets/Icon',
};

const Template = () => createListBulletsIconPardot();

export const ListBulletsComponent = Template.bind({});
