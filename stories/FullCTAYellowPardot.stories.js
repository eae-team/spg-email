import {createFullCTAYellowPardot} from './FullCTAYellowPardot';

export default {
  title: 'Pardot Tags/Full CTA/Yellow',
};

const Template = () => createFullCTAYellowPardot();

export const FullCTAComponent = Template.bind({});
