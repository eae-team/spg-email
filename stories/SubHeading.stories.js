import { createSubHeading } from './SubHeading';

export default {
  title: 'Blocks/Sub-Heading/Default',
};

const Template = () => createSubHeading();

export const SubHeadingComponent = Template.bind({});