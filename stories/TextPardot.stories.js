import {createIntro, createTextPardot} from './TextPardot';

export default {
  title: 'Pardot Tags/Text',
};

const Template = () => createTextPardot();

export const TextComponent = Template.bind({});
