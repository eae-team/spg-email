import {createEditorBlock} from './EditorBlock';

export default {
  title: 'Blocks/Editor Block',
};

const Template = () => createEditorBlock();

export const EditorBlockComponent = Template.bind({});
