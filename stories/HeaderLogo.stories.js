import { createHeaderLogo } from './HeaderLogo';

export default {
  title: 'Blocks/Header/Logo Only',
};

const Template = () => createHeaderLogo();

export const HeaderComponent = Template.bind({});

