import './main.css';

export const createTeaserFull = () => {
    const teaserFull = `
<!-- Projects -->
<table
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 640px; width: 100%; background: #ffffff;"
        class="teaser-image-full"
>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 10px; font-size: 10px; height: 10px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td>
            <h2
                    style="
              color: #fde9cc;
              font-size: 107px;
              line-height: 124px;
              font-family: Arial, Helvetica, sans-serif;
              margin: 0;
            "
                    class="projects-bigger-title"
            >
                Projects
            </h2>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 173px">&nbsp;</td>
                    <td style="background: #f39200; height: 20px; width: 427px" class="projects-horizontal-top-bar">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <img
                                src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/a4fe80ec-a3c1-5642-b13a-658b04fbfa79.png"
                                alt=""
                                style="display: block; width: 100%;"
                        />
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background: #f39200; height: 312px; width: 80px"
                                    class="projects-vertical-right-bar">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="width: 66px">&nbsp;</td>
                    <td style="background: #fcedd2; height: 20px; width: 427px" class="projects-horizontal-bottom-bar">
                        &nbsp;
                    </td>
                    <td style="width: 147px">&nbsp;</td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr class="space bg-table">
                    <td style="line-height: 40px; height: 40px">&nbsp;</td>
                </tr>
                <tr>
                    <td
                            style="
                    color: #019CDB;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 13px;
                    line-height: 18px;
                    font-weight: bold;
                  "
                  class="projects__sub-title"
                    >
                        Carbon Credits | Climate Risks & Opportunities
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 10px; height: 10px; font-size: 10px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td
                            style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 34px;
                    line-height: 32px;
                    font-weight: bold;
                  "
                   class="projects__title"
                    >
                        Saving forests, protecting wildlife, and changing lives
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 10px; height: 10px; font-size: 10px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td
                            style="
                    color: #485B7B;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 18px;
                    line-height: 25px;
                  "
                  class="projects__description"
                    >
                        Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit
                        amet massa varius fringilla. Duis metus ipsum, ullamcorper non consectetur et, euismod a leo.
                        Mauris volutpat urna ut
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 20px; height: 20px; font-size: 20px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <a
                                href="http://"
                                target="_blank"
                                rel="noopener noreferrer"
                                style="
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      font-size: 18px;
                      line-height: 20px;
                      font-weight: bold;
                      text-decoration: none !important;
                    "
                    class="projects__link"
                        >
                            Continue >
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 20px; height: 20px; font-size: 20px">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END Projects -->
<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
  `;

    return teaserFull;
};
