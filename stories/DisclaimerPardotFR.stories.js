import { createDisclaimerPardotFR } from './DisclaimerPardotFR';

export default {
  title: 'Pardot Tags/Disclaimer/Disclaimer FR',
};

const Template = () => createDisclaimerPardotFR();

export const DisclaimerComponent = Template.bind({});