import {createHeaderViewOnBrowser} from './HeaderViewOnBrowser';
import {createHeaderTwoLogos} from './HeaderTwoLogos';
import {createHero} from './Hero';
import {createListBulletsIcon} from './List-Bullets-Icon';
import {createFullCTADarkBlue} from './FullCTADarkBlue';
import {createFooter} from './Footer';
import {createDisclaimer} from './Disclaimer';


export const createPageTopic = () => {
    const viewOnBrowser = createHeaderViewOnBrowser();
    const header = createHeaderTwoLogos();
    const hero = createHero();
    const bullets = createListBulletsIcon();
    const fullCTADarkBlue = createFullCTADarkBlue();
    const footer = createFooter();
    const disclaimer = createDisclaimer();


    const section = `  
                    ${viewOnBrowser}
                    ${header}
                    ${hero}               
                    ${bullets}
                    ${fullCTADarkBlue}
                    ${footer}
                    ${disclaimer}
                


`;

    return section;
};
