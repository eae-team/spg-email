import {createHeaderViewOnBrowser} from './HeaderViewOnBrowser';
import {createHeaderTwoLogos} from './HeaderTwoLogos';
import {createHero} from './Hero';
import {createMixMedia} from './MixMedia';
import {createSpeaker} from './Speaker';
import {createFullCTADarkBlue} from './FullCTADarkBlue';
import {createFooter} from './Footer';
import {createDisclaimer} from './Disclaimer';


export const createPageWebinar = () => {
    const viewOnBrowser = createHeaderViewOnBrowser();
    const header = createHeaderTwoLogos();
    const hero = createHero();
    const mixMedia = createMixMedia();
    const speaker = createSpeaker();
    const fullCTADarkBlue = createFullCTADarkBlue();
    const footer = createFooter();
    const disclaimer = createDisclaimer();


    const section = `  
                    ${viewOnBrowser}
                    ${header}
                    ${hero}               
                    ${mixMedia}
                    ${speaker}
                    ${fullCTADarkBlue}
                    ${footer}
                    ${disclaimer}
                


`;

    return section;
};
