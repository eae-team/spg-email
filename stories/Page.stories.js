import { createPage } from './Page';

export default {
  title: 'Pages/Page',
};

const Template = () => createPage();

export const PageComponent = Template.bind({});

