import { createPageTopic } from './PageTopic';

export default {
  title: 'Pages/Topic 1',
};

const Template = () => createPageTopic();

export const PageComponent = Template.bind({});

