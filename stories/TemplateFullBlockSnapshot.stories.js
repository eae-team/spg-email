import { createTemplateFullBlockSnapshot } from './TemplateFullBlockSnapshot';

export default {
  title: 'Pardot Templates/Template FullBlock Snapshot',
};

const Template = () => createTemplateFullBlockSnapshot();

export const templateFullBlockSnapshotComponent = Template.bind({});

