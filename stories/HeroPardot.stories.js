import {createHeroPardot} from './HeroPardot';

export default {
  title: 'Pardot Tags/Hero',
};

const Template = () => createHeroPardot();

export const HeroComponent = Template.bind({});

