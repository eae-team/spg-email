import { createTeaserFull } from './Teaser-Image-Full';

export default {
  title: 'Blocks/Teaser',
};

const Template = () => createTeaserFull();

export const TeaserFullComponent = Template.bind({});

