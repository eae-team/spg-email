import {createFullCTAGreen} from './FullCTAGreen';

export default {
  title: 'Blocks/Full CTA/Green',
};

const Template = () => createFullCTAGreen();

export const FullCTAComponent = Template.bind({});
