import './main.css';

export const createHeader = () => {
    const header = `

<!-- HEADER -->
<table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        align="center"
>
    <tr>
        <td style="padding: 18px">
            <table align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="border-right: 1px solid #e4e9f1; padding-right: 18px">
                        <a href="https://www.southpole.com/" target="_blank">
                            <img
                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621242940/logo_wbwxyx.png"
                                    width="197"
                                    height="40"
                                    alt=""
                                    border="0"
                                    style="display: block"
                                    class="logo"
                            />
                            <!--[if !mso]>-->
                            <img
                                    src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/ecd38bf1-c2d9-adfb-d953-2f6b697bd71e.png"
                                    width="197"
                                    height="40"
                                    alt=""
                                    border="0"
                                    style="display: none"
                                    class="logo-white"
                            />
                            <!--<![endif]-->
                        </a>
                    </td>
                    <td
                            style="
                  font-size: 12px;
                  font-family: Arial, Helvetica, sans-serif;
                  color: #005091;
                  padding-left: 18px;
                "
                            class="header-content"
                    >
                        <p
                                style="
                    font-size: 18px;
                    font-family: Arial, Helvetica, sans-serif;
                    font-weight: bold;
                    margin: 0;
                  "
                        >
                            South Pole Snapshot
                        </p>
                        March 2021
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END HEADER -->
`;

    return header;
};
