/*import {createHeaderViewOnBrowserPardot} from './HeaderViewOnBrowserPardot';
import {createHeaderLogoPardot} from './HeaderLogoPardot';
import {createLogosRowPardot} from './Logos-RowPardot';
import {createTextPardot} from './TextPardot';
import {createEditorBlockPardot} from './EditorBlockPardot';
import {createPartnersPardot} from './PartnersPardot';
import {createFooterPardotES} from './FooterPardotES';
import {createDisclaimerPardotES} from './DisclaimerPardotES';
import {createFooterPardotDE} from './FooterPardotDE';
import {createDisclaimerPardotDE} from './DisclaimerPardotDE';
import {createFooterPardotBG} from './FooterPardotBG';
import {createDisclaimerPardotBG} from './DisclaimerPardotBG';
import {createFooterPardotSW} from './FooterPardotSW';
import {createDisclaimerPardotSW} from './DisclaimerPardotSW';
import {createFooterPardotFR} from './FooterPardotFR';
import {createDisclaimerPardotFR} from './DisclaimerPardotFR';

export const createTemplatePressRelease = () => {
	const viewOnBrowser = createHeaderViewOnBrowserPardot();
    const headerLogONly = createHeaderLogoPardot();
	const LogosRow = createLogosRowPardot();
    const textPardot = createTextPardot();
	const editor = createEditorBlockPardot();
	const partner = createPartnersPardot();
    const footerSW = createFooterPardotSW();
    const disclaimerSW = createDisclaimerPardotSW();
    const footerDE = createFooterPardotDE();
    const disclaimerDE = createDisclaimerPardotDE();
    const footerES = createFooterPardotES();
    const disclaimerES = createDisclaimerPardotES();
    const footerFR = createFooterPardotFR();
    const disclaimerFR = createDisclaimerPardotFR();
    const footerBG = createFooterPardotBG();
    const disclaimerBG = createDisclaimerPardotBG();

	const templatePressRelease = `
		${viewOnBrowser}
		${headerLogONly}
		${LogosRow}
		${textPardot}
		${editor}
		${partner}
		${footerSW}
		${disclaimerSW}
		${footerDE}
		${disclaimerDE}
		${footerES}
		${disclaimerES}
		${footerFR}
		${disclaimerFR}
		${footerBG}
		${disclaimerBG}
	`;

  	return templatePressRelease;
}*/
export const createTemplatePressRelease = () => {
	const templatePressRelease = `
	<!-- VIEW IN BROWSER -->
	<table align="center" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td style="padding: 20px 0"><a href="{{View_Online}}" pardot-region="" style="
color: #005091;
font-size: 11px;
line-height: 14px;
font-family: Arial, Helvetica, sans-serif;
">View in browser </a></td>
			</tr>
		</tbody>
	</table>
	<!-- END VIEW IN BROWSER -->
<!-- HEADER LOGO -->

	<table align="center" border="0" cellpadding="0" cellspacing="0" class="header" pardot-removable="" style="max-width: 640px; width: 100%; background: #ffffff">
		<tbody>
			<tr>
				<td style="padding: 18px">
				<table align="center" border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td><a href="https://www.southpole.com/" target="_blank"><img alt="" border="0" class="logo" height="40" pardot-region="" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621242940/logo_wbwxyx.png" style="display: block;width: 100%;height: 100%;" width="197"> <!--[if !mso]>--> <img alt="" border="0" class="logo-white" height="40" pardot-region="" src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/ecd38bf1-c2d9-adfb-d953-2f6b697bd71e.png" style="display: none; width: 100%; height: 100%;" width="197"> <!--<![endif]--> </a></td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- END HEADER LOGO --><!-- space -->

	<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
		<tbody>
			<tr>
				<td style="line-height: 15px; font-size: 15px">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<!-- END space --><!-- LOGO ROW -->

	<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-removable="" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #f2f4f7">
		<tbody>
			<tr>
				<td>
				<table align="center" border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td pardot-region=""><a href="https://www.southpole.com/" target="_blank"><img alt="" border="0" height="26" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1637332161/Mask_Group_31_2x_lgrzfs.png" style="display: block" width="83"> </a></td>
							<td pardot-region="" style="padding-left: 30px;"><img alt="" border="0" height="26" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1637332161/ParawayLogo_2x_svucxy.png" style="display: block" width="83"></td>
							<td pardot-region="" style="padding-left: 30px;"><img alt="" border="0" height="42" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1637334198/corp-carbon_y8pbrb.png" style="display: block" width="86"></td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- END LOGO ROW --><!-- space -->

	<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
		<tbody>
			<tr>
				<td style="line-height: 15px; font-size: 15px">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<!-- END space --><!-- TEXT BLOCK-->

	<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper text-block" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff">
		<tbody>
			<tr>
				<td class="spacing" style="height: 40px">&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" style="max-width: 640px; width: 100%; background: #ffffff">
					<tbody>
						<tr>
							<td width="20">&nbsp;</td>
							<td pardot-region="">
							<p pardot-region="" style="
	margin: 0;
	color: #019CDB;
	font-size: 13px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
  ">FOR IMMEDIATE RELEASE</p>
							&nbsp;

							<h2 class="white__text" pardot-region="" style="
	margin: 0;
	color: #005091 ;
	font-size: 32px;
	font-weight: bold;
	line-height: 34px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: center;
  ">Pioneering ‘herd aggregation’ project allows smaller graziers to earn carbon</h2>
							&nbsp;

							<p class="white__text" pardot-region="" style="
	margin: 0;
	color: #005091;
	font-size: 20px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
  ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel enim quis enim interdum viverra ut ut augue. Donec consequat purus sed nunc.</p>
							&nbsp;

							<p class="white__text_90" pardot-region="" style="
	margin: 0;
	color: #485B7B;
	font-size: 18px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
  ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel enim quis enim interdum viverra ut ut augue. Donec consequat purus sed nunc.</p>
							<br>
							<!-- QUOTE BLOCK-->
							<table align="center" border="0" cellpadding="20" cellspacing="0" pardot-removable="" pardot-repeatable="" style="max-width: 640px; width: 100%; background-color: #005091">
								<tbody>
									<tr>
										<td align="center">
										<h2 pardot-region="" style="
color: #FFFFFF;
font-size: 18px;
line-height: 24px;
font-weight: bold;
margin: 0;
font-family: Arial, Helvetica, sans-serif;
text-align: center;
">The previous version of this media release erroneously used the term carbon grazing to refer to the breed cattle heard management method of carbon abatement.</h2>
										<br>
										<!--[if mso]>
<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
	 xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
	 style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" stroke="f"
	 fillcolor="#FFFFFF">
<w:anchorlock/>
<center>
<![endif]--><a href="http://" pardot-region="" style="background-color:#FFFFFF;
border-radius:25px;color:#005091;
display:inline-block;font-family:sans-serif;
font-size:13px;font-weight:bold;line-height:40px;
text-align:center;text-decoration:none;width:175px;
-webkit-text-size-adjust:none;">Tell us your Goal » </a> <!--[if mso]>
</center>
</v:roundrect>
<![endif]--></td>
									</tr>
								</tbody>
							</table>
							<!-- END QUOTE BLOCK--><!-- space -->

							<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
								<tbody>
									<tr>
										<td style="line-height: 20px; font-size: 20px">&nbsp;</td>
									</tr>
								</tbody>
							</table>
							<!-- END space -->

							<p class="white__text_90" pardot-region="" style="
	margin: 0;
	color: #485B7B;
	font-size: 16px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
  "><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Ut sagittis enim ut risus ultrices, ac <a href="#" style="color: #589CD7;"> consequat</a> quam</p>

							<p class="white__text_90" pardot-region="" style="
	margin: 0;
	color: #485B7B;
	font-size: 16px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
  "><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Morbi rutrum euismod ipsum ut ornare</p>

							<p class="white__text_90" pardot-region="" style="
	margin: 0;
	color: #485B7B;
	font-size: 16px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
  "><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Sed hendrerit elit, non placerat nisl consequat ac From commitment to</p>
							&nbsp;

							<p class="white__text_90" pardot-region="" style="
	margin: 0;
	color: #485B7B;
	font-size: 18px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
  ">Etiam auctor, tellus id malesuada aliquet, leo mauris varius diam, a porta massa dui efficitur augue. Praesent eget luctus elit. In semper dui et euismod fermentum. Vestibulum bibendum, magna sed eleifend dapibus, orci ipsum finibus magna, eu egestas nisl erat in nulla. Aliquam et vulputate felis, ut ullamcorper augue. Etiam feugiat nibh ultrices velit tristique ornare. Proin tristique nulla at lectus hendrerit lacinia. Morbi tristique nisl turpis, in scelerisque mauris consequat vitae.</p>
							&nbsp;

							<p class="white__text_90" pardot-region="" style="
	margin: 0;
	color: #485B7B;
	font-size: 18px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
  ">King Regards,</p>
							&nbsp;

							<p class="white__text_90" pardot-region="" style="
	margin: 0;
	color: #485B7B;
	font-size: 18px;
	font-weight: bold;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
  ">Isabel Hagbrink,<br>
							<span style="font-weight: normal; font-size: 14px;">Director of Communications</span></p>
							</td>
							<td width="20">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
			<tr>
				<td class="spacing" style="height: 40px">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<!-- END TEXT BLOCK--><!-- space -->

	<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
		<tbody>
			<tr>
				<td style="line-height: 20px; font-size: 20px">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<!-- END space --><!-- EDITOR BLOCK-->

	<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper text-block" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff">
		<tbody>
			<tr>
				<td class="spacing" style="height: 40px;">&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" style="max-width: 640px; width: 100%; background: #ffffff">
					<tbody>
						<tr>
							<td style="font-size: 40px" width="40">&nbsp;</td>
							<td>
							<h2 class="white__text" pardot-region="" style="
			color: #005091;
			font-size: 32px;
			font-weight: bold;
			margin: 0;
			font-family: Arial, Helvetica, sans-serif;
			text-align: center;
		">Notes to the editor on the beef cattle herd management method:</h2>
							&nbsp;

							<p class="white__text_90" pardot-region="" style="
		margin: 0;
		color: #485B7B;
		font-size: 16px;
		line-height: 24px;
		font-family: Arial, Helvetica, sans-serif;
		text-align: left;
	  ">Ut sagittis enim ut risus ultrices, ac consequat quam volutpat. Morbi rutrum euismod ipsum ut ornare. Sed vestibulum hendrerit elit, non placerat nisl consequat ac.</p>

							<p class="white__text_90" pardot-region="" style="
	margin: 0;
	color: #485B7B;
	font-size: 16px;
	line-height: 24px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: left;
	"><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Ut sagittis enim ut risus ultrices, ac <a href="#" style="color: #005091;"> consequat</a> quam</p>

							<p class="white__text_90" pardot-region="" style="
		margin: 0;
		color: #485B7B;
		font-size: 16px;
		line-height: 24px;
		font-family: Arial, Helvetica, sans-serif;
		text-align: left;
	  "><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Morbi rutrum euismod ipsum ut ornare</p>

							<p class="white__text_90" pardot-region="" style="
		margin: 0;
		color: #485B7B;
		font-size: 16px;
		line-height: 24px;
		font-family: Arial, Helvetica, sans-serif;
		text-align: left;
	  "><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Sed hendrerit elit, non placerat nisl consequat ac From commitment to</p>
							</td>
							<td style="font-size: 0px" width="40">&nbsp;</td>
						</tr>
						<tr>
							<td style="font-size: 40px; display: block" width="40">&nbsp;</td>
							<td style="background:none; border-bottom: 1px solid #e6e6e6; height:1px; width:100%; margin:0px 0px 0px 0px;">&nbsp;</td>
							<td style="font-size: 40px; display: block" width="40">&nbsp;</td>
						</tr>
						<tr>
							<td style="font-size: 40px; display: block" width="40">&nbsp;</td>
							<td>&nbsp;</td>
							<td style="font-size: 40px; display: block" width="40">&nbsp;</td>
						</tr>
						<tr>
							<td width="185">&nbsp;</td>
							<td align="center" width="270">
							<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" style="max-width: 270px; background: #ffffff; text-align: center;" width="270">
								<tbody>
									<tr>
										<td align="center"><img pardot-region="" src="https://res.cloudinary.com/eae-pt/image/upload/v1625576775/speaker_q7vx7g.png" style="
				  width: 107px;
				  max-width: 100%;
				  margin: auto;
				  display: block;
				  -webkit-border-radius: 50%;
				  -moz-border-radius: 50%;
				  border-radius: 50%;
				" valign="top">
										<h2 class="white__text" pardot-region="" style="
					color: #005091;
					font-size: 18px;
					font-weight: bold;
					margin: 0;
					font-family: Arial, Helvetica, sans-serif;
					text-align: center;
				">Stephen Breslin</h2>

										<p class="white__text_90" pardot-region="" style="
				margin: 0;
				color: #005091;
				font-size: 14px;
				line-height: 24px;
				font-family: Arial, Helvetica, sans-serif;
				text-align: center;
			  ">Website &amp; Digital Manager</p>
										&nbsp;

										<p class="white__text_90" pardot-region="" style="
				margin: 0;
				color: #005091;
				font-size: 12px;
				padding: 12px 0px;
				font-family: Arial, Helvetica, sans-serif;
				text-align: center;
				border-bottom: 1px solid #e6e6e6;
				border-top: 1px solid #e6e6e6;
			  ">s.breslin@southpole.com +353 87 7763132</p>
										&nbsp;

										<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" style="max-width: 210px; width: 210px; background: #ffffff; text-align: center;" width="108">
											<tbody>
												<tr>
													<td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/link_hh8l6d.png" style="display: inline-block" width="27"> </a></td>
													<td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/linkedin_qz7xrv.png" style="display: inline-block" width="27"> </a></td>
													<td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/instagram_vmbmdz.png" style="display: inline-block" width="27"> </a></td>
													<td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/facebook_cdpecf.png" style="display: inline-block" width="27"> </a></td>
													<td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/twitter_ej1rlf.png" style="display: inline-block" width="27"> </a></td>
												</tr>
											</tbody>
										</table>
										</td>
									</tr>
								</tbody>
							</table>
							</td>
							<td width="185">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
			<tr>
				<td class="spacing" style="height: 40px;">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<!-- END EDITOR BLOCK--><!-- space -->

	<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
		<tbody>
			<tr>
				<td style="line-height: 20px; font-size: 20px">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<!-- END space --><!-- SUB-HEADING -->

	<table align="center" border="0" cellpadding="20" cellspacing="0" class="wrapper text-block" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #FFFFFF;">
		<tbody>
			<tr>
				<td style="padding: 20px 0 0;">
				<h2 class="white__text" pardot-region="" style="
color: #005091;
font-size: 32px;
font-weight: bold;
margin: 0;
font-family: Arial, Helvetica, sans-serif;
text-align: center;
padding-top: 12px;
">About the Brands</h2>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- PARTNERS BLOCK-->

	<table align="center" border="0" cellpadding="20" cellspacing="0" class="wrapper text-block" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #FFFFFF;">
		<tbody>
			<tr>
				<td><!-- PARTNER ITEM-->
				<table align="center" border="0" cellpadding="15" cellspacing="0" class="partner-item" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #98C21F;">
					<tbody>
						<tr>
							<td align="center" width="117"><img alt="" border="0" height="117" pardot-region="" src="https://res.cloudinary.com/eae-pt/image/upload/v1625751217/natural_carton_fzsusu.png" style="display: inline-block" width="117"></td>
							<td valign="top">
							<p pardot-region="" style="
		margin: 0;
		color: #1E355C;
		font-size: 14px;
		line-height: 19px;
		font-family: Arial, Helvetica, sans-serif;
		text-align: left;
	  ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus maximus sem at tortor venenatis, a dictum arcu facilisis. Suspendisse magna dolor, condimentum vel semper ac, efficitur eget justo. Duis in Leo a nunc fringilla elementum.</p>
							</td>
						</tr>
					</tbody>
				</table>
				<!-- space -->

				<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
					<tbody>
						<tr>
							<td style="line-height: 20px; font-size: 20px">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<!-- END space --><!-- PARTNER ITEM-->

				<table align="center" border="0" cellpadding="15" cellspacing="0" class="partner-item" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #F39200;">
					<tbody>
						<tr>
							<td align="center" width="117"><img alt="" border="0" height="117" pardot-region="" src="https://res.cloudinary.com/eae-pt/image/upload/v1625751217/natural_carton_fzsusu.png" style="display: inline-block" width="117"></td>
							<td valign="top">
							<p pardot-region="" style="
		margin: 0;
		color: #1E355C;
		font-size: 14px;
		line-height: 19px;
		font-family: Arial, Helvetica, sans-serif;
		text-align: left;
	  ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus maximus sem at tortor venenatis, a dictum arcu facilisis. Suspendisse magna dolor, condimentum vel semper ac, efficitur eget justo. Duis in Leo a nunc fringilla elementum.</p>
							</td>
						</tr>
					</tbody>
				</table>
				<!-- space -->

				<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
					<tbody>
						<tr>
							<td style="line-height: 20px; font-size: 20px">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<!-- END space --><!-- PARTNER ITEM-->

				<table align="center" border="0" cellpadding="15" cellspacing="0" class="partner-item" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #FDC300;">
					<tbody>
						<tr>
							<td align="center" width="117"><img alt="" border="0" height="117" pardot-region="" src="https://res.cloudinary.com/eae-pt/image/upload/v1625751217/natural_carton_fzsusu.png" style="display: inline-block" width="117"></td>
							<td valign="top">
							<p pardot-region="" style="
		margin: 0;
		color: #1E355C;
		font-size: 14px;
		line-height: 19px;
		font-family: Arial, Helvetica, sans-serif;
		text-align: left;
	  ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus maximus sem at tortor venenatis, a dictum arcu facilisis. Suspendisse magna dolor, condimentum vel semper ac, efficitur eget justo. Duis in Leo a nunc fringilla elementum.</p>
							</td>
						</tr>
					</tbody>
				</table>
				<!-- space -->

				<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
					<tbody>
						<tr>
							<td style="line-height: 20px; font-size: 20px">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<!-- END space --></td>
			</tr>
		</tbody>
	</table>
	<!-- END PARTNERS BLOCK--><!-- space -->

	<table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
		<tbody>
			<tr>
				<td style="line-height: 20px; font-size: 20px">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<!-- END space -->
	<!-- EN --><!-- FOOTER -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table"
pardot-removable=""
style="max-width: 640px; width: 100%; background: #005091"
>
<tbody>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100%; background: #005091;"
>
<tbody>
	<tr>
		<td align="center">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding:0px;" align="left" valign="top" width="340">
	<![endif]-->
			<div
				class="footer__logo"
				style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
			>
				<img
					alt=""
					border="0"
					height="31"
					pardot-region=""
					src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
					style="display: block"
					width="150"
				/>
			</div>
			<!--[if mso]>
	</td>
	<td style="padding:0px;" align="left" valign="top" width="240">
	<![endif]-->

			<div
				class="footer__socials"
				style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__socials__logos"
					style="width: 100%; background: #005091;"
				>
					<tbody>
						<tr>
							<td
								pardot-region=""
								style=" width: 90px; color:#FFFFFF;
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-weight: bold;
line-height: 18px;"
								width="90"
							>
								Get in touch
							</td>
							<td
								align="center"
								style="width: 25px;"
								width="25"
							>
								<a
									href="https://www.facebook.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
										style="display: block"
										width="8"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.linkedin.com/company/south-pole-global/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.instagram.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://twitter.com/southpoleglobal"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- FOOTER BOTTOM -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table-bottom"
style="max-width: 640px; width: 100%; background: #1E355C"
>
<tbody>
<tr>
<td style="line-height: 30px; height: 30px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100% !important;"
>
<tbody>
	<tr>
		<td align="left">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
	<![endif]-->
			<div
				class="footer__moto"
				style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
			>
				<h2
					pardot-region=""
					style="font-family: Arial, Helvetica, sans-serif;
font-size: 18px;
line-height: 21px;
font-weight: bold;
color: #019CDB;
margin: 0;"
				>
					{{{dynamic_content_476}}}
				</h2>
			</div>
			<!--[if mso]>
	</td>
  
	<td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
	<![endif]-->

			<div
				class="footer__address"
				style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__address-table"
					style="width: 100%;"
				>
					<tbody>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px;
font-weight: bold;
color: #FFFFFF;"
							>
								South Pole Global
								Headquarters
							</td>
						</tr>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px; color: #FFFFFF; padding-top: 10px;"
							>
								Technoparkstrasse 1 Zurich
								8005 Switzerland
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 35px; height: 35px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- END FOOTER-->

<!-- DISCLAIMER -->

<table
class="bg-table"
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="max-width: 510px; width: 100%;"
>
<tbody>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr style="">
<td
align="center"
class="footer__disclaimer"
pardot-region=""
style="text-align: center; color:#005091;
font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
line-height: 14px;"
>
You are receiving this email because this email address is
signed up to receive the South Pole communications. You can
<a
href="{{EmailPreferenceCenter_114}}"
style="
				text-decoration: underline;
				color: #005091;
				font-family: Arial, Helvetica,
				  sans-serif;
			  "
>update your preferences here</a
>.
</td>
</tr>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- DISCLAIMER -->

<!-- DE --><!-- FOOTER -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table"
pardot-removable=""
style="max-width: 640px; width: 100%; background: #005091"
>
<tbody>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100%; background: #005091;"
>
<tbody>
	<tr>
		<td align="center">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding:0px;" align="left" valign="top" width="340">
	<![endif]-->
			<div
				class="footer__logo"
				style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
			>
				<img
					alt=""
					border="0"
					height="31"
					pardot-region=""
					src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
					style="display: block"
					width="150"
				/>
			</div>
			<!--[if mso]>
	</td>
	<td style="padding:0px;" align="left" valign="top" width="240">
	<![endif]-->

			<div
				class="footer__socials"
				style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__socials__logos"
					style="width: 100%; background: #005091;"
				>
					<tbody>
						<tr>
							<td
								pardot-region=""
								style=" width: 90px; color:#FFFFFF;
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-weight: bold;
line-height: 18px;"
								width="90"
							>
								Kontakt
							</td>
							<td
								align="center"
								style="width: 25px;"
								width="25"
							>
								<a
									href="https://www.facebook.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
										style="display: block"
										width="8"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.linkedin.com/company/south-pole-global/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.instagram.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://twitter.com/southpoleglobal"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- FOOTER BOTTOM -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table-bottom"
style="max-width: 640px; width: 100%; background: #1E355C"
>
<tbody>
<tr>
<td style="line-height: 30px; height: 30px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100% !important;"
>
<tbody>
	<tr>
		<td align="left">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
	<![endif]-->
			<div
				class="footer__moto"
				style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
			>
				<h2
					pardot-region=""
					style="font-family: Arial, Helvetica, sans-serif;
font-size: 18px;
line-height: 21px;
font-weight: bold;
color: #019CDB;
margin: 0;"
				>
					{{{dynamic_content_479}}}
				</h2>
			</div>
			<!--[if mso]>
	</td>
  
	<td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
	<![endif]-->

			<div
				class="footer__address"
				style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__address-table"
					style="width: 100%;"
				>
					<tbody>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px;
font-weight: bold;
color: #FFFFFF;"
							>
								South Pole Hauptsitz
							</td>
						</tr>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px; color: #FFFFFF; padding-top: 10px;"
							>
								Technoparkstrasse 1 Zürich
								8005 Schweiz
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 35px; height: 35px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- END FOOTER-->

<!-- DISCLAIMER -->

<table
align="center"
class="bg-table"
border="0"
cellpadding="0"
cellspacing="0"
pardot-removable=""
style="max-width: 510px; width: 100%;"
>
<tbody>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr style="">
<td
align="center"
class="footer__disclaimer"
pardot-region=""
style="text-align: center; color:#005091;
font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
line-height: 14px;"
>
Sie erhalten diese E-Mail, weil Ihre E-Mail-Adresse für den
Erhalt dieser Mitteilungen bei South Pole registriert ist. Sie
können
<a
href="{{EmailPreferenceCenter_114}}"
style="
				text-decoration: underline;
				color: #005091;
				font-family: Arial, Helvetica,
				  sans-serif;
			  "
>Ihre Einstellungen hier aktualisieren</a
>.
</td>
</tr>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- DISCLAIMER -->

<!-- ES --><!-- FOOTER -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table"
pardot-removable=""
style="max-width: 640px; width: 100%; background: #005091"
>
<tbody>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100%; background: #005091;"
>
<tbody>
	<tr>
		<td align="center">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding:0px;" align="left" valign="top" width="340">
	<![endif]-->
			<div
				class="footer__logo"
				style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
			>
				<img
					alt=""
					border="0"
					height="31"
					pardot-region=""
					src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
					style="display: block"
					width="150"
				/>
			</div>
			<!--[if mso]>
	</td>
	<td style="padding:0px;" align="left" valign="top" width="240">
	<![endif]-->

			<div
				class="footer__socials"
				style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__socials__logos"
					style="width: 100%; background: #005091;"
				>
					<tbody>
						<tr>
							<td
								pardot-region=""
								style=" width: 90px; color:#FFFFFF;
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-weight: bold;
line-height: 18px;"
								width="90"
							>
								Contáctanos
							</td>
							<td
								align="center"
								style="width: 25px;"
								width="25"
							>
								<a
									href="https://www.facebook.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
										style="display: block"
										width="8"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.linkedin.com/company/south-pole-global/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.instagram.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://twitter.com/southpoleglobal"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- FOOTER BOTTOM -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table-bottom"
style="max-width: 640px; width: 100%; background: #1E355C"
>
<tbody>
<tr>
<td style="line-height: 30px; height: 30px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100% !important;"
>
<tbody>
	<tr>
		<td align="left">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
	<![endif]-->
			<div
				class="footer__moto"
				style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
			>
				<h2
					pardot-region=""
					style="font-family: Arial, Helvetica, sans-serif;
font-size: 18px;
line-height: 21px;
font-weight: bold;
color: #019CDB;
margin: 0;"
				>
					{{{dynamic_content_482}}}
				</h2>
			</div>
			<!--[if mso]>
	</td>
  
	<td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
	<![endif]-->

			<div
				class="footer__address"
				style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__address-table"
					style="width: 100%;"
				>
					<tbody>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px;
font-weight: bold;
color: #FFFFFF;"
							>
								South Pole Hauptsitz
							</td>
						</tr>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px; color: #FFFFFF; padding-top: 10px;"
							>
								Technoparkstrasse 1 Zürich
								8005 Schweiz
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 35px; height: 35px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- END FOOTER-->

<!-- DISCLAIMER -->

<table
align="center"
class="bg-table"
border="0"
cellpadding="0"
cellspacing="0"
pardot-removable=""
style="max-width: 510px; width: 100%;"
>
<tbody>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr style="">
<td
align="center"
class="footer__disclaimer"
pardot-region=""
style="text-align: center; color:#005091;
font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
line-height: 14px;"
>
Está recibiendo este email porque se ha suscrito para recibir
comunicaciones de South Pole. Puede actualizar
<a
href="{{EmailPreferenceCenter_114}}"
style="
				text-decoration: underline;
				color: #005091;
				font-family: Arial, Helvetica,
				  sans-serif;
			  "
>sus preferencias aquí</a
>.
</td>
</tr>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- DISCLAIMER -->
<!-- SV --><!-- FOOTER -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table"
pardot-removable=""
style="max-width: 640px; width: 100%; background: #005091"
>
<tbody>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100%; background: #005091;"
>
<tbody>
	<tr>
		<td align="center">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding:0px;" align="left" valign="top" width="340">
	<![endif]-->
			<div
				class="footer__logo"
				style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
			>
				<img
					alt=""
					border="0"
					height="31"
					pardot-region=""
					src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
					style="display: block"
					width="150"
				/>
			</div>
			<!--[if mso]>
	</td>
	<td style="padding:0px;" align="left" valign="top" width="240">
	<![endif]-->

			<div
				class="footer__socials"
				style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__socials__logos"
					style="width: 100%; background: #005091;"
				>
					<tbody>
						<tr>
							<td
								pardot-region=""
								style=" width: 90px; color:#FFFFFF;
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-weight: bold;
line-height: 18px;"
								width="90"
							>
								Kontakta oss
							</td>
							<td
								align="center"
								style="width: 25px;"
								width="25"
							>
								<a
									href="https://www.facebook.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
										style="display: block"
										width="8"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.linkedin.com/company/south-pole-global/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.instagram.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://twitter.com/southpoleglobal"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- FOOTER BOTTOM -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table-bottom"
style="max-width: 640px; width: 100%; background: #1E355C"
>
<tbody>
<tr>
<td style="line-height: 30px; height: 30px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100% !important;"
>
<tbody>
	<tr>
		<td align="left">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
	<![endif]-->
			<div
				class="footer__moto"
				style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
			>
				<h2
					pardot-region=""
					style="font-family: Arial, Helvetica, sans-serif;
font-size: 18px;
line-height: 21px;
font-weight: bold;
color: #019CDB;
margin: 0;"
				>
					{{{dynamic_content_592}}}
				</h2>
			</div>
			<!--[if mso]>
	</td>
  
	<td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
	<![endif]-->

			<div
				class="footer__address"
				style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__address-table"
					style="width: 100%;"
				>
					<tbody>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px;
font-weight: bold;
color: #FFFFFF;"
							>
								South Pole globala huvudkontoret
							</td>
						</tr>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px; color: #FFFFFF; padding-top: 10px;"
							>
								Technoparkstrasse 1 Zürich
								8005 Schweiz
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 35px; height: 35px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- END FOOTER-->

<!-- DISCLAIMER -->

<table
align="center"
class="bg-table"
border="0"
cellpadding="0"
cellspacing="0"
pardot-removable=""
style="max-width: 510px; width: 100%;"
>
<tbody>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr style="">
<td
align="center"
class="footer__disclaimer"
pardot-region=""
style="text-align: center; color:#005091;
font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
line-height: 14px;"
>
Du har mottagit detta e-mail då e-postadressen är registrerad hos oss för mottagning av kommunikation från South Pole. Du kan uppdatera dina preferenser
<a
href="{{EmailPreferenceCenter_114}}"
style="
				text-decoration: underline;
				color: #005091;
				font-family: Arial, Helvetica,
				  sans-serif;
			  "
>här</a
>.
</td>
</tr>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- DISCLAIMER -->


<!-- FR --><!-- FOOTER -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table"
pardot-removable=""
style="max-width: 640px; width: 100%; background: #005091"
>
<tbody>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100%; background: #005091;"
>
<tbody>
	<tr>
		<td align="center">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding:0px;" align="left" valign="top" width="340">
	<![endif]-->
			<div
				class="footer__logo"
				style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
			>
				<img
					alt=""
					border="0"
					height="31"
					pardot-region=""
					src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
					style="display: block"
					width="150"
				/>
			</div>
			<!--[if mso]>
	</td>
	<td style="padding:0px;" align="left" valign="top" width="240">
	<![endif]-->

			<div
				class="footer__socials"
				style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__socials__logos"
					style="width: 100%; background: #005091;"
				>
					<tbody>
						<tr>
							<td
								pardot-region=""
								style=" width: 90px; color:#FFFFFF;
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-weight: bold;
line-height: 18px;"
								width="90"
							>
								Contactez-nous
							</td>
							<td
								align="center"
								style="width: 25px;"
								width="25"
							>
								<a
									href="https://www.facebook.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
										style="display: block"
										width="8"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.linkedin.com/company/south-pole-global/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.instagram.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://twitter.com/southpoleglobal"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- FOOTER BOTTOM -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table-bottom"
style="max-width: 640px; width: 100%; background: #1E355C"
>
<tbody>
<tr>
<td style="line-height: 30px; height: 30px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100% !important;"
>
<tbody>
	<tr>
		<td align="left">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
	<![endif]-->
			<div
				class="footer__moto"
				style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
			>
				<h2
					pardot-region=""
					style="font-family: Arial, Helvetica, sans-serif;
font-size: 18px;
line-height: 21px;
font-weight: bold;
color: #019CDB;
margin: 0;"
				>
					{{{dynamic_content_485}}}
				</h2>
			</div>
			<!--[if mso]>
	</td>
  
	<td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
	<![endif]-->

			<div
				class="footer__address"
				style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__address-table"
					style="width: 100%;"
				>
					<tbody>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px;
font-weight: bold;
color: #FFFFFF;"
							>
								South Pole France
							</td>
						</tr>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px; color: #FFFFFF; padding-top: 10px;"
							>
								Morning, 75 rue d’Amsterdam,
								75009 Paris
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 35px; height: 35px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- END FOOTER-->

<!-- DISCLAIMER -->

<table
align="center"
class="bg-table"
border="0"
cellpadding="0"
cellspacing="0"
pardot-removable=""
style="max-width: 510px; width: 100%;"
>
<tbody>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr style="">
<td
align="center"
class="footer__disclaimer"
pardot-region=""
style="text-align: center; color:#005091;
font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
line-height: 14px;"
>
Vous recevez trop d'emails? Vous pouvez
<a
href="{{EmailPreferenceCenter_114}}"
style="
				text-decoration: underline;
				color: #005091;
				font-family: Arial, Helvetica,
				  sans-serif;
			  "
>gérer vos préférences ici</a
>.
</td>
</tr>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- DISCLAIMER -->

<!-- BELUX EN --><!-- FOOTER -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table"
pardot-removable=""
style="max-width: 640px; width: 100%; background: #005091"
>
<tbody>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100%; background: #005091;"
>
<tbody>
	<tr>
		<td align="center">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding:0px;" align="left" valign="top" width="340">
	<![endif]-->
			 <div class="footer__logo" style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"><img alt="" border="0" height="54" pardot-height="54" pardot-region="" pardot-width="120" src="https://go.southpole.com/l/881182/2022-01-14/2g3nw/881182/1642150089QLePlYqU/CO2logic_part_of_south_pole_white_EN.png" style="display: block; width: 120px; max-width: 120px; height: 54px; max-height: 54px;" width="120"></div>
			<!--[if mso]>
	</td>
	<td style="padding:0px;" align="left" valign="top" width="240">
	<![endif]-->

			<div
				class="footer__socials"
				style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__socials__logos"
					style="width: 100%; background: #005091;"
				>
					<tbody>
						<tr>
							<td
								pardot-region=""
								style=" width: 90px; color:#FFFFFF;
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-weight: bold;
line-height: 18px;"
								width="90"
							>
								Get in touch
							</td>
							<td
								align="center"
								style="width: 25px;"
								width="25"
							>
								<a
									href="https://www.facebook.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
										style="display: block"
										width="8"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.linkedin.com/company/south-pole-global/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://www.instagram.com/southpoleglobal/"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td
								align="center"
								style="width: 30px;"
								width="30"
							>
								<a
									href="https://twitter.com/southpoleglobal"
									><img
										alt=""
										border="0"
										height="18"
										src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
										style="display: block"
										width="18"
								/></a>
							</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- FOOTER BOTTOM -->

<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
class="footer-table-bottom"
style="max-width: 640px; width: 100%; background: #1E355C"
>
<tbody>
<tr>
<td style="line-height: 30px; height: 30px">&nbsp;</td>
</tr>
<tr>
<td>
<table
align="center"
border="0"
cellpadding="0"
cellspacing="0"
style="width: 100% !important;"
>
<tbody>
	<tr>
		<td align="left">
			<!--[if mso]>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
	<![endif]-->
			<div
				class="footer__moto"
				style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
			>
				<h2
					pardot-region=""
					style="font-family: Arial, Helvetica, sans-serif;
font-size: 18px;
line-height: 21px;
font-weight: bold;
color: #019CDB;
margin: 0;"
				>
					{{{dynamic_content_488}}}
				</h2>
			</div>
			<!--[if mso]>
	</td>
  
	<td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
	<![endif]-->

			<div
				class="footer__address"
				style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
			>
				<table
					align="center"
					border="0"
					cellpadding="0"
					cellspacing="0"
					class="footer__address-table"
					style="width: 100%;"
				>
					<tbody>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px;
font-weight: bold;
color: #FFFFFF;"
							>
								CO2logic part of South Pole
							</td>
						</tr>
						<tr style="">
							<td
								pardot-region=""
								style="font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
line-height: 18px; color: #FFFFFF; padding-top: 10px;"
							>
								Cantersteen 47, 1000
								Bruxelles, Belgium
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso]>
	</td>
	</tr>
	</table>
	<![endif]-->
		</td>
	</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="line-height: 35px; height: 35px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- END FOOTER-->
<!-- DISCLAIMER -->

<table
align="center"
class="bg-table"
border="0"
cellpadding="0"
cellspacing="0"
pardot-removable=""
style="max-width: 510px; width: 100%;"
>
<tbody>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
<tr style="">
<td
align="center"
class="footer__disclaimer"
pardot-region=""
style="text-align: center; color:#005091;
font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
line-height: 14px;"
>
You are receiving this email because this email address is
signed up to receive the South Pole communications. You can
<a
href="{{EmailPreferenceCenter_114}}"
style="
text-decoration: underline;
color: #005091;
font-family: Arial, Helvetica,
sans-serif;
"
>update your preferences here</a
>.
</td>
</tr>
<tr class="space bg-table">
<td style="line-height: 20px; height: 20px">&nbsp;</td>
</tr>
</tbody>
</table>
<!-- DISCLAIMER -->
	`;

  	return templatePressRelease;
}