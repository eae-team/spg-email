import {createIntro} from './Intro';

export default {
  title: 'Blocks/Intro',
};

const Template = () => createIntro();

export const IntroComponent = Template.bind({});
