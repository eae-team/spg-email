import { createTeaserFullPardot } from './Teaser-Image-FullPardot';

export default {
  title: 'Pardot Tags/Teaser',
};

const Template = () => createTeaserFullPardot();

export const TeaserFullComponent = Template.bind({});

