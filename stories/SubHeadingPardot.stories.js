import { createSubHeadingPardot } from './SubHeadingPardot';

export default {
  title: 'Pardot Tags/Sub-Heading/Default',
};

const Template = () => createSubHeadingPardot();

export const SubHeadingComponent = Template.bind({});