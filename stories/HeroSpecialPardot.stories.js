import {createHeroSpecialPardot} from './HeroSpecialPardot';

export default {
  title: 'Pardot Tags/Hero/Special',
};

const Template = () => createHeroSpecialPardot();

export const HeroComponent = Template.bind({});

