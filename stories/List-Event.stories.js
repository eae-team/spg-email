import { createListEvent } from './List-Event';

export default {
  title: 'Blocks/List',
};

const Template = () => createListEvent();

export const ListEventComponent = Template.bind({});
