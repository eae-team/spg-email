import { createFooterPardotSW } from './FooterPardotSW';

export default {
  title: 'Pardot Tags/Footer/Footer EN (Switzerland)',
};

const Template = () => createFooterPardotSW();

export const FooterComponent = Template.bind({});

