import './main.css';

export const createFooterPardot = () => {
    const footer = `

  <!-- FOOTER -->
<table  pardot-removable=""
        class="footer-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 640px; width: 100%; background: #005091"
>
    <tr>
        <td style="line-height: 20px; height: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table
                    class=""
                    cellpadding="0"
                    cellspacing="0"
                    border="0"
                    align="center"
                    style="width: 100%; background: #005091;"
            >
                <tr>
                    <td align="center">
                    
                        <!--[if mso]>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding:0px;" align="left" valign="top" width="340">
                        <![endif]-->
                        <div
                                style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
                                class="footer__logo"
                        >
                            <img
                                    src="https://res.cloudinary.com/eae-pt/image/upload/v1624634722/southpole_logo_white_w7xfia.png"
                                    width="150"
                                    height="31"
                                    pardot-region=""
                                    alt=""
                                    border="0"
                                    style="display: block"
                                    class=""
                            />
                        </div>
                        <!--[if mso]>
                        </td>
                        <td style="padding:0px;" align="left" valign="top" width="240">
                        <![endif]-->

                        <div
                                style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                class="footer__socials"
                        >
                            <table
                                    class="footer__socials__logos"
                                    cellpadding="0"
                                    cellspacing="0"
                                    border="0"
                                    align="center"
                                    style="width: 100%; background: #005091;"
                            >
                                <tr>
                                    <td pardot-region="" width="90" style=" width: 90px; color:#FFFFFF;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    font-weight: bold;
                    line-height: 18px;">Get in touch
                                    </td>
                                    <td width="25" align="center" style="width: 25px;">
                                        <img
                                                src="https://res.cloudinary.com/eae-pt/image/upload/v1624637456/facebook_white_nonq5p.png"
                                                width="8"
                                                height="18"
                                                alt=""
                                                border="0"
                                                style="display: block"

                                        />
                                    </td>
                                    <td width="30" align="center" style="width: 30px;">
                                        <img
                                                src="https://res.cloudinary.com/eae-pt/image/upload/v1624637456/in_white_lnctjn.png"
                                                width="18"
                                                height="18"
                                                alt=""
                                                border="0"
                                                style="display: block"

                                        />
                                    </td>
                                    <td width="30" align="center" style="width: 30px;">
                                        <img
                                                src="https://res.cloudinary.com/eae-pt/image/upload/v1624637456/insta_white_d1faer.png"
                                                width="18"
                                                height="18"
                                                alt=""
                                                border="0"
                                                style="display: block"

                                        />
                                    </td>
                                    <td width="30" align="center" style="width: 30px;">
                                        <img
                                                src="https://res.cloudinary.com/eae-pt/image/upload/v1624637456/twitter_white_c2224x.png"
                                                width="18"
                                                height="18"
                                                alt=""
                                                border="0"
                                                style="display: block"

                                        />
                                    </td>
                                    <td>&nbsp;</td>


                                </tr>
                            </table>
                        </div>
                        <!--[if mso]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>

                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td style="line-height: 20px; height: 20px">&nbsp;</td>
    </tr>
</table>
<!-- FOOTER BOTTOM -->
<table
        class="footer-table-bottom"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 640px; width: 100%; background: #1E355C"
>
    <tr>
        <td style="line-height: 30px; height: 30px">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table
                    class=""
                    cellpadding="0"
                    cellspacing="0"
                    border="0"
                    align="center"
                    style="width: 100% !important;"
            >
                <tr>
                    <td align="left">
                    <!--[if mso]>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                        <![endif]-->
                        <div
                                style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                class="footer__moto"
                        >
                            <h2 pardot-region="" style="font-family: Arial, Helvetica, sans-serif;
                    font-size: 18px;
                    line-height: 21px;
                    font-weight: bold;
                    color: #019CDB;
                    margin: 0;">{{{dynamic_content_476}}}</h2>
                        </div>
                        
                        <!--[if mso]>
                        </td>
                      
                        <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                        <![endif]-->

                        <div
                                style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                class="footer__address"
                        >
                        
                            <table
                                    class="footer__address-table"
                                    cellpadding="0"
                                    cellspacing="0"
                                    border="0"
                                    align="center"
                                    style="width: 100%;"
                            >
                                <tr>
                                    <td pardot-region="" style="font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    line-height: 18px;
                    font-weight: bold;
                    color: #FFFFFF;">South Pole Global Headquarters
                                    </td>
                                </tr>
                                <tr>
                                    <td pardot-region="" style="font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    line-height: 18px; color: #FFFFFF; padding-top: 10px;">Technoparkstrasse 1
                                        Zurich 8005
                                        Switzerland
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <!--[if mso]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>

                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td style="line-height: 35px; height: 35px">&nbsp;</td>
    </tr>
</table>

<!-- END FOOTER-->

  `;

    return footer;
};
