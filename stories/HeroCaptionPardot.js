import './main.css';

export const createHeroCaptionPardot = () => {
	const header = `
<!-- HERO INFO -->
<table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       pardot-removable=""
       class="header"
       align="center">
    <tr>
        <td>
            <!-- HEADER-HERO : BEGIN -->
            <table
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    border="0"
                    width="100%"
                    style="
                      max-width: 640px;
                      width: 100%;
                      margin: auto;
                      text-align: center;
                    "
                    class="deviceWidth"
            >
                <tbody>
                <tr>
                    <td
                            background="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621857015/logo_dbsewx_d7b0sc.jpg"
                            width="640"
                            height="355"
                            pardot-region=""
                            valign="top"
                            style="background-repeat: no-repeat; background-size: cover"
                            class="deviceWidth header"
                    >
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                style="width:640px;height:355px;">
                            <v:fill type="frame"
                                    pardot-region=""
                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621857015/logo_dbsewx_d7b0sc.jpg"
                                    color="#005091"/>
                            <v:textbox inset="0,0,0,0">
                        <![endif]-->
                        <div>
                            <table
                                    role="presentation"
                                    border="0"
                                    cellpadding="0"
                                    cellspacing="0"
                                    align="right"
                                    width="100%"
                                    style="max-width: 640px; margin: auto"
                            >
                                <tbody>
                                <tr>
                                    <td align="right" valign="middle">
                                        <table
                                                role="presentation"
                                                border="0"
                                                cellpadding="0"
                                                cellspacing="0"
                                                align="right"
                                                width="100%"
                                                class="header__content"
                                        >
                                            <tbody>
                                            <tr>
                                                <td align="right" style="padding: 15px 15px;"> 
													<p pardot-region="" style="margin: 0; text-align:right; font-size: 11px !important; color: #fff; font-family: Arial, Helvetica, sans-serif;"> Lorem ipsum dolor sit caption </p> 
												</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <!--[if gte mso 9]>
                        </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
</table>
<!-- END HERO INFO -->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table> 
<!-- END space -->
`;

	return header;
};
