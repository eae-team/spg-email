import { createFooter } from './Footer';

export default {
  title: 'Blocks/Footer',
};

const Template = () => createFooter();

export const FooterComponent = Template.bind({});

