import './main.css';

export const createListArticle = () => {
    const listArticle = `
  <!-- news columns sm image text -->
<table
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="news-cols-sm-img-text"
>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:150px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 150px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="news-cols-sm-img-text__image">
                    <tr>
                        <td
                                background="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                bgcolor="#ffffff"
                                width="150"
                                height="130"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                    style="width:150px;height:130px;">
                                <v:fill type="frame"
                                        src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                        color="#ffffff"/>
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="150px"
                                        class="img"
                                >
                                    <tr>
                                        <td width="125px" class="col-lg">&nbsp;</td>
                                        <td
                                                style="
                            width: 25px;
                            height: 110px;
                            background: rgba(255, 255, 255, 0.9);
                          "
                                                valign="bottom"
                                                class="col-sm"
                                        >
                                            &nbsp;
                                            <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 25px;
                                height: 110px;
                                position: absolute;
                                top: 20px;
                                left: 125px;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="news-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #019CDB;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 13px;
                    line-height: 18px;
                    font-weight: bold;
                  "
                  class="news-cols-sm-img-text__sub-title"
                        >
                            Carbon Markets & Climate Policy
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 21px;
                    line-height: 21px;
                    font-weight: bold;
                  "
                  class="news-cols-sm-img-text__title"
                        >
                            Donec consequat purus sed nunc
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #485B7B;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 16px;
                    line-height: 21px;
                  "
                  class="news-cols-sm-img-text__description"
                        >
                            Mauris volutpat urna ut feugiat tempor. Praesent elit sapien,
                            egestas vulputate orci eu, egestas laoreet nibh.
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a
                                    href="http://"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    style="
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      font-size: 16px;
                      line-height: 20px;
                      font-weight: bold;
                      text-decoration: none;
                    "
                     class="news-cols-sm-img-text__link"
                            >
                                Learn more &amp; register >
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:150px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 150px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="news-cols-sm-img-text__image" width="100%">
                    <tr>
                        <td
                                background="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                bgcolor="#ffffff"
                                width="150"
                                height="130"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                    style="width:150px;height:130px;">
                                <v:fill type="frame"
                                        src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                        color="#ffffff"/>
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="150px"
                                        class="img"
                                >
                                    <tr>
                                        <td width="125px" class="col-lg">&nbsp;</td>
                                        <td
                                                style="
                            width: 25px;
                            height: 110px;
                            background: rgba(255, 255, 255, 0.9);
                          "
                                                valign="bottom"
                                                class="col-sm"
                                        >
                                            &nbsp;
                                            <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 25px;
                                height: 110px;
                                position: absolute;
                                top: 20px;
                                left: 125px;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="news-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #019CDB;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 13px;
                    line-height: 18px;
                    font-weight: bold;
                  "
                  class="news-cols-sm-img-text__sub-title"
                        >
                            Carbon Markets & Climate Policy
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 21px;
                    line-height: 21px;
                    font-weight: bold;
                  "
                  class="news-cols-sm-img-text__title"
                        >
                            Donec consequat purus sed nunc
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #485B7B;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 16px;
                    line-height: 21px;
                  "
                   class="news-cols-sm-img-text__description"
                        >
                            Mauris volutpat urna ut feugiat tempor. Praesent elit sapien,
                            egestas vulputate orci eu, egestas laoreet nibh.
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a
                                    href="http://"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    style="
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      font-size: 16px;
                      line-height: 20px;
                      font-weight: bold;
                      text-decoration: none;
                    "
                    class="news-cols-sm-img-text__link"
                            >
                                Learn more &amp; register >
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:150px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 150px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="news-cols-sm-img-text__image" width="100%">
                    <tr>
                        <td
                                background="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                bgcolor="#ffffff"
                                width="150"
                                height="130"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                    style="width:150px;height:130px;">
                                <v:fill type="frame"
                                        src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                        color="#ffffff"/>
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="150px"
                                        class="img"
                                >
                                    <tr>
                                        <td width="125px" class="col-lg">&nbsp;</td>
                                        <td
                                                style="
                            width: 25px;
                            height: 110px;
                            background: rgba(255, 255, 255, 0.9);
                          "
                                                valign="bottom"
                                                class="col-sm"
                                        >
                                            &nbsp;
                                            <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 25px;
                                height: 110px;
                                position: absolute;
                                top: 20px;
                                left: 125px;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="news-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #019CDB;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 13px;
                    line-height: 18px;
                    font-weight: bold;
                  "
                  class="news-cols-sm-img-text__sub-title"
                        >
                            Carbon Markets & Climate Policy
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 21px;
                    line-height: 21px;
                    font-weight: bold;
                  "
                  class="news-cols-sm-img-text__title"
                        >
                            Donec consequat purus sed nunc
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #485B7B;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 16px;
                    line-height: 21px;
                  "
                   class="news-cols-sm-img-text__description"
                        >
                            Mauris volutpat urna ut feugiat tempor. Praesent elit sapien,
                            egestas vulputate orci eu, egestas laoreet nibh.
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a
                                    href="http://"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    style="
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      font-size: 16px;
                      line-height: 20px;
                      font-weight: bold;
                      text-decoration: none;
                    "
                    class="news-cols-sm-img-text__link"
                            >
                                Learn more &amp; register >
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END news columns sm image text -->
<!-- space -->
<table cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
  `;

    return listArticle;
};
