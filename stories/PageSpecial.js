import {createHeaderViewOnBrowser} from './HeaderViewOnBrowser';
import {createHeaderLogo} from './HeaderLogo';
import {createHeroSpecial} from './HeroSpecial';
import {createFooter} from './Footer';
import {createDisclaimer} from './Disclaimer';


export const createPageSpecial = () => {
    const viewOnBrowser = createHeaderViewOnBrowser();
    const header = createHeaderLogo();

    const heroSpecial = createHeroSpecial();
    const footer = createFooter();
    const disclaimer = createDisclaimer();


    const section = `  
                    ${viewOnBrowser}
                    ${header}
                   
                    ${heroSpecial}
                 
                    ${footer}
                    ${disclaimer}
                


`;

    return section;
};
