import './main.css';

export const createTemplateSurveyPardot = () => {
    const templateSurvey = `
    <table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    class="wrapper text-block"
    pardot-repeatable=""
    style="max-width: 640px; width: 100%; background: #ffffff"
>
    <tbody>
        <tr>
            <td class="spacing" style="height: 40px">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table
                    align="center"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    class="wrapper"
                    style="max-width: 640px; width: 100%; background: #ffffff"
                >
                    <tbody>
                        <tr style="">
                            <td width="20">&nbsp;</td>
                            <td
                                class="white__text_90"
                                pardot-region=""
                                style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 18px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                            >
                                <p>
                                    <strong>Dear {{Recipient.FirstName}}</strong
                                    >,
                                </p>

                                <p>
                                    At South Pole, our aim is to help our
                                    customers turn their climate ambition into
                                    action.&nbsp;
                                </p>

                                <p>
                                    As we seek to improve our offering, we would
                                    like to hear from you and request your
                                    feedback to a single question that will
                                    provide valuable insight to support your
                                    climate action.<br />
                                    &nbsp;
                                </p>
                            </td>
                            <td width="20">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- END 1ST TEXT BLOCK--><!-- space --><!-- END space -->

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    class="wrapper text-block"
    pardot-repeatable=""
    style="max-width: 640px; width: 100%; background: #005091"
>
    <tbody>
        <!--<tr>
									<td class="spacing" style="height: 40px">&nbsp;</td>
								</tr>-->
        <tr>
            <td>
                <table
                    align="center"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    class="wrapper"
                    style="max-width: 640px; background: #005091"
                >
                    <tbody>
                        <tr></tr>
                        <tr style="">
                            <td width="20">&nbsp;</td>
                            <td
                                class="smaller-header-text"
                                pardot-region=""
                                style="font-size:23px;font-family: Arial, Helvetica, sans-serif;color:#ffffff;font-weight:600;line-height:30px;text-align:center;"
                            >
                                <br />
                                Considering your experience with South Pole’s
                                services, how likely are you to recommend South
                                Pole to someone with similar business
                                challenges?<br />
                                <span style="font-size:10px;"
                                    >(click on your choice below)</span
                                >
                            </td>
                            <td width="20">&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    class="rating wrapper"
    style="max-width: 640px; width: 100%; background: #005091;"
>
    <tbody>
        <tr>
            <td width="20">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=1&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >1</a
                                >
                            </td>
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=2&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >2</a
                                >
                            </td>    
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=3&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >3</a
                                >
                            </td>      
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=4&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >4</a
                                >
                            </td>      
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=5&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >5</a
                                >
                            </td>      
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=6&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >6</a
                                >
                            </td>       
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=7&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >7</a
                                >
                            </td>   
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=8&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >8</a
                                >
                            </td>     
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=9&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >9</a
                                >
                            </td>      
            <td width="5">&nbsp;</td>
            <td bgcolor="#f5f5f5"
                                style="font-size:21px;line-height:40px;text-align:center; width: 32px"
                                class="rating-box"
                            >
                                <a
                                    href="https://go.southpole.com/we-value-your-opinion-en-2022?nps=10&amp;email={{Recipient.Email}}"
                                    style="color:#2E528F;text-decoration:none;"
                                    >10</a
                                >
                            </td>  
            <td width="20">&nbsp;</td>
        </tr>
    </tbody>
</table>

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    class="wrapper"
    style="max-width: 640px; width: 100%; background: #005091;"
>
    <tbody>
        <tr>
            <td>
                <table
                    align="center"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    class="wrapper"
                    style="max-width: 640px; width: 100%; background: #005091;"
                >
                    <tbody>
                        <tr>
                            <td width="20">&nbsp;</td>
                            <td
                                style="padding: 5px 0 25px;font-size:13px;font-family:'Open Sans',Arial,Sans-serif;color:#ffffff;line-height:24px;"
                            >
                                0: not at all likely
                            </td>
                            <td
                                style="padding: 5px 0 25px;text-align:right;font-size:13px;font-family:'Open Sans',Arial,Sans-serif;color:#ffffff;line-height:24px;"
                            >
                                10: extremely likely
                            </td>
                            <td width="20">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    class="wrapper text-block"
    pardot-repeatable=""
    style="max-width: 640px; width: 100%; background: #ffffff"
>
    <tbody>
        <!--<tr>
									<td class="spacing" style="height: 40px">&nbsp;</td>
								</tr>-->
        <tr>
            <td>
                <table
                    align="center"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    class="wrapper"
                    style="max-width: 640px; width: 100%; background: #ffffff"
                >
                    <tbody>
                        <tr style="">
                            <td width="20">&nbsp;</td>
                            <td
                                class="white__text_90"
                                pardot-region=""
                                style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 18px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                            >
                                <br />
                                With thanks,<br />
                                <br />
                                ​​​​South Pole Customer Insights Team<br />
                                &nbsp;
                            </td>
                            <td width="20">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<!-- space -->

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    pardot-repeatable=""
    class="bg-table"
>
    <tbody>
        <tr>
            <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END space -->

`;

    return templateSurvey;
};
