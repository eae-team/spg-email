import {createIntroPardot} from './IntroPardot';

export default {
  title: 'Pardot Tags/Intro',
};

const Template = () => createIntroPardot();

export const IntroComponent = Template.bind({});
