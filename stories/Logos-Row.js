import './main.css';

export const createLogosRow = () => {
	const logosRow = `
    <!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tbody>
        <tr>
            <td style="line-height: 15px; font-size: 15px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END space -->

    <!-- LOGO ROW -->
<table
    cellpadding="0"
    cellspacing="0"
    border="0"
    style="max-width: 640px; width: 100%; background: #f2f4f7"
    align="center"
>
    <tbody>
        <tr>
            <td>
                <table
                    align="center"
                    cellpadding="0"
                    cellspacing="0"
                    border="0"
                >
                    <tbody>
                        <tr>
                            <td>
                                <a
                                    href="https://www.southpole.com/"
                                    target="_blank"
                                >
                                    <img
                                        src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1637332161/Mask_Group_31_2x_lgrzfs.png"
                                        width="83"
                                        height="26"
                                        alt=""
                                        border="0"
                                        style="display: block"
                                    />
                                </a>
                            </td>
                            <td
                                style="padding-left: 30px;"
                            >
                            <img
                            src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1637332161/ParawayLogo_2x_svucxy.png"
                            width="83"
                            height="26"
                            alt=""
                            border="0"
                            style="display: block"
                        />
                            </td>

                            <td
                                style="padding-left: 30px;"
                            >
                            <img
                            src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1637334198/corp-carbon_y8pbrb.png"
                            width="86"
                            height="42"
                            alt=""
                            border="0"
                            style="display: block"
                        />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- END LOGO ROW -->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tbody>
        <tr>
            <td style="line-height: 15px; font-size: 15px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END space -->

`;

	return logosRow;
};