import {createFullCTAOrangePardot} from './FullCTAOrangePardot';

export default {
  title: 'Pardot Tags/Full CTA/Orange',
};

const Template = () => createFullCTAOrangePardot();

export const FullCTAComponent = Template.bind({});
