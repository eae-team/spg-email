import { createTeaserLeft } from './Teaser-Image-Left';

export default {
  title: 'Blocks/Teaser',
};

const Template = () => createTeaserLeft();

export const TeaserLeftComponent = Template.bind({});

