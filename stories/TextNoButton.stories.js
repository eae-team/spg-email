import {createTextNoButton} from './TextNoButton';

export default {
  title: 'Blocks/Text/Text No Button',
};

const Template = () => createTextNoButton();

export const TextNoButtonComponent = Template.bind({});
