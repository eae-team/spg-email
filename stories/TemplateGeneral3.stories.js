import { createTemplateGeneral3 } from './TemplateGeneral3';

export default {
  title: 'Pardot Templates/Template General 3',
};

const Template = () => createTemplateGeneral3();

export const templateGeneral3Component = Template.bind({});

