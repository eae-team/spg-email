import {createFullCTAOrange} from './FullCTAOrange';

export default {
  title: 'Blocks/Full CTA/Orange',
};

const Template = () => createFullCTAOrange();

export const FullCTAComponent = Template.bind({});
