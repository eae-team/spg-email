import {createFullCTAYellow} from './FullCTAYellow';

export default {
  title: 'Blocks/Full CTA/Yellow',
};

const Template = () => createFullCTAYellow();

export const FullCTAComponent = Template.bind({});
