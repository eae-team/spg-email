import { createTemplateGeneral2 } from './TemplateGeneral2';

export default {
  title: 'Pardot Templates/Template General 2',
};

const Template = () => createTemplateGeneral2();

export const templateGeneral2Component = Template.bind({});

