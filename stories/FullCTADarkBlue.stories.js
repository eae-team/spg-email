import {createFullCTADarkBlue} from './FullCTADarkBlue';

export default {
  title: 'Blocks/Full CTA/Dark blue',
};

const Template = () => createFullCTADarkBlue();

export const FullCTAComponent = Template.bind({});
