import { createSubHeadingGrayPardot } from './SubHeadingGrayPardot';

export default {
  title: 'Pardot Tags/Sub-Heading/Background Gray',
};

const Template = () => createSubHeadingGrayPardot();

export const SubHeadingGrayComponent = Template.bind({});