import { createListArticlePardot } from './List-ArticlePardot';

export default {
  title: 'Pardot Tags/List',
};

const Template = () => createListArticlePardot();

export const ListArticleComponent = Template.bind({});
