import { createSpeaker } from './Speaker';

export default {
  title: 'Blocks/Speaker',
};

const Template = () => createSpeaker();

export const SpeakerComponent = Template.bind({});

