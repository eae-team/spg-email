import {createHeroInfoNoButtonPardot} from './HeroInfoNoButtonPardot';

export default {
  title: 'Pardot Tags/Hero/Info No Button',
};

const Template = () => createHeroInfoNoButtonPardot();

export const HeroComponent = Template.bind({});

