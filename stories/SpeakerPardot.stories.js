import { createSpeakerPardot } from './SpeakerPardot';

export default {
  title: 'Pardot Tags/Speaker',
};

const Template = () => createSpeakerPardot();

export const SpeakerComponent = Template.bind({});

