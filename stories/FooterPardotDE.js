import './main.css';

export const createFooterPardotDE = () => {
    const footer = `
    <!-- DE --><!-- FOOTER -->

                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="footer-table" pardot-removable="" style="max-width: 640px; width: 100%; background: #005091">
                            <tbody>
                                <tr>
                                    <td style="line-height: 20px; height: 20px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100%; background: #005091;">
                                        <tbody>
                                            <tr>
                                                <td align="center"><!--[if mso]>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding:0px;" align="left" valign="top" width="340">
                        <![endif]-->
                                                <div class="footer__logo" style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"><img alt="" border="0" height="31" pardot-region="" src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png" style="display: block" width="150"></div>
                                                <!--[if mso]>
                        </td>
                        <td style="padding:0px;" align="left" valign="top" width="240">
                        <![endif]-->

                                                <div class="footer__socials" style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;">
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="footer__socials__logos" style="width: 100%; background: #005091;">
                                                    <tbody>
                                                        <tr>
                                                            <td pardot-region="" style=" width: 90px; color:#FFFFFF;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    font-weight: bold;
                    line-height: 18px;" width="90">Kontakt</td>
                                                            <td align="center" style="width: 25px;" width="25"><a href="https://www.facebook.com/southpoleglobal/"><img alt="" border="0" height="18" src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png" style="display: block" width="8"></a></td>
                                                            <td align="center" style="width: 30px;" width="30"><a href="https://www.linkedin.com/company/south-pole-global/"><img alt="" border="0" height="18" src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png" style="display: block" width="18"></a></td>
                                                            <td align="center" style="width: 30px;" width="30"><a href="https://www.instagram.com/southpoleglobal/"><img alt="" border="0" height="18" src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png" style="display: block" width="18"></a></td>
                                                            <td align="center" style="width: 30px;" width="30"><a href="https://twitter.com/southpoleglobal"><img alt="" border="0" height="18" src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png" style="display: block" width="18"></a></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </div>
                                                <!--[if mso]>
                        </td>
                        </tr>
                        </table>
                        <![endif]--></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="line-height: 20px; height: 20px">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- FOOTER BOTTOM -->

                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="footer-table-bottom" style="max-width: 640px; width: 100%; background: #1E355C">
                            <tbody>
                                <tr>
                                    <td style="line-height: 30px; height: 30px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 100% !important;">
                                        <tbody>
                                            <tr>
                                                <td align="left"><!--[if mso]>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                        <![endif]-->
                                                <div class="footer__moto" style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;">
                                                <h2 pardot-region="" style="font-family: Arial, Helvetica, sans-serif;
                    font-size: 18px;
                    line-height: 21px;
                    font-weight: bold;
                    color: #019CDB;
                    margin: 0;">{{{dynamic_content_479}}}</h2>
                                                </div>
                                                <!--[if mso]>
                        </td>
                      
                        <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                        <![endif]-->

                                                <div class="footer__address" style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;">
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="footer__address-table" style="width: 100%;">
                                                    <tbody>
                                                        <tr style="">
                                                            <td pardot-region="" style="font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    line-height: 18px;
                    font-weight: bold;
                    color: #FFFFFF;">South Pole Hauptsitz</td>
                                                        </tr>
                                                        <tr style="">
                                                            <td pardot-region="" style="font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    line-height: 18px; color: #FFFFFF; padding-top: 10px;">Technoparkstrasse 1 Zürich 8005 Schweiz</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </div>
                                                <!--[if mso]>
                        </td>
                        </tr>
                        </table>
                        <![endif]--></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="line-height: 35px; height: 35px">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- END FOOTER-->`;

    return footer;
};
