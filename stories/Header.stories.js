import { createHeader } from './Header';

export default {
  title: 'Blocks/Header/Logo and Title',
};

const Template = () => createHeader();

export const HeaderComponent = Template.bind({});

