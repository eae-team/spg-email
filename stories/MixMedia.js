import './main.css';

export const createMixMedia = () => {
    const mixMedia = `
<!-- MIX MEDIA BLOCK-->
<table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       class="wrapper text-block"
       align="center">
    <tr>
        <td style="height: 40px;" class="spacing">&nbsp;</td>
    </tr>
    <tr>
      
        <td>
            <table cellpadding="0"
               cellspacing="0"
               border="0"
               style="max-width: 640px; width: 100%; background: #ffffff"
               class="wrapper mix-media"
               align="center">
                <tr>
                    <td width="20">&nbsp;</td>
                    <td>
                        <!--[if mso]>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding:0px 15px 0px 0px;" align="left" valign="top" width="200">
                        <![endif]-->
                       <div
                        style="display: inline-block; vertical-align: top; width: 100%; margin: 0 15px 0 0;"
                        class="deviceWidth heightAuto col-lg mix-media__wrapper-img"
                        >
                        <img
                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625696659/miximg_zhxgqh.png"
                            width="100%"
                            height="260"
                            alt=""
                            border="0"
                        />
                        </div>
                        
                        <!--[if mso]>
                            </td>
                            <td style="padding:0px;" align="left" valign="top" width="380">
                        <![endif]-->
                         
                        <div
                        style="display: inline-block; vertical-align: top; width: 100%"
                        class="deviceWidth heightAuto col-lg mix-media__wrapper-text"
                        >
                            <p style="
                                margin: 0;
                                color: #485B7B;
                                font-size: 16px;
                                line-height: 24px;
                                font-family: Arial, Helvetica, sans-serif;
                                text-align: left;
                              "
                                class="white__text_90"
                            > Ut sagittis enim ut risus ultrices, ac consequat quam volutpat. Morbi rutrum euismod ipsum ut ornare. Sed vestibulum hendrerit elit, non placerat nisl consequat ac. </p>
                     
                            <p style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 16px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                            "
                            class="white__text_90"
                            ><img
                                src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                                width="12"
                                height="9"
                                alt=""
                                border="0"
                                style="display: inline-block"
                                class=""
                                /> Ut sagittis enim ut risus ultrices, ac <a href="#" style="color: #005091;"> consequat</a> quam 
                            </p>
                     
                            <p style="
                                margin: 0;
                                color: #485B7B;
                                font-size: 16px;
                                line-height: 24px;
                                font-family: Arial, Helvetica, sans-serif;
                                text-align: left;
                              "
                              class="white__text_90"
                            ><img
                                    src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                                    width="12"
                                    height="9"
                                    alt=""
                                    border="0"
                                    style="display: inline-block"
                                    class=""
                            /> Morbi rutrum euismod ipsum ut ornare</p>
                              
                            <p style="
                                margin: 0;
                                color: #485B7B;
                                font-size: 16px;
                                line-height: 24px;
                                font-family: Arial, Helvetica, sans-serif;
                                text-align: left;
                              "
                              class="white__text_90"
                            ><img
                                    src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                                    width="12"
                                    height="9"
                                    alt=""
                                    border="0"
                                    style="display: inline-block"
                                    class=""
                            /> Sed hendrerit elit, non placerat nisl consequat ac From commitment to </p>
                              
                            <div style="margin-top: 30px;">
                            <!--[if mso]>
                            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                         xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
                                         style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" stroke="f"
                                         fillcolor="#019CDB">
                                <w:anchorlock/>
                                <center>
                            <![endif]-->
                            <a href="http://"
                               style="background-color:#019CDB;
                                      border-radius:25px;
                                      color:#ffffff;
                                      display:inline-block;
                                      font-family:sans-serif;
                                      font-size:13px;
                                      font-weight:bold;
                                      line-height:40px;
                                      text-align:center;
                                      text-decoration:none;
                                      width:175px;
                                      -webkit-text-size-adjust:none;">Download the Toolkit</a>
                            <!--[if mso]>
                            </center>
                            </v:roundrect>
                            <![endif]--></div>
                      </div>
                      
                        <!--[if mso]>
                            </td>
                            </tr>
                        </table>
                        <![endif]-->
                    </td>
                    <td width="20">&nbsp;</td>
                </tr>
            </table>
        </td>
        
    
    </tr>
    <tr>
        <td style="height: 40px;" class="spacing" >&nbsp;</td>
    </tr>
</table>
<!-- END MIX MEDIA BLOCK-->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
`;

    return mixMedia
}
