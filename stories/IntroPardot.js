import './main.css';
export const createIntroPardot = () => {
  const Intro = `
        <!-- INTRO -->
        <table
        role="presentation"
        border="0"
        cellpadding="0"
        cellspacing="0"
        align="center"
        width="100%"
        pardot-removable=""
        pardot-repeatable=""
        style="
          text-align: center;
          max-width: 640px;
          width: 100%;
          margin: auto;
        "
        class="intro"
      >
        <tbody>
          <tr>
            <td width="20">&nbsp;</td>
            <td>
              <table
                role="presentation"
                border="0"
                cellpadding="0"
                cellspacing="0"
                id="snapshot"
              >
                <tr>
                  <td>
                    <h2 pardot-region=""
                      style="
                        color: #005091;
                        font-size: 32px;
                        font-weight: bold;
                        margin: 0;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                    >
                      South Pole Snapshot
                    </h2>
                    <p pardot-region=""
                      style="
                        margin: 0;
                        color: #019CDB;
                        font-size: 14px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                      class="date"
                    >
                      MARCH 2021
                    </p>
                  </td>
                </tr>
                <tr>
                  <td style="padding-top: 35px">
                    <p pardot-region=""
                      style="
                        margin: 0;
                        color: #019CDB;
                        font-size: 18px;
                        font-weight: bold;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                      class="blue"
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing
                      elit. Pellentesque vel enim quis enim interdum
                      viverra ut ut augue. Donec consequat purus sed nunc.
                    </p>
                  </td>
                </tr>
                <tr>
                  <td style="padding-top: 25px; padding-bottom: 25px">
                    <p pardot-region=""
                      style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 16px;
                        line-height: 23px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                      class="grey"
                    >
                      Ut sagittis enim ut <strong>risus ultrices</strong>,
                      ac consequat quam volutpat. Morbi rutrum euismod
                      ipsum ut ornare. Sed
                      <a pardot-region=""
                        href="#"
                        style="
                          text-decoration: underline;
                          color: #005091;
                          font-family: Arial, Helvetica, sans-serif;
                        "
                        >vestibulum</a>
                      hendrerit elit, non
                      <a pardot-region=""
                        href="#"
                        style="
                          text-decoration: underline;
                          color: #005091;
                          font-family: Arial, Helvetica, sans-serif;
                        "
                        >placerat</a>
                      nisl consequat ac. Nunc eget
                      <a pardot-region=""
                        href="#"
                        style="
                          text-decoration: underline;
                          color: #005091;
                          font-family: Arial, Helvetica, sans-serif;
                        "
                        >tempus</a>
                      sem. Duis viverra eu felis suscipit scelerisque.
                    </p>
                  </td>
                </tr>
              </table>
            </td>
            <td width="20">&nbsp;</td>
          </tr>
        </tbody>
      </table>
        <table
        role="presentation"
        border="0"
        cellpadding="0"
        cellspacing="0"
        align="center"
        class="intro link"
      >
        <tbody>
          <tr>
            <td>
              <a pardot-region=""
                href="https://www.southpole.com/"
                style="
                  font-size: 14px;
                  font-weight: bold;
                  color: #005091;
                  text-decoration: none;
                  font-family: Arial, Helvetica, sans-serif;
                "
              >
                Continue
              </a>
            </td>
            <td style="padding-left: 8px">
              <a href="#" pardot-region="">
                <img
                  heigh='8"'
                  width="5"
                  src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621245721/arrow-right_eirwxw.png"
                  alt=""
                  class="icon"
                />
                <!--[if !mso]>-->
                <img
                  heigh='8"'
                  width="5"
                  src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126397/arrow-right_kchzsy.png"
                  alt=""
                  class="icon-dark"
                  style="display: none"
                />
                <!--<![endif]-->
              </a>
            </td>
          </tr>
        </tbody>
      </table>
        <!-- END INTRO -->
    `;

  return Intro
}
