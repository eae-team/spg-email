

export const createPageHTML = () => {
    const section = `
 
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
    <meta charset="utf-8" />
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width" />
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting" />
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>Welcome - [Plain HTML]</title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 14 - 27 can be safely removed. -->

    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
      <style>
        body {
          font-family: Arial, sans-serif !important;
        }
      </style>
    <![endif]-->

    <!-- Web Font / @font-face : END -->

    <!-- CSS Reset -->
    <style>
      /* CLIENT-SPECIFIC STYLES */
      body,
      table,
      td,
      a {
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
      }
      table,
      td {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }
      img {
        -ms-interpolation-mode: bicubic;
      }

      /* RESET STYLES */
      img {
        border: 0;
        line-height: 100%;
        outline: none;
        text-decoration: none;
      }
      body {
        height: 100% !important;
        margin: 0 !important;
        padding: 0 !important;
        width: 100% !important;
        font-family: "Frutiger Next", sans-serif;
        font-weight: 500;
        font-style: normal;
      }

      /* What it does: Centers email on Android 4.4 */
      div[style*="margin: 16px 0"] {
        margin: 0 !important;
      }

      /* What it does: Stops Outlook from adding extra spacing to tables. */
      table,
      td {
        mso-table-lspace: 0pt !important;
        mso-table-rspace: 0pt !important;
      }

      /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
      table {
        border-spacing: 0 !important;
        border-collapse: collapse !important;
        table-layout: fixed !important;
        margin: 0 auto !important;
      }
      table table table {
        table-layout: auto;
      }

      /* What it does: A work-around for email clients meddling in triggered links. */
      *[x-apple-data-detectors],  /* iOS */
            .x-gmail-data-detectors,    /* Gmail */
            .x-gmail-data-detectors *,
            .aBn {
        border-bottom: 0 !important;
        cursor: default !important;
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
      }

      body[data-outlook-cycle] .hidden {
        display: none !important;
      }

      /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
      .a6S {
        display: none !important;
        opacity: 0.01 !important;
      }
      /* If the above doesn't work, add a .g-img class to any image in question. */
      img.g-img + div {
        display: none !important;
      }

      /* What it does: Prevents underlining the button text in Windows 10 */
      .button-link {
        text-decoration: none !important;
      }

      [data-imagetype="External"] {
        display: block !important;
      }

      .img-mobile {
        display: none !important;
      }

      .img-mobile,
      .img-mobile img {
        width: 100% !important;
      }

      @media only screen and (max-width: 600px) {
        .col-sml {
          width: 38.319% !important;
        }
        .col-lge {
          width: 61.681% !important;
        }

        table.footer__top > tbody > tr > td {
          padding: 28px 35px 20px !important;
        }

        table.footer__bottom > tbody > tr > td {
          padding: 32px 35px !important;
        }

        .footer__logo img {
          margin: 0 auto;
        }

        .footer__col-lge {
          width: 370px !important;
          margin-bottom: 30px !important;
        }

        .footer__col-sml {
          width: 190px !important;
        }

        .footer__col-lge,
        .footer__col-sml {
          text-align: center;
        }

        .footer__col-lge h3,
        .footer__col-sml h3 {
          width: 100% !important;
        }

        .footer__col-sml .deviceWidth {
          text-align: center !important;
        }

        .footer__disclaimer {
          padding: 20px !important;
        }
        .spacing {
          line-height: 45px !important;
          height: 45px !important;
        }


        h1 {
          font-size: 29px !important;
          line-height: 33px !important;
        }

        .header__title {
          padding: 70px 20px 0 !important;
        }

        .header__text {
          padding: 0 20px 54px !important;
        }

        .header__image {
          padding-top: 0 !important; 
        }

        .header__content p {
          font-size: 17px !important;
          line-height: 28px !important;
        }

        .deviceWidth {
          width: 100% !important;
        }

        .intro,
        .p-mobile {
          padding: 0 20px;
        }
        .intro table {
          text-align: center !important;
        }
        .intro h2 {
          font-size: 28px !important;
          line-height: 32px !important;
        }

        .intro .blue {
          font-size: 16px !important;
          line-height: 21px !important;
        }

        .intro .grey {
          font-size: 14px !important;
          line-height: 21px !important;
        }

        .intro .date {
          font-size: 12px !important;
        }

        .heightAuto {
          height: auto !important;
        }

        .navigation {
          padding: 0 15px !important;
        }

        .navigation-p-h {
          width: 10px !important;
        }
        .south-pole-link {
          width: 69px !important;
        }
        .news-link {
          width: 38px !important;
        }
        .events-link {
          width: 46px !important;
        }
        .news-blue-bar {
          height: 170px !important;
        }

        .news-container-td {
          padding-left: 20px;
        }

        .event__date {
          width: 116px !important;
        }

        .event__content h3 {
          font-size: 18px !important;
        }

        .article .img {
          width: 76px !important;
          vertical-align: top;
        }

        .article .img table tr td {
          vertical-align: top;
        }

        .article .img img {
          height: 66px !important;
        }
        .article .article__content {
          padding: 0 0 0 10px !important;
        }

        .article .article__content h4 {
          font-size: 16px !important;
        }

        .hidden {
          display: none !important;
        }
        .img-mobile {
          display: block !important;
        }
        
        .news-cols-sm-img-text .news-cols-sm-img-text__image {
            margin-left: 30px !important;
        }
        .news-cols-sm-img-text .news-cols-sm-img-text__text {
            margin-left: 30px !important;
            width: 90% !important;
        }

        .events-cols-sm-img-text .events-cols-sm-img-text__image{
            margin-left: 30px !important;
        }
        .events-cols-sm-img-text .events-cols-sm-img-text__text {
            margin-left: 30px !important;
            width: 90% !important;
        }
        
        /* projects highlight */
        .projects-bigger-title {
            font-size: 59px !important;
            line-height: 68px !important;
        }
    
        .projects-horizontal-top-bar {
            width: 375px !important;
            height: 10px !important;
        }
    
        .projects-vertical-right-bar {
            width: 10px !important;
            height: 170px !important;
        }
        
        .news-cols-sm-img-text .col-lg,
        .events-cols-sm-img-text .col-lg {
            width: calc(100% - 150px) !important;
        }
    
        /** FOOTER **/
        .footer__logo {
            display: block !important;
            width: auto !important;
            margin: 0 !important;
        }
    
        .footer__logo img {
            margin: auto !important;
        }
    
        .footer__socials {
            display: block !important;
            width: auto !important;
            margin-top: 40px !important;
        }
    
        .footer__socials__logos {
            width: auto !important;
        }
    
        .footer__moto {
            width: calc(100% - 40px) !important;
            margin: 0 20px !important;
            text-align: center;
        }
    
        .footer__address {
            width: 100% !important;
            margin-top: 20px !important;
            margin-left: auto !important;
        }
    
        .footer__address-table {
            width: 40% !important;
            text-align: center;
        }
    
        .footer__disclaimer {
            font-size: 12px !important;
            padding: 20px !important;
        }
    
        .news-cols-sm-img-text .news-cols-sm-img-text__image {
            margin-left: 30px !important;
        }
        .news-cols-sm-img-text .news-cols-sm-img-text__text {
            margin-left: 30px !important;
            width: 90% !important;
        }
    
        .events-cols-sm-img-text .events-cols-sm-img-text__image{
            margin-left: 30px !important;
        }
        .events-cols-sm-img-text .events-cols-sm-img-text__text {
            margin-left: 30px !important;
            width: 90% !important;
        }
    
      }

      .logo-white {
        display: none !important;
      }

      .icon-dark {
        display: none !important;
      }

      u + .body .img-mobile {
        display: none !important;
      }

      u + .body .icon-dark {
        display: none !important;
      }

      @media (prefers-color-scheme: dark) {
        
        .logo,
        .icon {
          display: none !important;
        }

        .logo-white,
        .icon-dark {
          display: block !important;
        }

        .intro h2,
        .intro .blue,
        .intro .grey,
        .intro .date,
        .news-title,
        .highlight-title,
        .article__content h4,
        .footer__disclaimer p,
        .footer__disclaimer a,
        .view-in-browser a,
        .events td {
          color: white !important;
        }

        .highlight-pretitle,
        .highlight-text,
        .article__content p {
          color: #c2c5cb !important;
        }

        .intro a,
        .intro.link,
        .intro .link td,
        .article__content a,
        .events a,
        .news-container-td a {
          color: #73acdd !important;
        }

        .wrapper{
          background: #111a22 !important;
        }

        .view-in-browser {
          background: #7b828f !important;
        }

        .events {
          background: #000000 !important;
        }
        .col-green {
          background: #98c21f !important;
          color: #1e355c !important;
        }
        .col-yellow {
          background: #fdc300 !important;
          color: #1e355c !important;
        }
        .col-yellow h4,
        .col-yellow p,
        .col-green h4,
        .col-green p {
          color: #1e355c !important;
        }

        .event__content {
          background-color: #ffffff !important;
        }

        .event__content h3,
        .event__content p,
        .event__content a {
          color: #005091 !important;
        }

        .article-border-b {
          border-color: #3b444d !important;
        }
      }
    </style>

    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG />
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
    <![endif]-->
  </head>
  <body>
  </body>
  </html>
`;

    return section;
};
