import './main.css';

export const createListLink = () => {
	const listLink = `
<!-- LINK -->
<table
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class=""
>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="text-align: center">
            <h2
                    style="
              margin: 0;
              font-size: 52px;
              line-height: 52px;
              color: #005091;
              font-family: Arial, Helvetica, sans-serif;
            "
            >
                In the News
            </h2>
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:55px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 55px"
                    class="heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="55"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        height="55"
                                        width="55"
                                        class="img"
                                        style="
                        background-color: #019CDB;
                        height: 55px;
                        width: 55px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="27"
                                                height="55"
                                                class="col-box"
                                                style="
                            width: 27px;
                            height: 55px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                        >
                                            
                                        </td>
                                        <td align="right" valign="bottom" width="28" height="55" class="col-wrapper">
                                            <table cellpadding="0" cellspacing="0" valign="bottom" height="45">
                                                <tr>
                                                    <td width="28" height="45"
                                                            style="
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-overlay"
                                                    >
                                                        &nbsp;
                                                       <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 28px;
                                height: 45px;
                                position: absolute;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-text"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 25px; height: 25px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 18px;
                    line-height: 24px;
                    font-weight: bold;
                  "
                  class="events__title"
                        >
                            Sustainable Finance: Impact and Implications for the Real
                            Economy
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    line-height: 18px;
                  "
                  class="events__sub-title"
                        >
                            Wed, 12 May — Online: 18:00 - 20:00 CET
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:55px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 55px"
                    class="heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="55"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="55"
                                        height="55"
                                        class="img"
                                        style="
                        background-color: #019CDB;
                        height: 55px;
                        width: 55px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="27"
                                                height="55"
                                                class="col-box"
                                                style="
                            width: 27px;
                            height: 55px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                        >
                                            
                                        </td>
                                        <td align="right" valign="bottom" width="28" height="55" class="col-wrapper">
                                            <table cellpadding="0" cellspacing="0" valign="bottom" height="45">
                                                <tr>
                                                    <td width="28" height="45"
                                                            style="
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-overlay"
                                                    >
                                                        &nbsp;
                                                     
                                            <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 28px;
                                height: 45px;
                                position: absolute;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-text"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 25px; height: 25px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 18px;
                    line-height: 24px;
                    font-weight: bold;
                  "
                   class="events__title"
                        >
                            Sustainable Finance: Impact and Implications for the Real
                            Economy
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    line-height: 18px;
                  "
                   class="events__sub-title"
                        >
                            Wed, 12 May — Online: 18:00 - 20:00 CET
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>

                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END EVENTS -->
<!-- space -->
<table cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
  `;

	return listLink;
};
