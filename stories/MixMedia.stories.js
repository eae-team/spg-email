import { createMixMedia} from './MixMedia';

export default {
  title: 'Blocks/MixMedia',
};

const Template = () => createMixMedia();

export const MixMediaComponent = Template.bind({});
