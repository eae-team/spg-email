import { createTeaserTwoImagesPardot } from './Teaser-Two-ImagesPardot';

export default {
  title: 'Pardot Tags/Teaser',
};

const Template = () => createTeaserTwoImagesPardot();

export const TeaserTwoImagesComponent = Template.bind({});

