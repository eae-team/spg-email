import {createQuoteBlockPardot} from './QuoteBlockPardot';

export default {
  title: 'Pardot Tags/Quote Block',
};

const Template = () => createQuoteBlockPardot();

export const QuoteBlockComponent = Template.bind({});
