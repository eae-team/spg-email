import { createPageSpecial } from './PageSpecial';

export default {
  title: 'Pages/Special',
};

const Template = () => createPageSpecial();

export const PageComponent = Template.bind({});

