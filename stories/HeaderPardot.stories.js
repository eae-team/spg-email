import { createHeaderPardot } from './HeaderPardot';

export default {
  title: 'Pardot Tags/Header/Logo and Title',
};

const Template = () => createHeaderPardot();

export const HeaderComponent = Template.bind({});

