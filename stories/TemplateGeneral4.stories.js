import { createTemplateGeneral4 } from './TemplateGeneral4';

export default {
  title: 'Pardot Templates/Template General 4',
};

const Template = () => createTemplateGeneral4();

export const templateGeneral4Component = Template.bind({});

