import { createTemplateSurveyPardot } from './TemplateSurveyPardot';

export default {
  title: 'Pardot Templates/Template Survey',
};

const Template = () => createTemplateSurveyPardot();

export const templateSurveyComponent = Template.bind({});