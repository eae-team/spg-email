import './main.css';

export const createListBullets = () => {
    const listBullets = `
<!-- BULLETS -->
<table
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="events-cols-sm-img-text"
>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
   
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:145px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 145px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__image">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="145"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="145"
                                        class="img"
                                        style="
                        background-color: #98C21F;
                        height: 105px;
                        width: 145px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="110px"
                                                class="col-lg"
                                                style="
                            width: 95px;
                            height: 90px;
                            padding-left: 10px;
                            padding-bottom: 15px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                                align="left"
                                        >
                                            <table
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    border="0"
                                                    valign="bottom"
                                                    style="width: 100%;"
                                            >
                                                <tr>
                                                    <td
                                                            style="
                                  color: #ffffff;
                                  font-family: Arial, Helvetica, sans-serif;
                                  font-size: 12px;
                                  font-weight: bold;
                                "
                                                    >
                                                        <p
                                                                style="
                                    font-size: 45px;
                                    line-height: 26px;
                                    margin-bottom: 8px;
                                    margin-top: 0;
                                    color: #ffffff;
                                    font-weight: bold;
                                  "
                                                        >
                                                            1
                                                        </p>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" valign="bottom">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td
                                                            style="
                                  width: 40px;
                                  height: 85px;
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-sm"
                                                    >
                                                        &nbsp;
                                                       <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 40px;
                                height: 85px;
                                position: absolute;
                                top: 20px;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 16px;
                    line-height: 22px;
                  "
                  class="white__text"
                        >
                           The transport industry is responsible for 13% of global direct greenhouse gas emissions 13% paesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh. Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas.
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                 
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:145px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 145px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__image">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="145"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="145"
                                        class="img"
                                        style="
                        background-color: #F39200;
                        height: 105px;
                        width: 145px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="110px"
                                                class="col-lg"
                                                style="
                            width: 95px;
                            height: 90px;
                            padding-left: 10px;
                            padding-bottom: 15px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                                align="left"
                                        >
                                            <table
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    border="0"
                                                    valign="bottom"
                                                    style="width: 100%;"
                                            >
                                                <tr>
                                                    <td
                                                            style="
                                  color: #ffffff;
                                  font-family: Arial, Helvetica, sans-serif;
                                  font-size: 12px;
                                  font-weight: bold;
                                "
                                                    >
                                                        <p
                                                                style="
                                    font-size: 45px;
                                    line-height: 26px;
                                    margin-bottom: 8px;
                                    margin-top: 0;
                                    color: #ffffff;
                                    font-weight: bold;
                                  "
                                                        >
                                                           2
                                                        </p>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" valign="bottom">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td
                                                            style="
                                  width: 40px;
                                  height: 85px;
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-sm"
                                                    >
                                                        &nbsp;
                                                       <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 40px;
                                height: 85px;
                                position: absolute;
                                top: 20px;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 16px;
                    line-height: 22px;
                  "
                  class="white__text"
                        >
                           The transport industry is responsible for 13% of global direct greenhouse gas emissions 13% paesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh. Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas.
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                 
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>

    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:145px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 145px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__image">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="145"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="145"
                                        class="img"
                                        style="
                        background-color: #019CDB;
                        height: 105px;
                        width: 145px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="110px"
                                                class="col-lg"
                                                style="
                            width: 95px;
                            height: 90px;
                            padding-left: 10px;
                            padding-bottom: 15px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                                align="left"
                                        >
                                            <table
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    border="0"
                                                    valign="bottom"
                                                    style="width: 100%;"
                                            >
                                                <tr>
                                                    <td
                                                            style="
                                  color: #ffffff;
                                  font-family: Arial, Helvetica, sans-serif;
                                  font-size: 12px;
                                  font-weight: bold;
                                "
                                                    >
                                                        <p
                                                                style="
                                    font-size: 45px;
                                    line-height: 26px;
                                    margin-bottom: 8px;
                                    margin-top: 0;
                                    color: #ffffff;
                                    font-weight: bold;
                                  "
                                                        >
                                                            3
                                                        </p>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" valign="bottom">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td
                                                            style="
                                  width: 40px;
                                  height: 85px;
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-sm"
                                                    >
                                                        &nbsp;
                                                       <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 40px;
                                height: 85px;
                                position: absolute;
                                top: 20px;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 16px;
                    line-height: 22px;
                  "
                  class="white__text"
                        >
                           The transport industry is responsible for 13% of global direct greenhouse gas emissions 13% paesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh. Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas.
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                 
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END BULLETS -->
<!-- space -->
<table cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
  `;

    return listBullets;
};
