import {createEditorBlock, createPartnersPardot} from './PartnersPardot';

export default {
  title: 'Pardot Tags/Partners',
};

const Template = () => createPartnersPardot();

export const PartnersComponent = Template.bind({});
