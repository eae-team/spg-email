import './main.css';

export const createQuoteBlockPardot = () => {
    const quoteBlock = `
<!-- QUOTE BLOCK-->
<table cellpadding="20"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background-color: #005091"
       pardot-repeatable=""
       class=""
       align="center">

    <tr>
        <td align="center">
            <h2 pardot-region=""
                style="
                color: #FFFFFF;
                font-size: 18px;
                line-height: 24px;
                font-weight: bold;
                margin: 0;
                font-family: Arial, Helvetica, sans-serif;
                text-align: center;
                "
     
            >
            The previous version of this media release erroneously used the term carbon grazing to refer to the breed cattle heard management method of carbon abatement.
            </h2>
            <br>
           <p pardot-region="" style="
                color: #FFFFFF;
                font-size: 61px;
                line-height: 24px;
                font-weight: bold;
                margin: 0;
                font-family: Times, 'Times New Roman', sans-serif;
                text-align: center;
                ">&#8220;</p>
        </td>
    </tr>

</table>
<!-- END QUOTE BLOCK-->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->

`;

    return quoteBlock
}
