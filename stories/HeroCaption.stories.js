import {createHeroCaption} from './HeroCaption';

export default {
  title: 'Blocks/Hero/Caption',
};

const Template = () => createHeroCaption();

export const HeroComponent = Template.bind({});

