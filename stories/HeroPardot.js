import './main.css';

export const createHeroPardot = () => {
    const header = `
<!-- HERO -->
<table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       class="header"
       pardot-repeatable=""
       align="center">
    <tr>
        <td>
            <!-- HEADER-HERO : BEGIN -->
            <table
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    border="0"
                    width="100%"
                    style="
                      max-width: 640px;
                      width: 100%;
                      margin: auto;
                      text-align: center;
                    "
                    class="deviceWidth"
            >
                <tbody>
                <tr>
                    <td
                            background="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621857015/logo_dbsewx_d7b0sc.jpg"
                            width="640"
                            height="355"
                            pardot-region=""
                            valign="top"
                            style="background-repeat: no-repeat; background-size: cover"
                            class="deviceWidth header"
                    >
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                style="width:640px;height:355px;">
                            <v:fill type="frame"
                                    pardot-region=""
                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621857015/logo_dbsewx_d7b0sc.jpg"
                                    color="#005091"/>
                            <v:textbox inset="0,0,0,0">
                        <![endif]-->
                        <div>
                            <table
                                    role="presentation"
                                    border="0"
                                    cellpadding="0"
                                    cellspacing="0"
                                    align="center"
                                    width="100%"
                                    style="max-width: 500px; margin: auto"
                            >
                                <tbody>
                                <tr>
                                    <td align="center" valign="middle">
                                        <table
                                                role="presentation"
                                                border="0"
                                                cellpadding="0"
                                                cellspacing="0"
                                                align="center"
                                                width="100%"
                                                class="header__content"
                                        >
                                            <tbody>
                                            <tr>
                                                <td
                                                        valign="top"
                                                        style="
                                  text-align: center;
                                  padding: 78px 40px 0;
                                  text-align: center;
                                "
                                                        class="header__title"
                                                >
                                                    <h1 pardot-region=""
                                                            style="
                                    margin: 0;
                                    font-size: 40px;
                                    line-height: 42px;
                                    color: #ffffff;
                                    font-weight: bold;
                                    font-family: Arial, Helvetica,
                                      sans-serif;
                                  "
                                                    >
                                                        How can we support your climate journey?
                                                    </h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                        valign="top"
                                                        style="
                                  text-align: center;
                                  padding: 5px 60px 77px 60px;
                                  font-size: 22px;
                                  line-height: 28px;
                                  color: #ffffff;
                                  font-family: Arial, Helvetica,
                                    sans-serif;
                                  text-align: center;
                                "
                                                        class="header__text" pardot-region=""
                                                >
                                                    <p style="margin: 0">
                                                        Welcome to 2021 - we hope you’re ready
                                                        for a climate action-packed year!
                                                    </p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <!--[if gte mso 9]>
                        </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
</table>

`;

    return header;
};
