import {createHeaderViewOnBrowser} from './HeaderViewOnBrowser';
import {createHeaderLogo} from './HeaderLogo';
import {createHeroInfo} from './HeroInfo';
import {createTeaserTwoImages} from './Teaser-Two-Images';
import {createFooter} from './Footer';
import {createDisclaimer} from './Disclaimer';
import {createText} from './Text';
import {createHighlight} from './Highlight';


export const createPageLayout = () => {
    const viewOnBrowser = createHeaderViewOnBrowser();
    const header = createHeaderLogo();
    const hero = createHeroInfo();
    const teaserTwoImages = createTeaserTwoImages();
    const footer = createFooter();
    const disclaimer = createDisclaimer();
    const text = createText();
    const highlight = createHighlight();


    const section = `
 
                        ${viewOnBrowser}
                        ${header}
                        ${hero}
                      
                        ${text}
                        
                        ${highlight}
                        ${teaserTwoImages}
                        
                        ${footer}
                        ${disclaimer}


`;

    return section;
};
