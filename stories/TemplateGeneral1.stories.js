import { createTemplateGeneral1 } from './TemplateGeneral1';

export default {
  title: 'Pardot Templates/Template General 1',
};

const Template = () => createTemplateGeneral1();

export const templateGeneral1Component = Template.bind({});

