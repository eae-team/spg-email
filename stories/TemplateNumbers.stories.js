import { createTemplateNumbers } from './TemplateNumbers';

export default {
  title: 'Pardot Templates/Template Numbers',
};

const Template = () => createTemplateNumbers();

export const templateNumbersComponent = Template.bind({});

