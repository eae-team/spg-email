import './main.css';

export const createTemplateClimateHuddle1 = () => {
    const climateHuddle1 = `
    <!-- VIEW IN BROWSER -->
<!--  PREVIEW TEXT HERE -->
<div
    style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif !important; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;"
>
    This is the preview text
</div>
<!-- END PREVIEW TEXT -->
<table align="center" border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td style="padding: 20px 0">
                <a
                    href="{{View_Online}}"
                    pardot-region=""
                    style="
color: #3e3e3e;
font-size: 11px;
line-height: 14px;
font-family: Helvetica, Arial, sans-serif !important;
"
                    >View in browser
                </a>
            </td>
        </tr>
    </tbody>
</table>
<!-- END VIEW IN BROWSER -->
<!-- HEADER TWO LOGOS -->
<table
    cellpadding="0"
    cellspacing="0"
    border="0"
    style="max-width: 640px; width: 100%; background: #F3F3FF"
    class="header"
    align="center"
>
    <tbody>
        <tr>
            <td style="padding: 0 15px;">
                <table
                    align="center"
                    cellpadding="0"
                    cellspacing="0"
                    border="0"
                >
                    <tbody>
                        <tr>
                            <td style=" padding-right: 18px">
                                <a
                                    href="https://www.southpole.com/"
                                    target="_blank"
                                >
                                    <img
                                        src="https://go.southpole.com/l/881182/2022-07-14/37kxk/881182/1657852599YDZt0VtA/ClimateHuddle_logo.png"
                                        width="148"
                                        height="48"
                                        alt=""
                                        border="0"
                                        style="display: block"
                                        class="logo"
                                    />
                                    <!--[if !mso]>-->
                                    <img
                                        src="https://go.southpole.com/l/881182/2022-07-12/36nwy/881182/1657628296NmDS3aop/ClimateHuddle_Logo_white.png"
                                        width="148"
                                        height="48"
                                        alt=""
                                        border="0"
                                        style="display: none"
                                        class="logo-white"
                                    />
                                    <!--<![endif]-->
                                </a>
                            </td>
                            <td
                                style="
font-size: 12px;
font-family: Helvetica, Arial, sans-serif !important;
color: #005091;
padding-left: 18px;
"
                                class="header-content"
                            >
                                <img
                                    src="https://go.southpole.com/l/881182/2022-07-25/3cwbv/881182/16587391567AoMggZ5/climate_huddle_header.png"
                                    width="185"
                                    height="105"
                                    alt=""
                                    border="0"
                                    style="display: block"
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- END HEADER TWO LOGOS -->

<!-- HERO INFO -->
<table
    cellpadding="0"
    cellspacing="0"
    border="0"
    style="max-width: 640px; width: 100%; background: #ffffff"
    pardot-repeatable=""
    class="header"
    align="center"
>
    <tbody>
        <tr>
            <td style="height: 50px" class="spacing">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    width="100%"
                    style="
text-align: center;
width: 100%;
margin: auto;
"
                    class="intro"
                >
                    <tbody>
                        <tr>
                            <td
                                class="intro-text"
                                style="padding: 0px 40px"
                            >
                                <table
                                    role="presentation"
                                    border="0"
                                    cellpadding="0"
                                    cellspacing="0"
                                    id="snapshot"
                                >
                                    <tbody>
                                        <tr>
                                            <td pardot-region="">
                                                <h2
                                                    style="
color: #FD4D00;
font-size: 44px;
line-height: 27px;
font-weight: bold;
margin: 0;
font-family: Helvetica, Arial, sans-serif !important;
text-align: center;
"
                                                >
                                                    Join our webinar
                                                </h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 30px 15px 0;">
                                                <p
                                                    style="
margin: 0;
color: #3e3e3e;
font-size: 18px;
line-height: 24px;
font-family: Helvetica, Arial, sans-serif !important;
text-align: center;
"
                                                    class="blue"
                                                >
                                                    Lorem ipsum dolor sit amet,
                                                    consectetur adipiscing elit,
                                                    sed do eiusmod tempor
                                                    incididunt ut labore et
                                                    dolore magna aliqua. Ut enim
                                                    ad minim veniam, quis
                                                    nostrud exercit
                                                    <br />
                                                    <br />
                                                    <img
                                                        src="https://go.southpole.com/l/881182/2022-07-25/3cw9k/881182/1658738522rw7V1ZNf/clock.png"
                                                        height="20"
                                                        width="20"
                                                    />&nbsp;&nbsp;&nbsp;<strong
                                                        >Thursday, April 28,
                                                        2022 from 6:30 PM to
                                                        9:30 PM (CEST)</strong
                                                    >
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    class=""
                >
                    <tbody>
                        <tr>
                            <td>
                                <div style="margin-top: 30px; ">
                                    <!--[if mso]>
    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                 xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
                 style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" stroke="f"
                 fillcolor="#0061FC">
        <w:anchorlock/>
        <center>
    <![endif]-->
                                    <a
                                        pardot-region=""
                                        pardot-removable=""
                                        href="http://"
                                        style="padding:5px 0px;background-color:#0061FC;border-radius:20px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:18px;font-weight:normal;line-height:40px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;"
                                        >Register now</a
                                    >
                                    <!--[if mso]>
    </center>
    </v:roundrect>
    <![endif]-->
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 50px" class="spacing">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END HERO -->

<!-- space -->
<table
    class="bg-table"
    cellpadding="0"
    cellspacing="0"
    border="0"
    align="center"
>
    <tbody>
        <tr>
            <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END space -->

<!-- space -->
<table
    class="bg-table"
    cellpadding="0"
    cellspacing="0"
    border="0"
    align="center"
>
    <tbody>
        <tr>
            <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END space -->

<!-- HERO INFO -->
<table
    cellpadding="0"
    cellspacing="0"
    border="0"
    style="max-width: 640px; width: 100%; background: #ffffff"
    class="header"
    align="center"
>
    <tbody>
        <tr>
            <td>
                <!-- HEADER-HERO : BEGIN -->
                <table
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    border="0"
                    width="100%"
                    style="
max-width: 640px;
width: 100%;
margin: auto;
text-align: center;
"
                    class="deviceWidth"
                >
                    <tbody>
                        <tr>
                            <td
                            >
                                <img width="100%" src="https://go.southpole.com/l/881182/2022-07-25/3cw92/881182/1658737756iJ2FaigZ/climate_huddle_banner_1.png">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- END HERO INFO -->

<table
    cellpadding="0"
    cellspacing="0"
    border="0"
    style="max-width: 640px; width: 100%; background: #F6F6F6"
    pardot-repeatable=""
    class="header"
    align="center"
>
    <tbody>
        <tr>
            <td style="height: 50px" class="spacing">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="left"
                    width="100%"
                    style="
                       max-width: 640px;
text-align: center;
width: 100%;
margin: auto;
"
                    class=""
                >
                    <tbody>
                        <tr>
                            <td
                                class="intro-text-padding"
                                style="padding: 0px 40px"
                            >
                                <table
                                    role="presentation"
                                    border="0"
                                    cellpadding="0"
                                    cellspacing="0"
                                    id="snapshot"
                                >
                                    <tbody>
                                        <tr>
                                            <td pardot-region="">
                                                <h2
                                                    style="
color: #FD4D00;
font-size: 21px;
line-height: 27px;
font-weight: bold;
margin: 0;
font-family: Helvetica, Arial, sans-serif !important;
text-align: left;
"
                                                >
                                                    Heading title here
                                                </h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 30px">
                                                <p
                                                    style="
margin: 0;
color: #3e3e3e;
font-size: 18px;
line-height: 24px;
font-family: Helvetica, Arial, sans-serif !important;
text-align: left;
"
                                                    class="blue"
                                                >
                                                    Lorem ipsum dolor sit amet,
                                                    consectetur adipiscing elit,
                                                    sed do eiusmod tempor
                                                    incididunt ut labore et
                                                    dolore magna aliqua. Ut enim
                                                    ad minim veniam, quis
                                                    nostrud exercit<br /><br />

                                                    <img
                                                        src="https://go.southpole.com/l/881182/2022-07-25/3cw9k/881182/1658738522rw7V1ZNf/clock.png"
                                                        height="20"
                                                        width="20"
                                                    />&nbsp;&nbsp;&nbsp;<strong
                                                        >Thursday, April 28,
                                                        2022 from 6:30 PM to
                                                        9:30 PM (CEST)</strong
                                                    >
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="left"
                    class=""
                >
                    <tbody>
                        <tr>
                            <td class="intro-text-padding" style="padding: 0 40px">
                                <div
                                    style="margin-top: 30px;"
                                >
                                    <!--[if mso]>
    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                 xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
                 style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" stroke="f"
                 fillcolor="#0061FC">
        <w:anchorlock/>
        <center>
    <![endif]-->
                                    <a
                                        pardot-region=""
                                        pardot-removable=""
                                        href="http://"
                                        style="padding:5px 0px; background-color:#0061FC;border-radius:20px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:18px;font-weight:normal;line-height:40px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;"
                                        >Find out more</a
                                    >
                                    <!--[if mso]>
    </center>
    </v:roundrect>
    <![endif]-->
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 50px" class="spacing">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END HERO -->

<!-- space -->

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    pardot-repeatable=""
>
    <tbody>
        <tr>
            <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END space --><!-- EN --><!-- FOOTER -->

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    class="footer-table--black"
    pardot-removable=""
    style="max-width: 640px; width: 100%; background: #0061FC"
>
    <tbody>
        <tr class="space">
            <td style="line-height: 20px; height: 20px">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table
                    align="center"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    style="width: 100%; background: #0061FC;"
                >
                    <tbody>
                        <tr>
                            <td align="center">
                                <!--[if mso]>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding:0px;" align="left" valign="top" width="340">
<![endif]-->
                                <div
                                    class="footer__logo"
                                    style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
                                >
                                    <img
                                        alt=""
                                        border="0"
                                        height="40"
                                        pardot-region=""
                                        src="https://go.southpole.com/l/881182/2022-07-12/36nwy/881182/1657628296NmDS3aop/ClimateHuddle_Logo_white.png"
                                        style="display: block"
                                        width="141"
                                    />
                                </div>
                                <!--[if mso]>
</td>
<td style="padding:0px;" align="left" valign="top" width="240">
<![endif]-->

                                <div
                                    class="footer__socials"
                                    style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                >
                                    <table
                                        align="center"
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        class="footer__socials__logos"
                                        style="width: 100%; background: #0061FC;"
                                    >
                                        <tbody>
                                            <tr>
                                                <td
                                                    pardot-region=""
                                                    style=" width: 90px; color: #ffffff;
font-family: 'DM Sans', sans-serif;
font-size: 14px;
font-weight: bold;
line-height: 18px;"
                                                    width="90"
                                                >
                                                    Get in touch
                                                </td>
                                                <td
                                                    align="center"
                                                    style="width: 25px;"
                                                    width="25"
                                                >
                                                    <a
                                                        href="https://www.facebook.com/southpoleglobal/"
                                                        ><img
                                                            alt=""
                                                            border="0"
                                                            height="18"
                                                            src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
                                                            style="display: block"
                                                            width="8"
                                                    /></a>
                                                </td>
                                                <td
                                                    align="center"
                                                    style="width: 30px;"
                                                    width="30"
                                                >
                                                    <a
                                                        href="https://www.linkedin.com/company/south-pole-global/"
                                                        ><img
                                                            alt=""
                                                            border="0"
                                                            height="18"
                                                            src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
                                                            style="display: block"
                                                            width="18"
                                                    /></a>
                                                </td>
                                                <td
                                                    align="center"
                                                    style="width: 30px;"
                                                    width="30"
                                                >
                                                    <a
                                                        href="https://www.instagram.com/southpoleglobal/"
                                                        ><img
                                                            alt=""
                                                            border="0"
                                                            height="18"
                                                            src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
                                                            style="display: block"
                                                            width="18"
                                                    /></a>
                                                </td>
                                                <td
                                                    align="center"
                                                    style="width: 30px;"
                                                    width="30"
                                                >
                                                    <a
                                                        href="https://twitter.com/southpoleglobal"
                                                        ><img
                                                            alt=""
                                                            border="0"
                                                            height="18"
                                                            src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
                                                            style="display: block"
                                                            width="18"
                                                    /></a>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--[if mso]>
</td>
</tr>
</table>
<![endif]-->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr class="space">
            <td style="line-height: 20px; height: 20px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- FOOTER BOTTOM -->

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    class="footer-table-bottom--black"
    pardot-removable=""
    style="max-width: 640px; width: 100%; background: #0061FC"
>
    <tbody>
        <tr class="space">
            <td style="line-height: 30px; height: 30px">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table
                    align="center"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    style="width: 100% !important;"
                >
                    <tbody>
                        <tr>
                            <td align="left">
                                <!--[if mso]>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
<![endif]-->
                                <div
                                    class="footer__moto"
                                    style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                >
                                    <h2
                                        pardot-region=""
                                        style="font-family: Helvetica, Arial, sans-serif !important;
                                font-size: 18px;
                                line-height: 21px;
                                font-weight: bold;
                                color: #ffffff;
                                margin: 0;"
                                    >
                                        If you have any questions, email us at
                                        <a
                                            href="mailto:hello@climatehuddle.com"
                                            style="color: #ffffff;"
                                            >hello@climatehuddle.com</a
                                        >.
                                    </h2>
                                </div>
                                <!--[if mso]>
</td>

<td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
<![endif]-->

                                <div
                                    class="footer__address"
                                    style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                >
                                    <table
                                        align="center"
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        class="footer__address-table"
                                        style="width: 100%;"
                                    >
                                        <tbody>
                                            <tr style="">
                                                <td
                                                    pardot-region=""
                                                    style="font-family: Helvetica, Arial, sans-serif !important;
font-size: 14px;
line-height: 18px;
font-weight: bold;
color: #ffffff;"
                                                >
                                                    South Pole Global
                                                    Headquarters
                                                </td>
                                            </tr>
                                            <tr style="">
                                                <td
                                                    pardot-region=""
                                                    style="font-family: Helvetica, Arial, sans-serif !important;
font-size: 14px;
line-height: 18px; color: #FFFFFF; padding-top: 10px;"
                                                >
                                                    Technoparkstrasse 1 Zurich
                                                    8005 Switzerland
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--[if mso]>
</td>
</tr>
</table>
<![endif]-->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr class="space">
            <td style="line-height: 35px; height: 35px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- END FOOTER-->
<!-- DISCLAIMER -->

<table
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    pardot-removable=""
   class="disclaimer-bg"
    style="max-width: 510px; width: 100%;"
>
    <tbody>
        <tr class="space">
            <td style="line-height: 20px; height: 20px">&nbsp;</td>
        </tr>
        <tr style="">
            <td
                align="center"
                class="footer__disclaimer"
                pardot-region=""
                style="text-align: center; color:#000000;
font-family: Helvetica, Arial, sans-serif !important;
font-size: 12px;
line-height: 14px;"
            >
                © 2022 South Pole |
                <a
                    href="{{Unsubscribe}}"
                    style="
text-decoration: underline;
color: #000000;
font-family: Arial, Helvetica,
sans-serif;
"
                    >Unsubscribe</a
                >.
            </td>
        </tr>
        <tr class="space">
            <td style="line-height: 20px; height: 20px">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- DISCLAIMER -->
    `;
    return climateHuddle1;
}