import {createEditorBlock, createPartners} from './Partners';

export default {
  title: 'Blocks/Partners',
};

const Template = () => createPartners();

export const PartnersComponent = Template.bind({});
