import { createDisclaimerPardotSW } from './DisclaimerPardotSW';

export default {
  title: 'Pardot Tags/Disclaimer/Disclaimer EN (Switzerland)',
};

const Template = () => createDisclaimerPardotSW();

export const DisclaimerComponent = Template.bind({});