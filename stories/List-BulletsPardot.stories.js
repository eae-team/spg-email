import { createListBulletsPardot } from './List-BulletsPardot';

export default {
  title: 'Pardot Tags/Bullets/Number',
};

const Template = () => createListBulletsPardot();

export const ListBulletsComponent = Template.bind({});
