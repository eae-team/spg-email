/*import {createHeaderViewOnBrowserPardot} from './HeaderViewOnBrowserPardot';
import {createHeaderLogoPardot} from './HeaderLogoPardot';
import {createHeroPardot} from './HeroPardot';
import {createHeroImagePardot} from './HeroImagePardot';
import {createHeroInfoPardot} from './HeroInfoPardot';
import {createHeroCaptionPardot} from './HeroCaptionPardot';
import {createTextPardot} from './TextPardot';
import {createTeaserTwoImagesPardot} from './Teaser-Two-ImagesPardot';
import {createFooterPardotES} from './FooterPardotES';
import {createDisclaimerPardotES} from './DisclaimerPardotES';
import {createFooterPardotDE} from './FooterPardotDE';
import {createDisclaimerPardotDE} from './DisclaimerPardotDE';
import {createFooterPardotBG} from './FooterPardotBG';
import {createDisclaimerPardotBG} from './DisclaimerPardotBG';
import {createFooterPardotSW} from './FooterPardotSW';
import {createDisclaimerPardotSW} from './DisclaimerPardotSW';
import {createFooterPardotFR} from './FooterPardotFR';
import {createDisclaimerPardotFR} from './DisclaimerPardotFR';

export const createTemplateGeneral2 = () => {
    const viewOnBrowser = createHeaderViewOnBrowserPardot();
    const headerLogONly = createHeaderLogoPardot();
    const hero = createHeroPardot();
    const heroImage = createHeroImagePardot();
    const heroInfo = createHeroInfoPardot();
    const heroCaption = createHeroCaptionPardot();
    const text = createTextPardot();
    const teaserTwoImages = createTeaserTwoImagesPardot();
    const footerSW = createFooterPardotSW();
    const disclaimerSW = createDisclaimerPardotSW();
    const footerDE = createFooterPardotDE();
    const disclaimerDE = createDisclaimerPardotDE();
    const footerES = createFooterPardotES();
    const disclaimerES = createDisclaimerPardotES();
    const footerFR = createFooterPardotFR();
    const disclaimerFR = createDisclaimerPardotFR();
    const footerBG = createFooterPardotBG();
    const disclaimerBG = createDisclaimerPardotBG();


    const templateGeneral2 = `
        ${viewOnBrowser}
        ${headerLogONly}
        <!-- space -->
            <table class="bg-table" align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                <tbody>
                    <tr>
                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                    </tr>
                </tbody>
        </table>
        <!-- END space -->
        ${hero}
        <!-- space -->
        <table class="bg-table" align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
            <tbody>
                <tr>
                    <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <!-- END space -->
        ${heroImage}
        ${heroInfo}
        ${heroCaption}
        ${text}
        ${teaserTwoImages}
        ${footerSW}
        ${disclaimerSW}
        ${footerDE}
        ${disclaimerDE}
        ${footerES}
        ${disclaimerES}
        ${footerFR}
        ${disclaimerFR}
        ${footerBG}
        ${disclaimerBG}
    `

  return templateGeneral2;
}*/
export const createTemplateGeneral2 = () => {
    const templateGeneral2 = `
    <!-- VIEW IN BROWSER -->
    <table cellpadding="0" cellspacing="0" border="0" align="center">
        <tbody>
            <tr>
                <td style="padding: 20px 0">
                    <a
                        href="{{View_Online}}"
                        pardot-region=""
                        style="
                  color: #005091;
                  font-size: 11px;
                  line-height: 14px;
                  font-family: Arial, Helvetica, sans-serif;
                "
                    >
                        View in browser
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END VIEW IN BROWSER -->
    
    <!-- HEADER LOGO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
    >
        <tbody>
            <tr>
                <td style="padding: 18px">
                    <table
                        align="center"
                        cellpadding="0"
                        cellspacing="0"
                        border="0"
                    >
                        <tbody>
                            <tr>
                                <td>
                                    <a
                                        href="https://www.southpole.com/"
                                        target="_blank"
                                    >
                                        <img
                                            src="https://go.southpole.com/sp-logo-transp-rgb"
                                            width="197"
                                            height="40"
                                            pardot-region=""
                                            alt=""
                                            border="0"
                                            style="display: block;width: 100%;height: 100%;"
                                            class="logo"
                                        />
                                        <!--[if !mso]>-->
                                        <img
                                            src="https://go.southpole.com/l/881182/2022-08-24/3r2kt/881182/1661327460VOKcM8hs/sp_logo_white.png"
                                            width="197"
                                            height="40"
                                            pardot-region=""
                                            alt=""
                                            border="0"
                                            style="display: none;width: 100%;height: 100%;"
                                            class="logo-white"
                                        />
                                        <!--<![endif]-->
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HEADER LOGO -->
    
    <!-- space -->
    <table
        class="bg-table"
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <!-- HERO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                          max-width: 640px;
                          width: 100%;
                          margin: auto;
                          text-align: center;
                        "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr>
                                <td
                                    background="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                    width="640"
                                    height="355"
                                    pardot-region=""
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                <!-- CHANGE OUTLOOK BANNER HERE -->
                                    <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                    style="width:640px;height:355px;">
                                <v:fill type="frame"
                                        pardot-region=""
                                        src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                        color="#005091"/>
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                               <!-- END CHANGE OUTLOOK BANNER HERE -->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="center"
                                            width="100%"
                                            style="max-width: 500px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="center"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="center"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                      text-align: center;
                                      padding: 78px 40px 0;
                                      text-align: center;
                                    "
                                                                        class="header__title"
                                                                    >
                                                                        <h1
                                                                            pardot-region=""
                                                                            style="
                                        margin: 0;
                                        font-size: 40px;
                                        line-height: 42px;
                                        color: #ffffff;
                                        font-weight: bold;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                                                                        >
                                                                           How can we support your climate journey?
                                                                        </h1>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                      text-align: center;
                                      padding: 5px 60px 77px 60px;
                                      font-size: 22px;
                                      line-height: 28px;
                                      color: #ffffff;
                                      font-family: Arial, Helvetica,
                                        sans-serif;
                                      text-align: center;
                                    "
                                                                        class="header__text"
                                                                        pardot-region=""
                                                                    >
                                                                        <p
                                                                            style="margin: 0"
                                                                        >
                                                                            Welcome
                                                                            to 2021
                                                                            - we
                                                                            hope
                                                                            you’re
                                                                            ready
                                                                            for a
                                                                            climate
                                                                            action-packed
                                                                            year!
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    
    <!-- space -->
    <table
        class="bg-table"
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        pardot-removable=""
        class="header"
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                        max-width: 640px;
                        width: 100%;
                        margin: auto;
                        text-align: center;
                    "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr>
                                <td
                                    class="header__title"
                                    style="padding-top: 40px;"
                                >
                                    <h1
                                        pardot-region=""
                                        style="
                                    margin: 0;
                                    font-size: 40px;
                                    line-height: 42px;
                                    color: #005091;
                                    font-weight: bold;
                                    font-family: Arial, Helvetica,
                                    sans-serif;
                                    "
                                    >
                                        Register for the Webinar
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    class="header__text"
                                    style="text-align: center;
                            font-size: 22px;
                            line-height: 28px;
                            color: #005091;
                            font-family: Arial, Helvetica,
                            sans-serif;"
                                >
                                    <div style="display: inline-block;">
                                        <p
                                            pardot-region=""
                                            style="margin-bottom: 0; padding-bottom: 5px;"
                                        >
                                            Improve your CDP performance in 2021
                                        </p>
                                         <hr style="border-bottom: 1px solid #005091;">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        align="center"
                                        border="0"
                                        width="100%"
                                        style="
                                    max-width: 602px;
                                    width: 100%;
                                    margin: auto;
                                    text-align: center;
                                    "
                                        class="deviceWidth"
                                    >
                                        <tbody>
                                            <tr>
                                                <td
                                                    class="header__image"
                                                    style="padding-top: 40px;"
                                                >
                                                    <!-- CHANGE BANNER HERE --><img alt="" border="0" height="100%" src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                                        width="100%"
                                                        height="100%"
                                                        alt=""
                                                        border="0"
                                                        style="display: block; height: 100%; width: 100%; object-fit: cover;"
                                                    /> <!-- END CHANGE BANNER HERE -->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <!-- HERO INFO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                              max-width: 640px;
                              width: 100%;
                              margin: auto;
                              text-align: center;
                            "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr> <!-- CHANGE BANNER HERE -->
                                <td 
                                    background="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                    width="640"
                                    height="355"
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                <!-- CHANGE OUTLOOK BANNER HERE -->
                                    <!--[if mso]>
        
                                <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 640px; height:355px;" src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg" />                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 640px; height:355px;">
                                <v:fill  opacity="0%" color="#005091"  />
                                <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="center"
                                            width="100%"
                                            style="max-width: 500px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="center"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="center"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                          text-align: center;
                                          padding: 78px 40px 0;
                                          text-align: center;
                                        "
                                                                        class="header__title"
                                                                    >
                                                                        <h1
                                                                            pardot-region=""
                                                                            style="
                                            margin: 0;
                                            font-size: 40px;
                                            line-height: 42px;
                                            color: #ffffff;
                                            font-weight: bold;
                                            font-family: Arial, Helvetica,
                                              sans-serif;
                                          "
                                                                        >
                                                                           Crossing the Line to Net Zero
                                                                        </h1>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                          text-align: center;
                                          padding: 5px 60px 77px 60px;
                                          font-size: 22px;
                                          line-height: 28px;
                                          color: #ffffff;
                                          font-family: Arial, Helvetica,
                                            sans-serif;
                                          text-align: center;
                                        "
                                                                        class="header__text"
                                                                    >
                                                                        <div
                                                                            style="display: inline-block; padding-bottom: 22px;"
                                                                        >
                                                                            <p
                                                                                pardot-region=""
                                                                                style="margin: 0; padding-top: 15px; padding-bottom: 3px;"
                                                                            >
                                                                               A new South Pole Report: The State of Net Zero
                                                                            </p>
                                                                          <hr>
                                                                        </div>
                                                                        <br />
                                                                        <!--[if !mso]>-->
                                                                        <a
                                                                            pardot-region=""
                                                                            href="http://"
                                                                            style="background-color:#FFFFFF;
                                                               border-radius:25px;color:#005091;
                                                               display:inline-block;font-family:sans-serif;
                                                               font-size:13px;font-weight:bold;line-height:40px;
                                                               text-align:center;text-decoration:none;width:175px;
                                                               -webkit-text-size-adjust:none;"
                                                                        >
                                                                            Register
                                                                            Today
                                                                            &raquo;
                                                                        </a>
                                                                        <!--<![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- CHANGE OUTLOOK CTA HERE -->
                                    <!--[if mso]>
                                </v:textbox>
                                </v:fill>
                                </v:rect>
                                </v:image>
                                 <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="#" style="height:40px;width:175px;position:absolute;top:235px;left:232px;" arcsize="63%" fillcolor="#FFFFFF" stroke="false">
                                <center style="color:#005091;-webkit-text-size-adjust:none;font-family:sans-serif;font-size:13px;font-weight:bold;">Register Today &raquo;</center>
                                </v:roundrect>
                                <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    <!-- HERO INFO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                              max-width: 640px;
                              width: 100%;
                              margin: auto;
                              text-align: center;
                            "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr> <!-- CHANGE BANNER HERE -->
                                <td 
                                    background="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                    width="640"
                                    height="355"
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                <!-- CHANGE OUTLOOK BANNER HERE -->
                                    <!--[if mso]>
        
                                <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 640px; height:355px;" src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg" />                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 640px; height:355px;">
                                <v:fill  opacity="0%" color="#005091"  />
                                <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="center"
                                            width="100%"
                                            style="max-width: 500px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="center"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="center"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                          text-align: center;
                                          padding: 78px 40px 0;
                                          text-align: center;
                                        "
                                                                        class="header__title"
                                                                    >
                                                                        <h1
                                                                            pardot-region=""
                                                                            style="
                                            margin: 0;
                                            font-size: 40px;
                                            line-height: 42px;
                                            color: #ffffff;
                                            font-weight: bold;
                                            font-family: Arial, Helvetica,
                                              sans-serif;
                                          "
                                                                        >
                                                                          Improve your CDP performance in 2022
                                                                        </h1>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                          text-align: center;
                                          padding: 5px 60px 77px 60px;
                                          font-size: 22px;
                                          line-height: 28px;
                                          color: #ffffff;
                                          font-family: Arial, Helvetica,
                                            sans-serif;
                                          text-align: center;
                                        "
                                                                        class="header__text"
                                                                    >
                                                                        <div
                                                                            style="display: inline-block; padding-bottom: 22px;"
                                                                        >
                                                                            <p
                                                                                pardot-region=""
                                                                                style="margin: 0; padding-top: 15px; padding-bottom: 3px;"
                                                                            >
                                                                              Wednesday 24 March, 14:00 GMT
    
                                                                            </p>
                                                                          <hr>
                                                                        </div>
                                                                        <br />
                                                                        <!--[if !mso]>-->
                                                                        <a
                                                                            pardot-region=""
                                                                            href="http://"
                                                                            style="background-color:#FFFFFF;
                                                               border-radius:25px;color:#005091;
                                                               display:inline-block;font-family:sans-serif;
                                                               font-size:13px;font-weight:bold;line-height:40px;
                                                               text-align:center;text-decoration:none;width:175px;
                                                               -webkit-text-size-adjust:none;"
                                                                        >
                                                                            Register
                                                                            Today
                                                                            &raquo;
                                                                        </a>
                                                                        <!--<![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- CHANGE OUTLOOK CTA HERE -->
                                    <!--[if mso]>
                                </v:textbox>
                                </v:fill>
                                </v:rect>
                                </v:image>
                                 <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="#" style="height:40px;width:175px;position:absolute;top:235px;left:232px;" arcsize="63%" fillcolor="#FFFFFF" stroke="false">
                                <center style="color:#005091;-webkit-text-size-adjust:none;font-family:sans-serif;font-size:13px;font-weight:bold;">Register Today &raquo;</center>
                                </v:roundrect>
                                <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <!-- HERO INFO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        pardot-removable=""
        class="header"
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                          max-width: 640px;
                          width: 100%;
                          margin: auto;
                          text-align: center;
                        "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr>
                                 <!-- CHANGE BANNER HERE -->
                                <td
                                    background="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                    width="640"
                                    height="355"
                                    pardot-region=""
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                 <!-- CHANGE OUTLOOK BANNER HERE -->
                                    <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                    style="width:640px;height:355px;">
                                <v:fill type="frame"
                                        pardot-region=""
                                        src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                        color="#005091"/>
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="right"
                                            width="100%"
                                            style="max-width: 640px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="right"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="right"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        align="right"
                                                                        style="padding: 15px 15px;"
                                                                    >
                                                                        <p
                                                                            pardot-region=""
                                                                            style="margin: 0; text-align:right; font-size: 11px !important; color: #fff; font-family: Arial, Helvetica, sans-serif;"
                                                                        >
                                                                            Lorem
                                                                            ipsum
                                                                            dolor
                                                                            sit
                                                                            caption
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- TEXT BLOCK-->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        pardot-repeatable=""
        class="wrapper text-block"
        align="center"
    >
        <tbody>
            <tr>
                <td style="height: 40px" class="spacing">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        border="0"
                        style="max-width: 640px; width: 100%; background: #ffffff"
                        class="wrapper"
                        align="center"
                    >
                        <tbody>
                            <tr>
                                <td width="20">&nbsp;</td>
                                <td pardot-region="">
                                    <p
                                        style="
                            margin: 0;
                            color: #019CDB;
                            font-size: 13px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: center;
                          "
                                    >
                                        FOR IMMEDIATE RELEASE
                                    </p>
                                    <br />
                                    <h2
                                        style="
                            margin: 0;
                            color: #005091 ;
                            font-size: 32px;
                            font-weight: bold;
                            line-height: 34px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: center;
                          "
                                        class="white__text"
                                    pardot-region="">
                                        Pioneering ‘herd aggregation’ project allows
                                        smaller graziers to earn carbon
                                    </h2>
    
                                    <br />
                                    <p
                                        style="
                            margin: 0;
                            color: #005091;
                            font-size: 20px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                                        class="white__text"
                                    pardot-region="">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. Pellentesque vel enim quis
                                        enim interdum viverra ut ut augue. Donec
                                        consequat purus sed nunc.
                                    </p>
    
                                    <br />
                                    <p
                                        style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                                        class="white__text_90"
                                    pardot-region="">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. Pellentesque vel enim quis
                                        enim interdum viverra ut ut augue. Donec
                                        consequat purus sed nunc.
                                    </p>
                                    <br />
    
                                    <!-- QUOTE BLOCK-->
                                    <table
                                        cellpadding="20"
                                        cellspacing="0"
                                        border="0"
                                        style="max-width: 640px; width: 100%; background-color: #005091"
                                        pardot-repeatable=""
                                        pardot-removable=""
                                        class=""
                                        align="center"
                                    >
                                        <tbody>
                                            <tr>
                                                <td align="center">
                                                    <h2
                                                        pardot-region=""
                                                        style="
                    color: #FFFFFF;
                    font-size: 18px;
                    line-height: 24px;
                    font-weight: bold;
                    margin: 0;
                    font-family: Arial, Helvetica, sans-serif;
                    text-align: center;
                    "
                                                    >
                                                        The previous version of this
                                                        media release erroneously
                                                        used the term carbon grazing
                                                        to refer to the breed cattle
                                                        heard management method of
                                                        carbon abatement.
                                                    </h2>
                                                    <br />
                                                    <!--[if mso]>
                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                             xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
                             style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" stroke="f"
                             fillcolor="#FFFFFF">
                    <w:anchorlock/>
                    <center>
                <![endif]-->
                                                    <a
                                                        pardot-region=""
                                                        href="http://"
                                                        style="background-color:#FFFFFF;
                   border-radius:25px;color:#005091;
                   display:inline-block;font-family:sans-serif;
                   font-size:13px;font-weight:bold;line-height:40px;
                   text-align:center;text-decoration:none;width:175px;
                   -webkit-text-size-adjust:none;"
                                                    >
                                                        Tell us your Goal &raquo;
                                                    </a>
                                                    <!--[if mso]>
                </center>
                </v:roundrect>
                <![endif]-->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END QUOTE BLOCK-->
    
                                    <!-- space -->
                                    <table
                                        class="bg-table"
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        align="center"
                                        pardot-repeatable=""
                                    >
                                        <tbody>
                                            <tr>
                                                <td
                                                    style="line-height: 20px; font-size: 20px"
                                                >
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END space -->
    
                                    <p
                                        style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 16px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                                        class="white__text_90"
                                    pardot-region="">
                                        <img
                                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                                            width="12"
                                            height="9"
                                            alt=""
                                            border="0"
                                            style="display: inline-block"
                                            class=""
                                        />
                                        Ut sagittis enim ut risus ultrices, ac
                                        <a href="#" style="color: #589CD7;">
                                            consequat</a
                                        >
                                        quam
                                    </p>
    
                                    <p
                                        style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 16px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                                        class="white__text_90"
                                    pardot-region="">
                                        <img
                                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                                            width="12"
                                            height="9"
                                            alt=""
                                            border="0"
                                            style="display: inline-block"
                                            class=""
                                        />
                                        Morbi rutrum euismod ipsum ut ornare
                                    </p>
    
                                    <p
                                        style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 16px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                                        class="white__text_90"
                                    pardot-region="">
                                        <img
                                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                                            width="12"
                                            height="9"
                                            alt=""
                                            border="0"
                                            style="display: inline-block"
                                            class=""
                                        />
                                        Sed hendrerit elit, non placerat nisl
                                        consequat ac From commitment to
                                    </p>
                                    <br />
                                    <p
                                        style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                                        class="white__text_90"
                                    pardot-region="">
                                        Etiam auctor, tellus id malesuada aliquet,
                                        leo mauris varius diam, a porta massa dui
                                        efficitur augue. Praesent eget luctus elit.
                                        In semper dui et euismod fermentum.
                                        Vestibulum bibendum, magna sed eleifend
                                        dapibus, orci ipsum finibus magna, eu
                                        egestas nisl erat in nulla. Aliquam et
                                        vulputate felis, ut ullamcorper augue. Etiam
                                        feugiat nibh ultrices velit tristique
                                        ornare. Proin tristique nulla at lectus
                                        hendrerit lacinia. Morbi tristique nisl
                                        turpis, in scelerisque mauris consequat
                                        vitae.
                                    </p>
                                    <br />
                                    <p
                                        style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                                        class="white__text_90"
                                    pardot-region="">
                                        King Regards,
                                    </p>
                                    <br />
                                    <p
                                        style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            font-weight: bold;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                                        class="white__text_90"
                                    pardot-region="">
                                        Isabel Hagbrink,<br />
                                        <span
                                            style="font-weight: normal; font-size: 14px;"
                                        >
                                            Director of Communications</span
                                        >
                                    </p>
                                </td>
                                <td width="20">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 40px" class="spacing">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END TEXT BLOCK-->
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        align="center"
    >
        <tbody>
            <tr>
                <td style="height: 50px" class="spacing">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        role="presentation"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        width="100%"
                        style="
              text-align: center;
              width: 100%;
              margin: auto;
            "
                        class="intro"
                    >
                        <tbody>
                            <tr>
                                <td class="intro-text" style="padding: 0px 40px">
                                    <table
                                        role="presentation"
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        id="snapshot"
                                    >
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h2
                                                        style="
                            color: #005091;
                            font-size: 21px;
                            line-height: 27px;
                            font-weight: bold;
                            margin: 0;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: center;
                          "
                                                    pardot-region="">
                                                        Lorem ipsum dolor sit amet,
                                                        consectetur adipiscing elit.
                                                        Pellentesque vel enim quis
                                                        enim interdum viverra ut ut
                                                        augue. Donec consequat purus
                                                        sed nunc.
                                                    </h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 30px">
                                                    <p
                                                        style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: center;
                          "
                                                        class="blue"
                                                    pardot-region="">
                                                        Ut sagittis enim ut risus
                                                        ultrices, ac consequat quam
                                                        volutpat. Morbi rutrum
                                                        euismod ipsum ut ornare. Sed
                                                        vestibulum hendrerit elit,
                                                        non placerat nisl consequat
                                                        ac. Nunc eget tempus sem.
                                                        Duis viverra eu felis
                                                        suscipit scelerisque.
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 50px" class="spacing">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    
    
    
    <!-- SUB-HEADING -->
    <table
        cellpadding="20"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #FFFFFF;"
        pardot-repeatable=""
        class="wrapper text-block"
        align="center"
    >
        <tbody>
            <tr>
                <td style="padding: 20px 0 0;">
                    <h2
                        pardot-region=""
                        style="
                        color: #005091;
                        font-size: 32px;
                        font-weight: bold;
                        margin: 0;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                        padding-top: 12px;
                    "
                        class="white__text"
                    >
                        About the Brands
                    </h2>
                </td>
            </tr>
        </tbody>
    </table>
    
    <!-- TEASER TWO IMAGES-->
    <table
        cellpadding="0"
        cellspacing="0"
        align="center"
        border="0"
        width="640"
        style="max-width: 640px; width: 100% !important; margin: auto; background-color: #FFFFFF;"
        pardot-repeatable=""
        class="wrapper"
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px; height:20px;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-size: 0px" class="p-mobile" align="center">
                    <!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="width:290px; padding: 0 15px;" align="left" valign="top">
                  <![endif]-->
                    <div
                        style="
                      display: inline-block;
                      vertical-align: top;
                      width: 50%;
                      margin:0;
                    "
                        class="deviceWidth heightAuto col-lge mobile-margin-bottom"
                    >
                        <!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="width:290px; padding: 0px;" align="left" valign="top">
                  <![endif]-->
                        <div
                            style="
                      display: block;
                      vertical-align: top;
                      height: 160px;
                      width: 290px;
                    
                    "
                            class="deviceWidth heightAuto"
                        >
                            <img
                                valign="bottom"
                                height="160"
                                pardot-region=""
                                src="https://res.cloudinary.com/eae-pt/image/upload/v1625571324/two_images_vdktoe.png"
                                style="
                        width: 100%;
                        max-width: 100%;
                        margin: auto;
                        display: block;
                      "
                            />
                        </div>
                        <!--[if mso]>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:290px; padding: 20px 0px;" align="left" valign="top" bgcolor="#fdc300">
                  <![endif]-->
    
                        <div
                            style="
                      background: #fdc300;
                      height: 205px;
                      padding: 20px 0;
                      display: block;
                      vertical-align: top;
                      width: 290px;
                      text-align: left;
                     
                   
                    "
                            class="deviceWidth heightAuto col-yellow teaser-content"
                        >
                            <!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="padding: 0 10px 0 15px">
                  <![endif]-->
                            <div style="padding: 0 10px 0 15px">
                                <h4
                                    pardot-region=""
                                    style="
                          font-size: 21px !important;
                          color: #1e355c;
                          font-weight: bold;
                          margin: 0 0 10px;
                          line-height: 24px;
                          font-family: Arial, Helvetica, sans-serif;
                        "
                                >
                                    Pellentesque vel enim quis enim interdum viverra
                                </h4>
                                <p
                                    pardot-region=""
                                    style="
                          font-size: 14px !important;
                          color: #1e355c;
                          margin: 0 0 20px;
                          line-height: 19px;
                          font-family: Arial, Helvetica, sans-serif;
                        "
                                >
                                    Praesent ultrices luctus cursus. Fusce a nunc
                                    quis arcu sollicitudin dapibus. Sed in ante sit
                                    amet massa varius fringilla…
                                </p>
    
                                <table
                                    role="presentation"
                                    border="0"
                                    cellpadding="0"
                                    cellspacing="0"
                                    align="left"
                                    class="link"
                                >
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a
                                                    pardot-region=""
                                                    href="https://www.southpole.com/"
                                                    style="
                                  font-size: 16px !important;
                                  font-weight: bold;
                                  color: #1e355c;
                                  text-decoration: none;
                                  font-family: Arial, Helvetica, sans-serif;
                                "
                                                >
                                                     Learn more &raquo;
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="line-height: 20px; font-size: 20px; height:20px;"
                                            >
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso]>
                        </td>
                    </tr>
                    </table>
                  <![endif]-->
                        </div>
                        <!--[if mso]>
                        </td>
                    </tr>
                    </table>
                  <![endif]-->
                    </div>
    
                    <!--[if mso]>
                    </td>
                    <td width="60">&nbsp;</td>
                    <td style="width:290px; padding: 0 15px;" align="left" valign="top">
                  <![endif]-->
                    <div
                        style="
                      display: inline-block;
                      vertical-align: top;
                      width: 50%;
                      margin: 0;
                    "
                        class="deviceWidth heightAuto col-lge"
                    >
                        <!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="width:290px; padding: 0px;" align="left" valign="top">
                  <![endif]-->
                        <div
                            style="
                      display: block;
                      vertical-align: top;
                      height: 160px;
                      width: 290px;
                     
                    "
                            class="deviceWidth heightAuto"
                        >
                            <img
                                valign="bottom"
                                height="160"
                                pardot-region=""
                                src="https://res.cloudinary.com/eae-pt/image/upload/v1625571111/two_images_2_ysxup1.png"
                                style="
                        width: 100%;
                        max-width: 100%;
                        margin: auto;
                        display: block;
                      "
                            />
                        </div>
                        <!--[if mso]>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:290px; padding: 20px 0px;" align="left" valign="top" bgcolor="#98C21F">
                  <![endif]-->
                        <div
                            style="
                      background: #98C21F;
                      height: 205px;;
                      padding: 20px 0;
                      display: block;
                      vertical-align: top;
                      width: 290px;
                      text-align: left;
                      margin: 0;
                 
                    "
                            class="deviceWidth heightAuto col-green teaser-content"
                        >
                            <!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="padding: 0 10px 0 15px">
                  <![endif]-->
                            <div style="padding: 0 10px 0 15px">
                                <h4
                                    pardot-region=""
                                    style="
                          font-size: 21px !important;
                          color: #1e355c;
                          font-weight: bold;
                          margin: 0 0 10px;
                          line-height: 24px;
                          font-family: Arial, Helvetica, sans-serif;
                        "
                                >
                                    Pellentesque vel enim quis enim interdum viverra
                                </h4>
                                <p
                                    pardot-region=""
                                    style="
                          font-size: 14px !important;
                          color: #1e355c;
                          margin: 0 0 20px;
                          line-height: 19px;
                          font-family: Arial, Helvetica, sans-serif;
                        "
                                >
                                    Praesent ultrices luctus cursus. Fusce a nunc
                                    quis arcu sollicitudin dapibus. Sed in ante sit
                                    amet massa varius fringilla…
                                </p>
    
                                <table
                                    role="presentation"
                                    border="0"
                                    cellpadding="0"
                                    cellspacing="0"
                                    align="left"
                                    class="link"
                                >
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a
                                                    pardot-region=""
                                                    href="https://www.southpole.com/"
                                                    style="
                                  font-size: 16px !important;
                                  font-weight: bold;
                                  color: #1e355c;
                                  text-decoration: none;
                                  font-family: Arial, Helvetica, sans-serif;
                                "
                                                >
                                                    Learn more &raquo;
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="line-height: 20px; font-size: 20px; height:20px;"
                                            >
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso]>
                        </td>
                    </tr>
                    </table>
                  <![endif]-->
                        </div>
                        <!--[if mso]>
                        </td>
                    </tr>
                    </table>
                  <![endif]-->
                    </div>
                    <!--[if mso]>
                    </td>
                   </tr>
                   </table>
                  <![endif]-->
                </td>
            </tr>
            <tr>
                <td style="line-height: 20px; font-size: 20px; height:20px;">
                    &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END TEASER TWO IMAGES-->
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px; height:20px;">
                    &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    
    <!-- FULL CTA DARK BLUE BLOCK-->
    <table
        cellpadding="20"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background-color: #005091"
        class=""
        pardot-repeatable=""
        align="center"
    >
        <tbody>
            <tr>
                <td align="center">
                    <h2
                        pardot-region=""
                        style="
                    color: #FFFFFF;
                    font-size: 21px;
                    font-weight: bold;
                    margin: 0;
                    font-family: Arial, Helvetica, sans-serif;
                    text-align: center;
                    "
                    >
                        Looking for more CDP Resources?
                    </h2>
                    <br />
                    <a
                        pardot-region=""
                        href="#"
                        style="
                    color: #FFFFFF;
                    font-size: 16px;
                    font-weight: bold;
                    text-decoration: none;
                    margin: 0;
                    font-family: Arial, Helvetica, sans-serif;
                    text-align: center;
                    "
                        >Download our Tool &raquo;
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END FULL CTA DARK BLUE BLOCK-->
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <!-- EN --><!-- FOOTER -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table"
        pardot-removable=""
        style="max-width: 640px; width: 100%; background: #005091"
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100%; background: #005091;"
                    >
                        <tbody>
                            <tr>
                                <td align="center">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding:0px;" align="left" valign="top" width="340">
                            <![endif]-->
                                    <div
                                        class="footer__logo"
                                        style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
                                    >
                                        <img
                                            alt=""
                                            border="0"
                                            height="31"
                                            pardot-region=""
                                            src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
                                            style="display: block"
                                            width="150"
                                        />
                                    </div>
                                    <!--[if mso]>
                            </td>
                            <td style="padding:0px;" align="left" valign="top" width="240">
                            <![endif]-->
    
                                    <div
                                        class="footer__socials"
                                        style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__socials__logos"
                                            style="width: 100%; background: #005091;"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        pardot-region=""
                                                        style=" width: 90px; color:#FFFFFF;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 18px;"
                                                        width="90"
                                                    >
                                                        Get in touch
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 25px;"
                                                        width="25"
                                                    >
                                                        <a
                                                            href="https://www.facebook.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
                                                                style="display: block"
                                                                width="8"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.linkedin.com/company/south-pole-global/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.instagram.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://twitter.com/southpoleglobal"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- FOOTER BOTTOM -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table-bottom"
        style="max-width: 640px; width: 100%; background: #1E355C"
    >
        <tbody>
            <tr>
                <td style="line-height: 30px; height: 30px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100% !important;"
                    >
                        <tbody>
                            <tr>
                                <td align="left">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                            <![endif]-->
                                    <div
                                        class="footer__moto"
                                        style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                    >
                                        <h2
                                            pardot-region=""
                                            style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 21px;
                        font-weight: bold;
                        color: #019CDB;
                        margin: 0;"
                                        >
                                            {{{dynamic_content_476}}}
                                        </h2>
                                    </div>
                                    <!--[if mso]>
                            </td>
                          
                            <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                            <![endif]-->
    
                                    <div
                                        class="footer__address"
                                        style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__address-table"
                                            style="width: 100%;"
                                        >
                                            <tbody>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                        font-weight: bold;
                        color: #FFFFFF;"
                                                    >
                                                        South Pole Global
                                                        Headquarters
                                                    </td>
                                                </tr>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px; color: #FFFFFF; padding-top: 10px;"
                                                    >
                                                        Technoparkstrasse 1 Zurich
                                                        8005 Switzerland
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 35px; height: 35px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END FOOTER-->
    
    <!-- DISCLAIMER -->
    
    <table
        class="bg-table"
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        style="max-width: 510px; width: 100%;"
    >
        <tbody>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr style="">
                <td
                    align="center"
                    class="footer__disclaimer"
                    pardot-region=""
                    style="text-align: center; color:#005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 12px;
                        line-height: 14px;"
                >
                    You are receiving this email because this email address is
                    signed up to receive the South Pole communications. You can
                    <a
                        href="{{EmailPreferenceCenter_114}}"
                        style="
                                        text-decoration: underline;
                                        color: #005091;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                        >update your preferences here</a
                    >.
                </td>
            </tr>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- DISCLAIMER -->
    
    <!-- DE --><!-- FOOTER -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table"
        pardot-removable=""
        style="max-width: 640px; width: 100%; background: #005091"
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100%; background: #005091;"
                    >
                        <tbody>
                            <tr>
                                <td align="center">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding:0px;" align="left" valign="top" width="340">
                            <![endif]-->
                                    <div
                                        class="footer__logo"
                                        style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
                                    >
                                        <img
                                            alt=""
                                            border="0"
                                            height="31"
                                            pardot-region=""
                                            src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
                                            style="display: block"
                                            width="150"
                                        />
                                    </div>
                                    <!--[if mso]>
                            </td>
                            <td style="padding:0px;" align="left" valign="top" width="240">
                            <![endif]-->
    
                                    <div
                                        class="footer__socials"
                                        style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__socials__logos"
                                            style="width: 100%; background: #005091;"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        pardot-region=""
                                                        style=" width: 90px; color:#FFFFFF;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 18px;"
                                                        width="90"
                                                    >
                                                        Kontakt
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 25px;"
                                                        width="25"
                                                    >
                                                        <a
                                                            href="https://www.facebook.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
                                                                style="display: block"
                                                                width="8"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.linkedin.com/company/south-pole-global/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.instagram.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://twitter.com/southpoleglobal"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- FOOTER BOTTOM -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table-bottom"
        style="max-width: 640px; width: 100%; background: #1E355C"
    >
        <tbody>
            <tr>
                <td style="line-height: 30px; height: 30px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100% !important;"
                    >
                        <tbody>
                            <tr>
                                <td align="left">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                            <![endif]-->
                                    <div
                                        class="footer__moto"
                                        style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                    >
                                        <h2
                                            pardot-region=""
                                            style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 21px;
                        font-weight: bold;
                        color: #019CDB;
                        margin: 0;"
                                        >
                                            {{{dynamic_content_479}}}
                                        </h2>
                                    </div>
                                    <!--[if mso]>
                            </td>
                          
                            <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                            <![endif]-->
    
                                    <div
                                        class="footer__address"
                                        style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__address-table"
                                            style="width: 100%;"
                                        >
                                            <tbody>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                        font-weight: bold;
                        color: #FFFFFF;"
                                                    >
                                                        South Pole Hauptsitz
                                                    </td>
                                                </tr>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px; color: #FFFFFF; padding-top: 10px;"
                                                    >
                                                        Technoparkstrasse 1 Zürich
                                                        8005 Schweiz
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 35px; height: 35px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END FOOTER-->
    
    <!-- DISCLAIMER -->
    
    <table
        align="center"
        class="bg-table"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-removable=""
        style="max-width: 510px; width: 100%;"
    >
        <tbody>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr style="">
                <td
                    align="center"
                    class="footer__disclaimer"
                    pardot-region=""
                    style="text-align: center; color:#005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 12px;
                        line-height: 14px;"
                >
                    Sie erhalten diese E-Mail, weil Ihre E-Mail-Adresse für den
                    Erhalt dieser Mitteilungen bei South Pole registriert ist. Sie
                    können
                    <a
                        href="{{EmailPreferenceCenter_114}}"
                        style="
                                        text-decoration: underline;
                                        color: #005091;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                        >Ihre Einstellungen hier aktualisieren</a
                    >.
                </td>
            </tr>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- DISCLAIMER -->
    
    <!-- ES --><!-- FOOTER -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table"
        pardot-removable=""
        style="max-width: 640px; width: 100%; background: #005091"
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100%; background: #005091;"
                    >
                        <tbody>
                            <tr>
                                <td align="center">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding:0px;" align="left" valign="top" width="340">
                            <![endif]-->
                                    <div
                                        class="footer__logo"
                                        style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
                                    >
                                        <img
                                            alt=""
                                            border="0"
                                            height="31"
                                            pardot-region=""
                                            src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
                                            style="display: block"
                                            width="150"
                                        />
                                    </div>
                                    <!--[if mso]>
                            </td>
                            <td style="padding:0px;" align="left" valign="top" width="240">
                            <![endif]-->
    
                                    <div
                                        class="footer__socials"
                                        style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__socials__logos"
                                            style="width: 100%; background: #005091;"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        pardot-region=""
                                                        style=" width: 90px; color:#FFFFFF;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 18px;"
                                                        width="90"
                                                    >
                                                        Contáctanos
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 25px;"
                                                        width="25"
                                                    >
                                                        <a
                                                            href="https://www.facebook.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
                                                                style="display: block"
                                                                width="8"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.linkedin.com/company/south-pole-global/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.instagram.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://twitter.com/southpoleglobal"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- FOOTER BOTTOM -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table-bottom"
        style="max-width: 640px; width: 100%; background: #1E355C"
    >
        <tbody>
            <tr>
                <td style="line-height: 30px; height: 30px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100% !important;"
                    >
                        <tbody>
                            <tr>
                                <td align="left">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                            <![endif]-->
                                    <div
                                        class="footer__moto"
                                        style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                    >
                                        <h2
                                            pardot-region=""
                                            style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 21px;
                        font-weight: bold;
                        color: #019CDB;
                        margin: 0;"
                                        >
                                            {{{dynamic_content_482}}}
                                        </h2>
                                    </div>
                                    <!--[if mso]>
                            </td>
                          
                            <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                            <![endif]-->
    
                                    <div
                                        class="footer__address"
                                        style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__address-table"
                                            style="width: 100%;"
                                        >
                                            <tbody>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                        font-weight: bold;
                        color: #FFFFFF;"
                                                    >
                                                        South Pole Hauptsitz
                                                    </td>
                                                </tr>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px; color: #FFFFFF; padding-top: 10px;"
                                                    >
                                                        Technoparkstrasse 1 Zürich
                                                        8005 Schweiz
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 35px; height: 35px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END FOOTER-->
    
    <!-- DISCLAIMER -->
    
    <table
        align="center"
        class="bg-table"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-removable=""
        style="max-width: 510px; width: 100%;"
    >
        <tbody>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr style="">
                <td
                    align="center"
                    class="footer__disclaimer"
                    pardot-region=""
                    style="text-align: center; color:#005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 12px;
                        line-height: 14px;"
                >
                    Está recibiendo este email porque se ha suscrito para recibir
                    comunicaciones de South Pole. Puede actualizar
                    <a
                        href="{{EmailPreferenceCenter_114}}"
                        style="
                                        text-decoration: underline;
                                        color: #005091;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                        >sus preferencias aquí</a
                    >.
                </td>
            </tr>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- DISCLAIMER -->
    <!-- SV --><!-- FOOTER -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table"
        pardot-removable=""
        style="max-width: 640px; width: 100%; background: #005091"
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100%; background: #005091;"
                    >
                        <tbody>
                            <tr>
                                <td align="center">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding:0px;" align="left" valign="top" width="340">
                            <![endif]-->
                                    <div
                                        class="footer__logo"
                                        style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
                                    >
                                        <img
                                            alt=""
                                            border="0"
                                            height="31"
                                            pardot-region=""
                                            src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
                                            style="display: block"
                                            width="150"
                                        />
                                    </div>
                                    <!--[if mso]>
                            </td>
                            <td style="padding:0px;" align="left" valign="top" width="240">
                            <![endif]-->
    
                                    <div
                                        class="footer__socials"
                                        style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__socials__logos"
                                            style="width: 100%; background: #005091;"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        pardot-region=""
                                                        style=" width: 90px; color:#FFFFFF;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 18px;"
                                                        width="90"
                                                    >
                                                        Kontakta oss
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 25px;"
                                                        width="25"
                                                    >
                                                        <a
                                                            href="https://www.facebook.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
                                                                style="display: block"
                                                                width="8"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.linkedin.com/company/south-pole-global/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.instagram.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://twitter.com/southpoleglobal"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- FOOTER BOTTOM -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table-bottom"
        style="max-width: 640px; width: 100%; background: #1E355C"
    >
        <tbody>
            <tr>
                <td style="line-height: 30px; height: 30px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100% !important;"
                    >
                        <tbody>
                            <tr>
                                <td align="left">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                            <![endif]-->
                                    <div
                                        class="footer__moto"
                                        style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                    >
                                        <h2
                                            pardot-region=""
                                            style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 21px;
                        font-weight: bold;
                        color: #019CDB;
                        margin: 0;"
                                        >
                                            {{{dynamic_content_592}}}
                                        </h2>
                                    </div>
                                    <!--[if mso]>
                            </td>
                          
                            <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                            <![endif]-->
    
                                    <div
                                        class="footer__address"
                                        style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__address-table"
                                            style="width: 100%;"
                                        >
                                            <tbody>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                        font-weight: bold;
                        color: #FFFFFF;"
                                                    >
                                                        South Pole globala huvudkontoret
                                                    </td>
                                                </tr>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px; color: #FFFFFF; padding-top: 10px;"
                                                    >
                                                        Technoparkstrasse 1 Zürich
                                                        8005 Schweiz
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 35px; height: 35px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END FOOTER-->
    
    <!-- DISCLAIMER -->
    
    <table
        align="center"
        class="bg-table"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-removable=""
        style="max-width: 510px; width: 100%;"
    >
        <tbody>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr style="">
                <td
                    align="center"
                    class="footer__disclaimer"
                    pardot-region=""
                    style="text-align: center; color:#005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 12px;
                        line-height: 14px;"
                >
                    Du har mottagit detta e-mail då e-postadressen är registrerad hos oss för mottagning av kommunikation från South Pole. Du kan uppdatera dina preferenser
                    <a
                        href="{{EmailPreferenceCenter_114}}"
                        style="
                                        text-decoration: underline;
                                        color: #005091;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                        >här</a
                    >.
                </td>
            </tr>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- DISCLAIMER -->
    
    
    <!-- FR --><!-- FOOTER -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table"
        pardot-removable=""
        style="max-width: 640px; width: 100%; background: #005091"
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100%; background: #005091;"
                    >
                        <tbody>
                            <tr>
                                <td align="center">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding:0px;" align="left" valign="top" width="340">
                            <![endif]-->
                                    <div
                                        class="footer__logo"
                                        style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
                                    >
                                        <img
                                            alt=""
                                            border="0"
                                            height="31"
                                            pardot-region=""
                                            src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
                                            style="display: block"
                                            width="150"
                                        />
                                    </div>
                                    <!--[if mso]>
                            </td>
                            <td style="padding:0px;" align="left" valign="top" width="240">
                            <![endif]-->
    
                                    <div
                                        class="footer__socials"
                                        style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__socials__logos"
                                            style="width: 100%; background: #005091;"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        pardot-region=""
                                                        style=" width: 90px; color:#FFFFFF;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 18px;"
                                                        width="90"
                                                    >
                                                        Contactez-nous
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 25px;"
                                                        width="25"
                                                    >
                                                        <a
                                                            href="https://www.facebook.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
                                                                style="display: block"
                                                                width="8"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.linkedin.com/company/south-pole-global/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.instagram.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://twitter.com/southpoleglobal"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- FOOTER BOTTOM -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table-bottom"
        style="max-width: 640px; width: 100%; background: #1E355C"
    >
        <tbody>
            <tr>
                <td style="line-height: 30px; height: 30px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100% !important;"
                    >
                        <tbody>
                            <tr>
                                <td align="left">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                            <![endif]-->
                                    <div
                                        class="footer__moto"
                                        style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                    >
                                        <h2
                                            pardot-region=""
                                            style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 21px;
                        font-weight: bold;
                        color: #019CDB;
                        margin: 0;"
                                        >
                                            {{{dynamic_content_485}}}
                                        </h2>
                                    </div>
                                    <!--[if mso]>
                            </td>
                          
                            <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                            <![endif]-->
    
                                    <div
                                        class="footer__address"
                                        style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__address-table"
                                            style="width: 100%;"
                                        >
                                            <tbody>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                        font-weight: bold;
                        color: #FFFFFF;"
                                                    >
                                                        South Pole France
                                                    </td>
                                                </tr>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px; color: #FFFFFF; padding-top: 10px;"
                                                    >
                                                        Morning, 75 rue d’Amsterdam,
                                                        75009 Paris
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 35px; height: 35px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END FOOTER-->
    
    <!-- DISCLAIMER -->
    
    <table
        align="center"
        class="bg-table"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-removable=""
        style="max-width: 510px; width: 100%;"
    >
        <tbody>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr style="">
                <td
                    align="center"
                    class="footer__disclaimer"
                    pardot-region=""
                    style="text-align: center; color:#005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 12px;
                        line-height: 14px;"
                >
                    Vous recevez trop d'emails? Vous pouvez
                    <a
                        href="{{EmailPreferenceCenter_114}}"
                        style="
                                        text-decoration: underline;
                                        color: #005091;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                        >gérer vos préférences ici</a
                    >.
                </td>
            </tr>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- DISCLAIMER -->
    
    <!-- BELUX EN --><!-- FOOTER -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table"
        pardot-removable=""
        style="max-width: 640px; width: 100%; background: #005091"
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100%; background: #005091;"
                    >
                        <tbody>
                            <tr>
                                <td align="center">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding:0px;" align="left" valign="top" width="340">
                            <![endif]-->
                                     <div class="footer__logo" style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"><img alt="" border="0" height="54" pardot-height="54" pardot-region="" pardot-width="120" src="https://go.southpole.com/l/881182/2022-01-14/2g3nw/881182/1642150089QLePlYqU/CO2logic_part_of_south_pole_white_EN.png" style="display: block; width: 120px; max-width: 120px; height: 54px; max-height: 54px;" width="120"></div>
                                    <!--[if mso]>
                            </td>
                            <td style="padding:0px;" align="left" valign="top" width="240">
                            <![endif]-->
    
                                    <div
                                        class="footer__socials"
                                        style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__socials__logos"
                                            style="width: 100%; background: #005091;"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        pardot-region=""
                                                        style=" width: 90px; color:#FFFFFF;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 18px;"
                                                        width="90"
                                                    >
                                                        Get in touch
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 25px;"
                                                        width="25"
                                                    >
                                                        <a
                                                            href="https://www.facebook.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
                                                                style="display: block"
                                                                width="8"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.linkedin.com/company/south-pole-global/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.instagram.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://twitter.com/southpoleglobal"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- FOOTER BOTTOM -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table-bottom"
        style="max-width: 640px; width: 100%; background: #1E355C"
    >
        <tbody>
            <tr>
                <td style="line-height: 30px; height: 30px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100% !important;"
                    >
                        <tbody>
                            <tr>
                                <td align="left">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                            <![endif]-->
                                    <div
                                        class="footer__moto"
                                        style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                    >
                                        <h2
                                            pardot-region=""
                                            style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 21px;
                        font-weight: bold;
                        color: #019CDB;
                        margin: 0;"
                                        >
                                            {{{dynamic_content_488}}}
                                        </h2>
                                    </div>
                                    <!--[if mso]>
                            </td>
                          
                            <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                            <![endif]-->
    
                                    <div
                                        class="footer__address"
                                        style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__address-table"
                                            style="width: 100%;"
                                        >
                                            <tbody>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                        font-weight: bold;
                        color: #FFFFFF;"
                                                    >
                                                        CO2logic part of South Pole
                                                    </td>
                                                </tr>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px; color: #FFFFFF; padding-top: 10px;"
                                                    >
                                                        Cantersteen 47, 1000
                                                        Bruxelles, Belgium
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 35px; height: 35px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END FOOTER-->
    <!-- DISCLAIMER -->
    
    <table
        align="center"
        class="bg-table"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-removable=""
        style="max-width: 510px; width: 100%;"
    >
        <tbody>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr style="">
                <td
                    align="center"
                    class="footer__disclaimer"
                    pardot-region=""
                    style="text-align: center; color:#005091;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    line-height: 14px;"
                >
                    You are receiving this email because this email address is
                    signed up to receive the South Pole communications. You can
                    <a
                        href="{{EmailPreferenceCenter_114}}"
                        style="
                    text-decoration: underline;
                    color: #005091;
                    font-family: Arial, Helvetica,
                      sans-serif;
                  "
                        >update your preferences here</a
                    >.
                </td>
            </tr>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- DISCLAIMER -->
    `

  return templateGeneral2;
}