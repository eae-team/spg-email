import { createDisclaimerPardotBG } from './DisclaimerPardotBG';

export default {
  title: 'Pardot Tags/Disclaimer/Disclaimer EN (Belgium)',
};

const Template = () => createDisclaimerPardotBG();

export const DisclaimerComponent = Template.bind({});

