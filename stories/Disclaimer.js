import './main.css';

export const createDisclaimer = () => {
    const disclaimer = `
  <!-- DISCLAIMER -->
<table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 510px; width: 100%;"
>
    <tr class="space bg-table">
        <td style="line-height: 20px; height: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; color:#005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 12px;
                    line-height: 14px;" align="center" class="footer__disclaimer">You are receiving this email because
            this email
            address is signed up to receive the South Pole
            communications. You can <a
                                  href="https://www.southpole.com/"
                                  style="
                                    text-decoration: underline;
                                    color: #005091;
                                    font-family: Arial, Helvetica,
                                      sans-serif;
                                  "
                                  >You can update your preferences</a>
                                or
                                <a
                                  href="https://www.southpole.com/"
                                  style="
                                    text-decoration: underline;
                                    color: #005091;
                                    font-family: Arial, Helvetica,
                                      sans-serif;
                                  "
                                  >unsubscribe from this list.</a>.
        </td>
    </tr>
    <tr class="space bg-table">
        <td style="line-height: 20px; height: 20px">&nbsp;</td>
    </tr>

</table>
<!-- DISCLAIMER -->
  `;

    return disclaimer;
};
