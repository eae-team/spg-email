import {createHeroInfoNoButton} from './HeroInfoNoButton';

export default {
  title: 'Blocks/Hero/Info No Button',
};

const Template = () => createHeroInfoNoButton();

export const HeroComponent = Template.bind({});

