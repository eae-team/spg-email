import { createListArticle } from './List-Article';

export default {
  title: 'Blocks/List',
};

const Template = () => createListArticle();

export const ListArticleComponent = Template.bind({});
