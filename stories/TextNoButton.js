import './main.css';

export const createTextNoButton = () => {
  const textNoButton = `
<table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       class="header"
       align="center">
    <tr>
        <td style="height: 50px" class="spacing">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    width="100%"
                    style="
          text-align: center;
          width: 100%;
          margin: auto;
        "
                    class="intro"
            >
                <tbody>
                <tr>
                    <td class="intro-text" style="padding: 0px 40px">
                        <table
                                role="presentation"
                                border="0"
                                cellpadding="0"
                                cellspacing="0"
                                id="snapshot"
                        >
                            <tr>
                                <td>
                                    <h2
                                            style="
                        color: #005091;
                        font-size: 21px;
                        line-height: 27px;
                        font-weight: bold;
                        margin: 0;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                                    >
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel enim
                                        quis enim interdum viverra ut ut augue. Donec consequat purus sed nunc.
                                    </h2>

                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 30px">
                                    <p
                                            style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 18px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                                            class="blue"
                                    >
                                        Ut sagittis enim ut risus ultrices, ac consequat quam volutpat. Morbi rutrum
                                        euismod ipsum ut ornare. Sed vestibulum hendrerit elit, non placerat nisl
                                        consequat ac. Nunc eget tempus sem. Duis viverra eu felis suscipit scelerisque.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 50px" class="spacing">&nbsp;</td>
    </tr>
</table>`;

return textNoButton;

};