import { createDisclaimerPardotES } from './DisclaimerPardotES';

export default {
  title: 'Pardot Tags/Disclaimer/Disclaimer ES',
};

const Template = () => createDisclaimerPardotES();

export const DisclaimerComponent = Template.bind({});