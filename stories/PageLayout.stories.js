import { createPageLayout } from './PageLayout';

export default {
  title: 'Pages/Page Layout',
};

const Template = () => createPageLayout();

export const PageComponent = Template.bind({});

