import { createFooterPardotFR } from './FooterPardotFR';

export default {
  title: 'Pardot Tags/Footer/Footer FR',
};

const Template = () => createFooterPardotFR();

export const FooterComponent = Template.bind({});
