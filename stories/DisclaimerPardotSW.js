import './main.css';

export const createDisclaimerPardotSW = () => {
    const disclaimer = `
    <!-- DISCLAIMER -->

                        <table class="bg-table" align="center" border="0" cellpadding="0" cellspacing="0" style="max-width: 510px; width: 100%;">
                            <tbody>
                                <tr class="space bg-table">
                                    <td style="line-height: 20px; height: 20px">&nbsp;</td>
                                </tr>
                                <tr style="">
                                    <td align="center" class="footer__disclaimer" pardot-region="" style="text-align: center; color:#005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 12px;
                    line-height: 14px;">You are receiving this email because this email address is signed up to receive the South Pole communications. You can <a href="{{EmailPreferenceCenter_114}}" style="
                                    text-decoration: underline;
                                    color: #005091;
                                    font-family: Arial, Helvetica,
                                      sans-serif;
                                  ">update your preferences here</a>.</td>
                                </tr>
                                <tr class="space bg-table">
                                    <td style="line-height: 20px; height: 20px">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- DISCLAIMER -->`;

    return disclaimer;
};