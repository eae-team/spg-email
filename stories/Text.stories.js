import {createIntro, createText} from './Text';

export default {
  title: 'Blocks/Text/Default',
};

const Template = () => createText();

export const TextComponent = Template.bind({});
