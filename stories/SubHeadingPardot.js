import './main.css';

export const createSubHeadingPardot = () => {
    const subHeading = `<!-- SUB-HEADING -->
    <table cellpadding="20"
    cellspacing="0"
    border="0"
    style="max-width: 640px; width: 100%; background: #FFFFFF;"
    pardot-repeatable=""
    class="wrapper text-block"
    align="center">
 
        <tr>      
            <td style="padding: 20px 0 0;">
                <h2 pardot-region=""
                    style="
                    color: #005091;
                    font-size: 32px;
                    font-weight: bold;
                    margin: 0;
                    font-family: Arial, Helvetica, sans-serif;
                    text-align: center;
                    padding-top: 12px;
                "
                class="white__text"
                >About the Brands</h2>
            </td>
        </tr>
    </table>


`;

    return subHeading;
};
