import { createLogosRowPardot } from './Logos-RowPardot';

export default {
  title: 'Pardot Tags/Logos',
};

const Template = () => createLogosRowPardot();

export const LogosRowComponent = Template.bind({});