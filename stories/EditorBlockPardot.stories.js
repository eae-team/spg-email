import {createEditorBlockPardot} from './EditorBlockPardot';

export default {
  title: 'Pardot Tags/Editor Block',
};

const Template = () => createEditorBlockPardot();

export const EditorBlockComponent = Template.bind({});
