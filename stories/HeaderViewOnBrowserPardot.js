import './main.css';

export const createHeaderViewOnBrowserPardot = () => {
    const header = `
<!-- VIEW IN BROWSER -->
<table cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="padding: 20px 0">
            <a
                    href="{{View_Online}}"
                    pardot-region=""
                    style="
              color: #005091;
              font-size: 11px;
              line-height: 14px;
              font-family: Arial, Helvetica, sans-serif;
            "
            >
                View in browser
            </a>
        </td>
    </tr>
</table>
<!-- END VIEW IN BROWSER -->
`;

    return header;
};
