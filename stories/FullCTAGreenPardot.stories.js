import {createFullCTAGreenPardot} from './FullCTAGreenPardot';

export default {
  title: 'Pardot Tags/Full CTA/Green',
};

const Template = () => createFullCTAGreenPardot();

export const FullCTAComponent = Template.bind({});
