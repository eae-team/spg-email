import './main.css';

export const createEditorBlock = () => {
    const editorBlock = `
<!-- EDITOR BLOCK-->
<table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       class="wrapper text-block"
       align="center">
    <tr>
        <td style="height: 40px;" class="spacing">&nbsp;</td>
    </tr>
    <tr>
      
        <td>
            <table cellpadding="0"
               cellspacing="0"
               border="0"
               style="max-width: 640px; width: 100%; background: #ffffff"
               class="wrapper"
               align="center">
                <tr>
                    <td width="40" style="font-size: 40px">&nbsp;</td>
                    <td>
                         <h2
                            style="
                                color: #005091;
                                font-size: 32px;
                                font-weight: bold;
                                margin: 0;
                                font-family: Arial, Helvetica, sans-serif;
                                text-align: center;
                            "
                            class="white__text"
                         >
                              Notes to the editor on the beef cattle herd management method:
                         </h2>
                        <br>
                        <p style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 16px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                            class="white__text_90"
                        > Ut sagittis enim ut risus ultrices, ac consequat quam volutpat. Morbi rutrum euismod ipsum ut ornare. Sed vestibulum hendrerit elit, non placerat nisl consequat ac. 
                        </p>
                 
                        <p style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 16px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                        "
                        class="white__text_90"
                        ><img
                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                            width="12"
                            height="9"
                            alt=""
                            border="0"
                            style="display: inline-block"
                            class=""
                            /> Ut sagittis enim ut risus ultrices, ac <a href="#" style="color: #005091;"> consequat</a> quam 
                        </p>
                 
                        <p style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 16px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                          class="white__text_90"
                        ><img
                                src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                                width="12"
                                height="9"
                                alt=""
                                border="0"
                                style="display: inline-block"
                                class=""
                        /> Morbi rutrum euismod ipsum ut ornare</p>
                          
                        <p style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 16px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          "
                          class="white__text_90"
                        ><img
                                src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                                width="12"
                                height="9"
                                alt=""
                                border="0"
                                style="display: inline-block"
                                class=""
                        /> Sed hendrerit elit, non placerat nisl consequat ac From commitment to </p>
                          
                                
                    </td>
                    <td width="40" style="font-size: 0px">&nbsp;</td>
                </tr>
                <tr>
                    <td width="40" style="font-size: 40px; display: block">&nbsp;</td>
                    <td style="background:none; border-bottom: 1px solid #e6e6e6; height:1px; width:100%; margin:0px 0px 0px 0px;">&nbsp;</td>
                    <td width="40" style="font-size: 40px; display: block">&nbsp;</td>
                </tr>
                <tr>
                    <td width="40" style="font-size: 40px; display: block">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td width="40" style="font-size: 40px; display: block">&nbsp;</td>
                </tr>
                <tr>
                    <td width="185">&nbsp;</td>
                    <td align="center" width="270">
                     <table cellpadding="0"
                       cellspacing="0"
                       border="0"
                       width="270"
                       style="max-width: 270px; background: #ffffff; text-align: center;"
                       class="wrapper"
                       align="center">
                        <tr>
                            <td align="center">
                                <img
                                    valign="top"
                                    src="https://res.cloudinary.com/eae-pt/image/upload/v1625576775/speaker_q7vx7g.png"
                                    style="
                                      width: 107px;
                                      max-width: 100%;
                                      margin: auto;
                                      display: block;
                                      -webkit-border-radius: 50%;
                                      -moz-border-radius: 50%;
                                      border-radius: 50%;
                                    "
                                  />
                                <br>
                                <h2
                                    style="
                                        color: #005091;
                                        font-size: 18px;
                                        font-weight: bold;
                                        margin: 0;
                                        font-family: Arial, Helvetica, sans-serif;
                                        text-align: center;
                                    "
                                    class="white__text"
                                >
                                  Stephen Breslin
                                </h2>
                              
                                <p style="
                                    margin: 0;
                                    color: #005091;
                                    font-size: 14px;
                                    line-height: 24px;
                                    font-family: Arial, Helvetica, sans-serif;
                                    text-align: center;
                                  "
                                    class="white__text_90"
                                >
                                    Website & Digital Manager
                                </p>
                                <br>
                                <p style="
                                    margin: 0;
                                    color: #005091;
                                    font-size: 12px;
                                    padding: 12px 0px;
                                    font-family: Arial, Helvetica, sans-serif;
                                    text-align: center;
                                    border-bottom: 1px solid #e6e6e6;
                                    border-top: 1px solid #e6e6e6;
                                  "
                                    class="white__text_90"
                                >
                                    s.breslin@southpole.com   +353 87 7763132
                                </p>
                                <br>
                                <table cellpadding="0"
                                   cellspacing="0"
                                   border="0"
                                   width="108"
                                   style="max-width: 210px; width: 210px; background: #ffffff; text-align: center;"
                                   class="wrapper"
                                   align="center">
                                    <tr>
                                        <td width="27">
                                            <a href="#">
                                                <img
                                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/link_hh8l6d.png"
                                                    width="27"
                                                    height="27"
                                                    alt=""
                                                    border="0"
                                                    style="display: inline-block"
                                                    class=""
                                                />
                                            </a>
                                        </td>
                                        <td width="27">
                                            <a href="#">
                                                <img
                                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/linkedin_qz7xrv.png"
                                                    width="27"
                                                    height="27"
                                                    alt=""
                                                    border="0"
                                                    style="display: inline-block"
                                                    class=""
                                                />
                                            </a>
                                        </td>
                                        <td width="27">
                                            <a href="#">
                                                <img
                                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/instagram_vmbmdz.png"
                                                    width="27"
                                                    height="27"
                                                    alt=""
                                                    border="0"
                                                    style="display: inline-block"
                                                    class=""
                                                />
                                            </a>
                                        </td>
                                        <td width="27">
                                            <a href="#">
                                                <img
                                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/facebook_cdpecf.png"
                                                    width="27"
                                                    height="27"
                                                    alt=""
                                                    border="0"
                                                    style="display: inline-block"
                                                    class=""
                                                />
                                            </a>
                                        </td>
                                        <td width="27">
                                            <a href="#">
                                                <img
                                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1647615659/twitter_ej1rlf.png"
                                                    width="27"
                                                    height="27"
                                                    alt=""
                                                    border="0"
                                                    style="display: inline-block"
                                                    class=""
                                                />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
               
                    </table>
                    </td>
                    <td width="185">&nbsp;</td>
                </tr>
            </table>
        </td>
        
    
    </tr>
    <tr>
        <td style="height: 40px;" class="spacing" >&nbsp;</td>
    </tr>
</table>
<!-- END EDITOR BLOCK-->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
`;

    return editorBlock
}
