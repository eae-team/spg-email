import {createHero} from './Hero';

export default {
  title: 'Blocks/Hero',
};

const Template = () => createHero();

export const HeroComponent = Template.bind({});

