import './main.css';

import {createHighlightPardot} from './HighlightPardot';

export const createTextPardot = () => {
    const highlight = createHighlightPardot();
    const Text = `
<!-- TEXT BLOCK-->
<table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       pardot-repeatable=""
       class="wrapper text-block"
       align="center">
    <tr>
        <td style="height: 40px" class="spacing">&nbsp;</td>
    </tr>
    <tr>
      
        <td>
            <table cellpadding="0"
               cellspacing="0"
               border="0"
               style="max-width: 640px; width: 100%; background: #ffffff"
               class="wrapper"
               align="center">
                <tr>
                    <td width="20">&nbsp;</td>
                    <td pardot-region="">
                        <p style="
                        margin: 0;
                        color: #019CDB;
                        font-size: 13px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                      >FOR IMMEDIATE RELEASE</p>
                      <br>
                        <h2 style="
                        margin: 0;
                        color: #005091 ;
                        font-size: 32px;
                        font-weight: bold;
                        line-height: 34px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      " class="white__text">Pioneering ‘herd aggregation’ project allows smaller graziers to earn carbon </h2>
                      
                      <br>
                      <p style="
                        margin: 0;
                        color: #005091;
                        font-size: 20px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                      class="white__text"
                      >Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                      Pellentesque vel enim quis enim interdum viverra ut ut augue. 
                      Donec consequat purus sed nunc.</p>
                      
                      <br>
                      <p style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 18px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                      class="white__text_90"
                      >Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                      Pellentesque vel enim quis enim interdum viverra ut ut augue. 
                      Donec consequat purus sed nunc.</p>
                      <br>
                      ${highlight}
                      
                      <p style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 16px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                      class="white__text_90"
                      ><img
                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                            width="12"
                            height="9"
                            alt=""
                            border="0"
                            style="display: inline-block"
                            class=""
                        /> Ut sagittis enim ut risus ultrices, ac <a href="#" style="color: #589CD7;"> consequat</a> quam </p>
                     
                      <p style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 16px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                      class="white__text_90"
                      ><img
                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                            width="12"
                            height="9"
                            alt=""
                            border="0"
                            style="display: inline-block"
                            class=""
                        /> Morbi rutrum euismod ipsum ut ornare</p>
                      
                      <p style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 16px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                      class="white__text_90"
                      ><img
                            src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png"
                            width="12"
                            height="9"
                            alt=""
                            border="0"
                            style="display: inline-block"
                            class=""
                        /> Sed hendrerit elit, non placerat nisl consequat ac From commitment to </p>
                      <br>
                      <p style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 18px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                      class="white__text_90"
                      >Etiam auctor, tellus id malesuada aliquet, leo mauris varius diam, 
                      a porta massa dui efficitur augue. Praesent eget luctus elit. 
                      In semper dui et euismod fermentum. Vestibulum bibendum, 
                      magna sed eleifend dapibus, orci ipsum finibus magna, 
                      eu egestas nisl erat in nulla. Aliquam et vulputate felis, 
                      ut ullamcorper augue. Etiam feugiat nibh ultrices velit tristique ornare. 
                      Proin tristique nulla at lectus hendrerit lacinia. 
                      Morbi tristique nisl turpis, in scelerisque mauris consequat vitae.</p>
                      <br>
                      <p style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 18px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                      class="white__text_90"
                      >King Regards,</p>
                      <br>
                      <p style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 18px;
                        font-weight: bold;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: left;
                      "
                      class="white__text_90"
                      >Isabel Hagbrink,<br>
                      <span style="font-weight: normal; font-size: 14px;"> Director of Communications</span></p>
                    </td>
                    <td width="20">&nbsp;</td>
                </tr>
            </table>
        </td>
        
    
    </tr>
    <tr>
        <td style="height: 40px" class="spacing" >&nbsp;</td>
    </tr>
</table>
<!-- END TEXT BLOCK-->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
`;

    return Text
}
