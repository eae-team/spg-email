import './main.css';

export const createTeaserRightPardot = () => {
    const teaserRight = `
      <!-- TEASER IMAGE RIGHT -->
      <table
        cellpadding="0"
        cellspacing="0"
        align="center"
        border="0"
        width="100%"
        style="max-width: 640px; width: 100%; margin: auto; background-color: #FFFFFF;"
        pardot-repeatable=""
        class="wrapper"
      >
        <tbody>
          <tr>
        
            <td valign="top" style="font-size: 0px" class="p-mobile">
              <div
                style="
                  display: none;
                  vertical-align: top;
                  height: 320px;
                  width: 366px;
                "
                class="deviceWidth heightAuto col-lge img-mobile"
              >
                <a
                pardot-region=""
                  href="https://www.southpole.com/"
                >
                  <img
                  pardot-region=""
                    valign="bottom"
                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126672/play-video-image_u1zm4v.png"
                    style="
                      width: 100%;
                      max-width: 100%;
                      margin: auto;
                      display: block;
                    "
                  />
                </a>
              </div>
              <!--[if mso]>
                <table role="presentation" width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td style="width:250px; height: 190px" valign="top">
                      <table role="presentation" width="100%" cellpadding="0" cellspacing="0" height="320">
                        <tr>
                          <td style="background: #98c21f; padding: 25px 5px 24px 15px;" valign="bottom">
              <![endif]-->
              <div
                style="
                  background: #98c21f;
                  height: 190px;
                  padding: 20px 0;
                  display: inline-block;
                  vertical-align: top;
                  width: 250px;
                  text-align: left;
                  margin: 0 auto;
                "
                class="deviceWidth heightAuto col-sml col-green"
              >
                <div style="padding: 0 10px 0 15px">
       
                  <h4 pardot-region=""
                    style="
                      font-size: 21px !important;
                      color: #1e355c;
                      font-weight: bold;
                      margin: 0 0 10px;
                      line-height: 24px;
                      font-family: Arial, Helvetica, sans-serif;
                    "
                  >
                    Pellentesque vel enim quis enim interdum viverra
                  </h4>
                  <p pardot-region=""
                    style="
                      font-size: 14px !important;
                      color: #1e355c;
                      margin: 0 0 20px;
                      line-height: 19px;
                      font-family: Arial, Helvetica, sans-serif;
                    "
                  >
                    Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla…
                  </p>

                  <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="left"
                    class="link"
                  >
                    <tbody>
                      <tr>
                        <td>
                          <a pardot-region=""
                            href="https://www.southpole.com/"
                            style="
                              font-size: 16px !important;
                              font-weight: bold;
                              color: #1e355c;
                              text-decoration: none;
                              font-family: Arial, Helvetica, sans-serif;
                            "
                          >
                            Learn more
                          </a>
                        </td>
                        <td style="padding-left: 8px">
                          <a href="#">
                            <img
                              heigh='8"'
                              width="5"
                              src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126630/arrow-right-2_uuxdqw.png"
                              alt=""
                          /></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!--[if mso]>
                    </td>
                  </tr>
                </table>
              </td>
              <td style="width:390px; padding: 0px;" align="left" valign="top">
                <![endif]-->
              <div
                style="
                  display: inline-block;
                  vertical-align: top;
                  height: 320px;
                  width: 390px;
                "
                class="deviceWidth heightAuto col-lge hidden"
              >
                <a
                  href="https://www.southpole.com/"
                >
                  <img
                    valign="bottom"
                    height="320"
                    pardot-region=""
                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126743/play-video-image-mobile_dxut0h.png"
                    style="
                      width: 100%;
                      max-width: 100%;
                      margin: auto;
                      display: block;
                    "
                  />
                </a>
              </div>
              <!--[if mso]>
                    </td>
                  </tr>
                </table>
              <![endif]-->
            </td>
        
          </tr>
        </tbody>
      </table>
      <!-- TEASER IMAGE RIGHT -->
        
      <!-- space -->
      <table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
        <tr>
            <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
        </tr>
      </table>
      <!-- END space -->
  `;

    return teaserRight;
};
