import { createHeaderViewOnBrowser } from './HeaderViewOnBrowser';

export default {
  title: 'Blocks/Header/View on Browser',
};

const Template = () => createHeaderViewOnBrowser();

export const HeaderComponent = Template.bind({});

