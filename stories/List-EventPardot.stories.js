import { createListEventPardot } from './List-EventPardot';

export default {
  title: 'Pardot Tags/List',
};

const Template = () => createListEventPardot();

export const ListEventComponent = Template.bind({});
