import { createHeaderViewOnBrowserPardot } from './HeaderViewOnBrowserPardot';

export default {
  title: 'Pardot Tags/Header/View on Browser',
};

const Template = () => createHeaderViewOnBrowserPardot();

export const HeaderComponent = Template.bind({});

