import { createTemplateGeneral5 } from './TemplateGeneral5';

export default {
  title: 'Pardot Templates/Template General 5',
};

const Template = () => createTemplateGeneral5();

export const templateGeneral5Component = Template.bind({});

