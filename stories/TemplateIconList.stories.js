import { createTemplateIconList } from './TemplateIconList';

export default {
  title: 'Pardot Templates/Template Icon List',
};

const Template = () => createTemplateIconList();

export const templateIconListComponent = Template.bind({});

