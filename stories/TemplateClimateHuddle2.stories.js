import { createTemplateClimateHuddle2 } from './TemplateClimateHuddle2';

export default {
  title: 'Pardot Templates/Template Climate Huddle2',
};

const Template = () => createTemplateClimateHuddle2();

export const templateClimateHuddle2Component = Template.bind({});

