import { createListLink } from './List-Link';

export default {
  title: 'Blocks/List',
};

const Template = () => createListLink();

export const ListLinkComponent = Template.bind({});
