import './main.css';

import {createSubHeadingPardot} from './SubHeadingPardot';

export const createTeaserTwoImagesPardot = () => {
    const subHeading = createSubHeadingPardot();
    const teaserTwoImages = `
    ${subHeading}
        <!-- TEASER TWO IMAGES-->
      <table
        cellpadding="0"
        cellspacing="0"
        align="center"
        border="0"
        width="640"
        style="max-width: 640px; width: 100% !important; margin: auto; background-color: #FFFFFF;"
        pardot-repeatable=""
        class="wrapper"
      >
        <tbody>
          <tr>
            <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
          </tr>
          <tr>
            <td style="font-size: 0px" class="p-mobile" align="center">
            <!--[if mso]>
                <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td style="width:290px; padding: 0 15px;" align="left" valign="top">
              <![endif]-->
            <div
                style="
                  display: inline-block;
                  vertical-align: top;
                  width: 50%;
                  margin:0;
                "
                class="deviceWidth heightAuto col-lge mobile-margin-bottom"
              >
              <!--[if mso]>
                <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td style="width:290px; padding: 0px;" align="left" valign="top">
              <![endif]-->
              <div
                style="
                  display: block;
                  vertical-align: top;
                  height: 160px;
                  width: 290px;
                
                "
                class="deviceWidth heightAuto"
              >
                <img
                  valign="bottom"
                  height="160"
                  pardot-region=""
                  src="https://res.cloudinary.com/eae-pt/image/upload/v1625571324/two_images_vdktoe.png"
                  style="
                    width: 100%;
                    max-width: 100%;
                    margin: auto;
                    display: block;
                  "
                 
                />
              </div>
              <!--[if mso]>
                    </td>
                </tr>
                <tr>
                    <td style="width:290px; padding: 20px 0px;" align="left" valign="top" bgcolor="#fdc300">
              <![endif]-->
              
              <div
                style="
                  background: #fdc300;
                  height: 205px;
                  padding: 20px 0;
                  display: block;
                  vertical-align: top;
                  width: 290px;
                  text-align: left;
                 
               
                "
                class="deviceWidth heightAuto col-yellow teaser-content"
              >

              <!--[if mso]>
                <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td style="padding: 0 10px 0 15px">
              <![endif]-->
                <div style="padding: 0 10px 0 15px">
                  
                  <h4 pardot-region=""
                    style="
                      font-size: 21px !important;
                      color: #1e355c;
                      font-weight: bold;
                      margin: 0 0 10px;
                      line-height: 24px;
                      font-family: Arial, Helvetica, sans-serif;
                    "
                  >
                    Pellentesque vel enim quis enim interdum viverra
                  </h4>
                  <p pardot-region=""
                    style="
                      font-size: 14px !important;
                      color: #1e355c;
                      margin: 0 0 20px;
                      line-height: 19px;
                      font-family: Arial, Helvetica, sans-serif;
                    "
                  >
                   Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla…
                  </p>

                  <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="left"
                    class="link"
                  >
                    <tbody>
                      <tr>
                        <td>
                          <a pardot-region=""
                            href="https://www.southpole.com/"
                            style="
                              font-size: 16px !important;
                              font-weight: bold;
                              color: #1e355c;
                              text-decoration: none;
                              font-family: Arial, Helvetica, sans-serif;
                            "
                          >
                            Learn more &gt;
                          </a>
                        </td>
                       
                      </tr>
                      <tr>
                        <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!--[if mso]>
                    </td>
                </tr>
                </table>
              <![endif]-->
              </div>
              <!--[if mso]>
                    </td>
                </tr>
                </table>
              <![endif]-->
              
              
              </div>
              
              <!--[if mso]>
                </td>
                <td width="60">&nbsp;</td>
                <td style="width:290px; padding: 0 15px;" align="left" valign="top">
              <![endif]-->
            <div
                style="
                  display: inline-block;
                  vertical-align: top;
                  width: 50%;
                  margin: 0;
                "
                class="deviceWidth heightAuto col-lge"
              >              
                 <!--[if mso]>
                <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td style="width:290px; padding: 0px;" align="left" valign="top">
              <![endif]-->
              <div
                style="
                  display: block;
                  vertical-align: top;
                  height: 160px;
                  width: 290px;
                 
                "
                class="deviceWidth heightAuto"
              >
                <img
                  valign="bottom"
                  height="160"
                  pardot-region=""
                  src="https://res.cloudinary.com/eae-pt/image/upload/v1625571111/two_images_2_ysxup1.png"
                  style="
                    width: 100%;
                    max-width: 100%;
                    margin: auto;
                    display: block;
                  "
                 
                />
                
              </div>
              <!--[if mso]>
                    </td>
                </tr>
                <tr>
                    <td style="width:290px; padding: 20px 0px;" align="left" valign="top" bgcolor="#98C21F">
              <![endif]-->
              <div
                style="
                  background: #98C21F;
                  height: 205px;;
                  padding: 20px 0;
                  display: block;
                  vertical-align: top;
                  width: 290px;
                  text-align: left;
                  margin: 0;
             
                "
                class="deviceWidth heightAuto col-green teaser-content"
              >
              <!--[if mso]>
                <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td style="padding: 0 10px 0 15px">
              <![endif]-->
                <div style="padding: 0 10px 0 15px">
                  
                  <h4 pardot-region=""
                    style="
                      font-size: 21px !important;
                      color: #1e355c;
                      font-weight: bold;
                      margin: 0 0 10px;
                      line-height: 24px;
                      font-family: Arial, Helvetica, sans-serif;
                    "
                  >
                    Pellentesque vel enim quis enim interdum viverra
                  </h4>
                  <p pardot-region=""
                    style="
                      font-size: 14px !important;
                      color: #1e355c;
                      margin: 0 0 20px;
                      line-height: 19px;
                      font-family: Arial, Helvetica, sans-serif;
                    "
                  >
                   Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla…
                  </p>

                  <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="left"
                    class="link"
                  
                    
                  >
                    <tbody>
                      <tr>
                        <td>
                          <a pardot-region=""
                            href="https://www.southpole.com/"
                            style="
                              font-size: 16px !important;
                              font-weight: bold;
                              color: #1e355c;
                              text-decoration: none;
                              font-family: Arial, Helvetica, sans-serif;
                            "
                          >
                            Learn more &gt;
                          </a>
                        </td>
                        
                      </tr>
                      <tr>
                        <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!--[if mso]>
                    </td>
                </tr>
                </table>
              <![endif]-->
              </div>
            <!--[if mso]>
                    </td>
                </tr>
                </table>
              <![endif]-->
              
              </div>
              <!--[if mso]>
                </td>
               </tr>
               </table>
              <![endif]-->
            </td>
            
          </tr>
          <tr>
            <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
          </tr>
        </tbody>
      </table>
      <!-- END TEASER TWO IMAGES-->
      <!-- space -->
      <table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
        <tr>
            <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
        </tr>
      </table>
      <!-- END space -->
  `;

    return teaserTwoImages;
};
