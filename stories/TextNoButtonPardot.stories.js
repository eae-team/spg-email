import {createTextNoButtonPardot} from './TextNoButtonPardot';

export default {
  title: 'Pardot Tags/Text No Button',
};

const Template = () => createTextNoButtonPardot();

export const TextNoButtonComponent = Template.bind({});
