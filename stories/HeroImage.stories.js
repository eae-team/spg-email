import {createHeroImage} from './HeroImage';

export default {
  title: 'Blocks/Hero/Image',
};

const Template = () => createHeroImage();

export const HeroComponent = Template.bind({});

