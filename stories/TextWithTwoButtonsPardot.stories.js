import {createTextWithTwoButtonsPardot} from './TextWithTwoButtonsPardot';

export default {
  title: 'Pardot Tags/Text/Text With Two Buttons',
};

const Template = () => createTextWithTwoButtonsPardot();

export const TextWithTwoButtonsComponent = Template.bind({});
