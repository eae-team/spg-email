import {createHeader} from './Header';
import {createLogosRow} from './Logos-Row';
import {createHero} from './Hero';
import {createIntro} from './Intro';
import {createTeaserFull} from './Teaser-Image-Full';
import {createTeaserLeft} from './Teaser-Image-Left';
import {createTeaserRight} from './Teaser-Image-Right';
import {createTeaserTwoImages} from './Teaser-Two-Images';
import {createListArticle} from './List-Article';
import {createListEvent} from './List-Event';
import {createSpeaker} from './Speaker';
import {createFooter} from './Footer';
import {createDisclaimer} from './Disclaimer';
import {createText} from './Text';
import {createMixMedia} from './MixMedia';
import {createFullCTADarkBlue} from './FullCTADarkBlue';
import {createEditorBlock} from './EditorBlock';
import {createPartners} from './Partners';
import {createHighlight} from './Highlight';
import {createQuoteBlock} from './QuoteBlock';


export const createPage = () => {
    const header = createHeader();
    const logoRow = createLogosRow();
    const hero = createHero();
    const intro = createIntro();
    const teaserFull = createTeaserFull();
    const teaserLeft = createTeaserLeft();
    const teaserRight = createTeaserRight();
    const teaserTwoImages = createTeaserTwoImages();
    const listArticle = createListArticle();
    const listEvent = createListEvent();
    const speaker = createSpeaker();
    const footer = createFooter();
    const disclaimer = createDisclaimer();
    const text = createText();
    const mixmedia = createMixMedia();
    const fullCTADarkBlue = createFullCTADarkBlue();
    const editorBlock = createEditorBlock();
    const partners = createPartners();
    const highlight = createHighlight();
    const quote = createQuoteBlock();


    const section = `
 
                
                        ${header}
                        ${logoRow}
                        ${hero}
                   
                        ${teaserFull}
                      
                        ${listArticle}
                        ${listEvent}
                        ${text}
                        ${speaker}
                        ${mixmedia}
                        
                        ${partners}
                        ${editorBlock}
                        ${fullCTADarkBlue}
                        
                        ${highlight}
                        ${quote}
                        
                        ${footer}
                        ${disclaimer}


`;

    return section;
};
