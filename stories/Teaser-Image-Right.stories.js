import { createTeaserRight } from './Teaser-Image-Right';

export default {
  title: 'Blocks/Teaser',
};

const Template = () => createTeaserRight();

export const TeaserRightComponent = Template.bind({});

