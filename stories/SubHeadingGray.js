import './main.css';

export const createSubHeadingGray = () => {
    const subHeadingGray = `<!-- SUB-HEADING -->
    <table cellpadding="20"
    cellspacing="0"
    border="0"
    style="max-width: 640px; width: 100%; background: #F2F4F7;"
    class="wrapper text-block"
    align="center">
 
        <tr>      
            <td style="padding: 20px 0 0;">
                <h2
                    style="
                    color: #005091;
                    font-size: 32px;
                    font-weight: bold;
                    margin: 0;
                    font-family: Arial, Helvetica, sans-serif;
                    text-align: center;
                    padding-top: 12px;
                "
                class="white__text"
                >About the Brands</h2>
                <!-- space -->
                <table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tr>
                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                    </tr>
                </table>
                <!-- END space -->
            </td>
        </tr>
    </table>


`;

    return subHeadingGray;
};
