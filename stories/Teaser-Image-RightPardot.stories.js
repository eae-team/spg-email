import { createTeaserRightPardot } from './Teaser-Image-RightPardot';

export default {
  title: 'Pardot Tags/Teaser',
};

const Template = () => createTeaserRightPardot();

export const TeaserRightComponent = Template.bind({});

