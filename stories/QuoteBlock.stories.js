import {createQuoteBlock} from './QuoteBlock';

export default {
  title: 'Blocks/Quote Block',
};

const Template = () => createQuoteBlock();

export const QuoteBlockComponent = Template.bind({});
