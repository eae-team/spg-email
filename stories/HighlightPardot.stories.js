import {createHighlightPardot} from './HighlightPardot';

export default {
  title: 'Pardot Tags/Highlight',
};

const Template = () => createHighlightPardot();

export const HighlightComponent = Template.bind({});
