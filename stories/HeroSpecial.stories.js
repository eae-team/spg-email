import {createHeroSpecial} from './HeroSpecial';

export default {
  title: 'Blocks/Hero/Special',
};

const Template = () => createHeroSpecial();

export const HeroComponent = Template.bind({});

