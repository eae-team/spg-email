import { createSubHeadingGray } from './SubHeadingGray';

export default {
  title: 'Blocks/Sub-Heading/Background Gray',
};

const Template = () => createSubHeadingGray();

export const SubHeadingGrayComponent = Template.bind({});