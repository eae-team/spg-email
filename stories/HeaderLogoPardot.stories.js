import { createHeaderLogoPardot } from './HeaderLogoPardot';

export default {
  title: 'Pardot Tags/Header/Logo Only',
};

const Template = () => createHeaderLogoPardot();

export const HeaderComponent = Template.bind({});

