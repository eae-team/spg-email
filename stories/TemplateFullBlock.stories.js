import { createTemplateFullBlock } from './TemplateFullBlock';

export default {
  title: 'Pardot Templates/Template FullBlock',
};

const Template = () => createTemplateFullBlock();

export const templateFullBlockComponent = Template.bind({});

