import { createTemplateFeaturedItems } from './TemplateFeaturedItems';

export default {
  title: 'Pardot Templates/Template Featured Items',
};

const Template = () => createTemplateFeaturedItems();

export const templateFeaturedItemsComponent = Template.bind({});

