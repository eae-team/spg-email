import './main.css';

export const createFullCTALightBlue = () => {
    const fullCTA = `

<!-- FULL CTA LIGHT BLUE BLOCK-->
<table cellpadding="20"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background-color: #019CDB"
       class=""
       align="center">

    <tr>
        <td align="center">
            <h2
                style="
                color: #FFFFFF;
                font-size: 21px;
                font-weight: bold;
                margin: 0;
                font-family: Arial, Helvetica, sans-serif;
                text-align: center;
                "
     
            >Looking for more CDP Resources?</h2>
            <br>
            <a href="#" style="
                color: #FFFFFF;
                font-size: 16px;
                font-weight: bold;
                text-decoration: none;
                margin: 0;
                font-family: Arial, Helvetica, sans-serif;
                text-align: center;
                ">Download our Tool &gt; </a>
        </td>
    </tr>

</table>
<!-- END TEXT BLOCK-->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->



`;

    return fullCTA
}
