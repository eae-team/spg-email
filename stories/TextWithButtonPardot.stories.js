import {createTextWithButtonPardot} from './TextWithButtonPardot';

export default {
  title: 'Pardot Tags/Text/Text With Button',
};

const Template = () => createTextWithButtonPardot();

export const TextWithButtonComponent = Template.bind({});
