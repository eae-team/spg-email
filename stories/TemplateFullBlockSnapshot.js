export const createTemplateFullBlockSnapshot = () => {

    const templateFullBlockSnapshot = `
    <!-- VIEW IN BROWSER -->
    <table cellpadding="0" cellspacing="0" border="0" align="center">
        <tbody>
            <tr>
                <td style="padding: 20px 0">
                    <a
                        href="{{View_Online}}"
                        pardot-region=""
                        style="
                  color: #005091;
                  font-size: 11px;
                  line-height: 14px;
                  font-family: Arial, Helvetica, sans-serif;
                "
                    >
                        View in browser
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END VIEW IN BROWSER -->
    <!-- HEADER TWO LOGOS -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        pardot-removable=""
        class="header"
        align="center"
    >
        <tbody>
            <tr>
                <td style="padding: 18px">
                    <table
                        align="center"
                        cellpadding="0"
                        cellspacing="0"
                        border="0"
                    >
                        <tbody>
                            <tr>
                                <td
                                    style="border-right: 1px solid #e4e9f1; padding-right: 18px"
                                >
                                    <a
                                        pardot-region=""
                                        href="https://www.southpole.com/"
                                        target="_blank"
                                    >
                                        <img
                                            src="https://go.southpole.com/sp-logo-transp-rgb"
                                            width="197"
                                            height="40"
                                            pardot-region=""
                                            alt=""
                                            border="0"
                                            style="display: block"
                                            class="logo"
                                        />
                                        <!--[if !mso]>-->
                                        <img
                                            src="https://go.southpole.com/l/881182/2022-08-24/3r2kt/881182/1661327460VOKcM8hs/sp_logo_white.png"
                                            width="197"
                                            height="40"
                                            alt=""
                                            pardot-region=""
                                            border="0"
                                            style="display: none"
                                            class="logo-white"
                                        />
                                        <!--<![endif]-->
                                    </a>
                                </td>
                                <td
                                    style="
                      font-size: 12px;
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      padding-left: 18px;
                    "
                                    class="header-content"
                                pardot-region="">
                                    <img
                                        src="https://res.cloudinary.com/eae-pt/image/upload/v1626796149/cdp_q7yz38.png"
                                        width="96"
                                        height="41"
                                        pardot-region=""
                                        alt=""
                                        border="0"
                                        style="display: block"
                                        class="logo"
                                    />
                                    <!--[if !mso]>-->
                                    <img
                                        src="https://res.cloudinary.com/eae-pt/image/upload/v1626796156/cdp_white_mdpkhn.png"
                                        width="96"
                                        height="41"
                                        pardot-region=""
                                        alt=""
                                        border="0"
                                        style="display: none"
                                        class="logo-white"
                                    />
                                    <!--<![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HEADER TWO LOGOS -->
    
    <!-- space -->
    <table
        class="bg-table"
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    <!-- HEADER -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        align="center"
    >
        <tbody>
            <tr>
                <td style="padding: 18px">
                    <table
                        align="center"
                        cellpadding="0"
                        cellspacing="0"
                        border="0"
                    >
                        <tbody>
                            <tr>
                                <td
                                    style="border-right: 1px solid #e4e9f1; padding-right: 18px"
                                >
                                    <a
                                        href="https://www.southpole.com/"
                                        target="_blank"
                                    >
                                        <img pardot-region=""
                                            src="https://go.southpole.com/sp-logo-transp-rgb"
                                            width="197"
                                            height="40"
                                            alt=""
                                            border="0"
                                            style="display: block"
                                            class="logo"
                                        />
                                        <!--[if !mso]>-->
                                        <img
                                            src="https://go.southpole.com/l/881182/2022-08-24/3r2kt/881182/1661327460VOKcM8hs/sp_logo_white.png"
                                            width="197"
                                            height="40"
                                            alt=""
                                            border="0"
                                            style="display: none"
                                            class="logo-white"
                                        />
                                        <!--<![endif]-->
                                    </a>
                                </td>
                                <td
                                    style="
                      font-size: 12px;
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      padding-left: 18px;
                    "
                                    class="header-content"
                                pardot-region="">
                                    <p
                                        style="
                        font-size: 18px;
                        font-family: Arial, Helvetica, sans-serif;
                        font-weight: bold;
                        margin: 0;
                      "
                                    >
                                        South Pole Snapshot
                                    </p>
                                    March 2021
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HEADER -->
    
    <!-- space -->
    <table
        class="bg-table"
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    <!-- HEADER LOGO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
    >
        <tbody>
            <tr>
                <td style="padding: 18px">
                    <table
                        align="center"
                        cellpadding="0"
                        cellspacing="0"
                        border="0"
                    >
                        <tbody>
                            <tr>
                                <td>
                                    <a
                                        href="https://www.southpole.com/"
                                        target="_blank"
                                    >
                                        <img
                                            src="https://go.southpole.com/sp-logo-transp-rgb"
                                            width="197"
                                            height="40"
                                            pardot-region=""
                                            alt=""
                                            border="0"
                                            style="display: block;width: 100%;height: 100%;"
                                            class="logo"
                                        />
                                        <!--[if !mso]>-->
                                        <img
                                            src="https://go.southpole.com/l/881182/2022-08-24/3r2kt/881182/1661327460VOKcM8hs/sp_logo_white.png"
                                            width="197"
                                            height="40"
                                            pardot-region=""
                                            alt=""
                                            border="0"
                                            style="display: none;width: 100%;height: 100%;"
                                            class="logo-white"
                                        />
                                        <!--<![endif]-->
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HEADER LOGO -->
    
    <!-- space -->
    <table
        class="bg-table"
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <!-- HERO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                          max-width: 640px;
                          width: 100%;
                          margin: auto;
                          text-align: center;
                        "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr>
                                <td
                                    background="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                    width="640"
                                    height="355"
                                    pardot-region=""
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                <!-- CHANGE OUTLOOK BANNER HERE -->
                                    <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                    style="width:640px;height:355px;">
                                <v:fill type="frame"
                                        pardot-region=""
                                        src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                        color="#005091"/>
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                               <!-- END CHANGE OUTLOOK BANNER HERE -->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="center"
                                            width="100%"
                                            style="max-width: 500px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="center"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="center"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                      text-align: center;
                                      padding: 78px 40px 0;
                                      text-align: center;
                                    "
                                                                        class="header__title"
                                                                    >
                                                                        <h1
                                                                            pardot-region=""
                                                                            style="
                                        margin: 0;
                                        font-size: 40px;
                                        line-height: 42px;
                                        color: #ffffff;
                                        font-weight: bold;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                                                                        >
                                                                           How can we support your climate journey?
                                                                        </h1>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                      text-align: center;
                                      padding: 5px 60px 77px 60px;
                                      font-size: 22px;
                                      line-height: 28px;
                                      color: #ffffff;
                                      font-family: Arial, Helvetica,
                                        sans-serif;
                                      text-align: center;
                                    "
                                                                        class="header__text"
                                                                        pardot-region=""
                                                                    >
                                                                        <p
                                                                            style="margin: 0"
                                                                        >
                                                                            Welcome
                                                                            to 2021
                                                                            - we
                                                                            hope
                                                                            you’re
                                                                            ready
                                                                            for a
                                                                            climate
                                                                            action-packed
                                                                            year!
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    
    <!-- space -->
    <table
        class="bg-table"
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        pardot-removable=""
        class="header"
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                        max-width: 640px;
                        width: 100%;
                        margin: auto;
                        text-align: center;
                    "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr>
                                <td
                                    class="header__title"
                                    style="padding-top: 40px;"
                                >
                                    <h1
                                        pardot-region=""
                                        style="
                                    margin: 0;
                                    font-size: 40px;
                                    line-height: 42px;
                                    color: #005091;
                                    font-weight: bold;
                                    font-family: Arial, Helvetica,
                                    sans-serif;
                                    "
                                    >
                                        Register for the Webinar
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    class="header__text"
                                    style="text-align: center;
                            font-size: 22px;
                            line-height: 28px;
                            color: #005091;
                            font-family: Arial, Helvetica,
                            sans-serif;"
                                >
                                    <div style="display: inline-block;">
                                        <p
                                            pardot-region=""
                                            style="margin-bottom: 0; padding-bottom: 5px;"
                                        >
                                            Improve your CDP performance in 2021
                                        </p>
                                         <hr style="border-bottom: 1px solid #005091;">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        align="center"
                                        border="0"
                                        width="100%"
                                        style="
                                    max-width: 602px;
                                    width: 100%;
                                    margin: auto;
                                    text-align: center;
                                    "
                                        class="deviceWidth"
                                    >
                                        <tbody>
                                            <tr>
                                                <td
                                                    class="header__image"
                                                    style="padding-top: 40px;"
                                                >
                                                    <!-- CHANGE BANNER HERE --><img alt="" border="0" height="100%" src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                                        width="100%"
                                                        height="100%"
                                                        alt=""
                                                        border="0"
                                                        style="display: block; height: 100%; width: 100%; object-fit: cover;"
                                                    /> <!-- END CHANGE BANNER HERE -->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <!-- HERO INFO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                              max-width: 640px;
                              width: 100%;
                              margin: auto;
                              text-align: center;
                            "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr> <!-- CHANGE BANNER HERE -->
                                <td 
                                    background="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                    width="640"
                                    height="355"
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                <!-- CHANGE OUTLOOK BANNER HERE -->
                                    <!--[if mso]>
        
                                <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 640px; height:355px;" src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg" />                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 640px; height:355px;">
                                <v:fill  opacity="0%" color="#005091"  />
                                <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="center"
                                            width="100%"
                                            style="max-width: 500px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="center"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="center"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                          text-align: center;
                                          padding: 78px 40px 0;
                                          text-align: center;
                                        "
                                                                        class="header__title"
                                                                    >
                                                                        <h1
                                                                            pardot-region=""
                                                                            style="
                                            margin: 0;
                                            font-size: 40px;
                                            line-height: 42px;
                                            color: #ffffff;
                                            font-weight: bold;
                                            font-family: Arial, Helvetica,
                                              sans-serif;
                                          "
                                                                        >
                                                                           Crossing the Line to Net Zero
                                                                        </h1>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                          text-align: center;
                                          padding: 5px 60px 77px 60px;
                                          font-size: 22px;
                                          line-height: 28px;
                                          color: #ffffff;
                                          font-family: Arial, Helvetica,
                                            sans-serif;
                                          text-align: center;
                                        "
                                                                        class="header__text"
                                                                    >
                                                                        <div
                                                                            style="display: inline-block; padding-bottom: 22px;"
                                                                        >
                                                                            <p
                                                                                pardot-region=""
                                                                                style="margin: 0; padding-top: 15px; padding-bottom: 3px;"
                                                                            >
                                                                               A new South Pole Report: The State of Net Zero
                                                                            </p>
                                                                          <hr>
                                                                        </div>
                                                                        <br />
                                                                        <!--[if !mso]>-->
                                                                        <a
                                                                            pardot-region=""
                                                                            href="http://"
                                                                            style="background-color:#FFFFFF;
                                                               border-radius:25px;color:#005091;
                                                               display:inline-block;font-family:sans-serif;
                                                               font-size:13px;font-weight:bold;line-height:40px;
                                                               text-align:center;text-decoration:none;width:175px;
                                                               -webkit-text-size-adjust:none;"
                                                                        >
                                                                            Register
                                                                            Today
                                                                            &raquo;
                                                                        </a>
                                                                        <!--<![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- CHANGE OUTLOOK CTA HERE -->
                                    <!--[if mso]>
                                </v:textbox>
                                </v:fill>
                                </v:rect>
                                </v:image>
                                 <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="#" style="height:40px;width:175px;position:absolute;top:235px;left:232px;" arcsize="63%" fillcolor="#FFFFFF" stroke="false">
                                <center style="color:#005091;-webkit-text-size-adjust:none;font-family:sans-serif;font-size:13px;font-weight:bold;">Register Today &raquo;</center>
                                </v:roundrect>
                                <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    <!-- HERO INFO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                              max-width: 640px;
                              width: 100%;
                              margin: auto;
                              text-align: center;
                            "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr> <!-- CHANGE BANNER HERE -->
                                <td 
                                    background="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                    width="640"
                                    height="355"
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                <!-- CHANGE OUTLOOK BANNER HERE -->
                                    <!--[if mso]>
        
                                <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 640px; height:355px;" src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg" />                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 640px; height:355px;">
                                <v:fill  opacity="0%" color="#005091"  />
                                <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="center"
                                            width="100%"
                                            style="max-width: 500px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="center"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="center"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                          text-align: center;
                                          padding: 78px 40px 0;
                                          text-align: center;
                                        "
                                                                        class="header__title"
                                                                    >
                                                                        <h1
                                                                            pardot-region=""
                                                                            style="
                                            margin: 0;
                                            font-size: 40px;
                                            line-height: 42px;
                                            color: #ffffff;
                                            font-weight: bold;
                                            font-family: Arial, Helvetica,
                                              sans-serif;
                                          "
                                                                        >
                                                                          Improve your CDP performance in 2022
                                                                        </h1>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        valign="top"
                                                                        style="
                                          text-align: center;
                                          padding: 5px 60px 77px 60px;
                                          font-size: 22px;
                                          line-height: 28px;
                                          color: #ffffff;
                                          font-family: Arial, Helvetica,
                                            sans-serif;
                                          text-align: center;
                                        "
                                                                        class="header__text"
                                                                    >
                                                                        <div
                                                                            style="display: inline-block; padding-bottom: 22px;"
                                                                        >
                                                                            <p
                                                                                pardot-region=""
                                                                                style="margin: 0; padding-top: 15px; padding-bottom: 3px;"
                                                                            >
                                                                              Wednesday 24 March, 14:00 GMT
    
                                                                            </p>
                                                                          <hr>
                                                                        </div>
                                                                        <br />
                                                                        <!--[if !mso]>-->
                                                                        <a
                                                                            pardot-region=""
                                                                            href="http://"
                                                                            style="background-color:#FFFFFF;
                                                               border-radius:25px;color:#005091;
                                                               display:inline-block;font-family:sans-serif;
                                                               font-size:13px;font-weight:bold;line-height:40px;
                                                               text-align:center;text-decoration:none;width:175px;
                                                               -webkit-text-size-adjust:none;"
                                                                        >
                                                                            Register
                                                                            Today
                                                                            &raquo;
                                                                        </a>
                                                                        <!--<![endif]-->
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- CHANGE OUTLOOK CTA HERE -->
                                    <!--[if mso]>
                                </v:textbox>
                                </v:fill>
                                </v:rect>
                                </v:image>
                                 <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="#" style="height:40px;width:175px;position:absolute;top:235px;left:232px;" arcsize="63%" fillcolor="#FFFFFF" stroke="false">
                                <center style="color:#005091;-webkit-text-size-adjust:none;font-family:sans-serif;font-size:13px;font-weight:bold;">Register Today &raquo;</center>
                                </v:roundrect>
                                <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->
    
    <!-- space -->
    <table
        class="bg-table"
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        pardot-repeatable=""
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END space -->
    
    <!-- HERO INFO -->
    <table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        pardot-removable=""
        class="header"
        align="center"
    >
        <tbody>
            <tr>
                <td>
                    <!-- HEADER-HERO : BEGIN -->
                    <table
                        cellpadding="0"
                        cellspacing="0"
                        align="center"
                        border="0"
                        width="100%"
                        style="
                          max-width: 640px;
                          width: 100%;
                          margin: auto;
                          text-align: center;
                        "
                        class="deviceWidth"
                    >
                        <tbody>
                            <tr>
                                 <!-- CHANGE BANNER HERE -->
                                <td
                                    background="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                    width="640"
                                    height="355"
                                    pardot-region=""
                                    valign="top"
                                    style="background-repeat: no-repeat; background-size: cover"
                                    class="deviceWidth header"
                                >
                                 <!-- CHANGE OUTLOOK BANNER HERE -->
                                    <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                    style="width:640px;height:355px;">
                                <v:fill type="frame"
                                        pardot-region=""
                                        src="https://go.southpole.com/l/881182/2022-02-02/2hxpw/881182/1643790397aQSvPF6Y/hero_image_default.jpeg"
                                        color="#005091"/>
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                                    <div>
                                        <table
                                            role="presentation"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            align="right"
                                            width="100%"
                                            style="max-width: 640px; margin: auto"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        align="right"
                                                        valign="middle"
                                                    >
                                                        <table
                                                            role="presentation"
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            align="right"
                                                            width="100%"
                                                            class="header__content"
                                                        >
                                                            <tbody>
                                                                <tr>
                                                                    <td
                                                                        align="right"
                                                                        style="padding: 15px 15px;"
                                                                    >
                                                                        <p
                                                                            pardot-region=""
                                                                            style="margin: 0; text-align:right; font-size: 11px !important; color: #fff; font-family: Arial, Helvetica, sans-serif;"
                                                                        >
                                                                            Lorem
                                                                            ipsum
                                                                            dolor
                                                                            sit
                                                                            caption
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END HERO INFO -->
    <!-- TEXT BLOCK -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="header" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff">
                                <tbody>
                                    <tr>
                                        <td class="spacing" style="height: 50px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="intro" role="presentation" style="
              text-align: center;
              width: 100%;
              margin: auto;
            " width="100%">
                                            <tbody>
                                                <tr>
                                                    <td class="intro-text" style="padding: 0px 40px">
                                                    <table border="0" cellpadding="0" cellspacing="0" id="snapshot" role="presentation">
                                                        <tbody>
                                                            <tr>
                                                                <td pardot-region="">
                                                                <h2 style="
                            color: #005091;
                            font-size: 21px;
                            line-height: 27px;
                            font-weight: bold;
                            margin: 0;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: center;
                          ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel enim quis enim interdum viverra ut ut augue. Donec consequat purus sed nunc.</h2>
                                                                &nbsp;
    
                                                                <p class="blue" style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: center;
                          ">Ut sagittis enim ut risus ultrices, ac consequat quam volutpat. Morbi rutrum euismod ipsum ut ornare. Sed vestibulum hendrerit elit, non placerat nisl consequat ac. Nunc eget tempus sem. Duis viverra eu felis suscipit scelerisque.</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
    
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                    <div style="margin-top: 30px;"><!--[if mso]>
                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                             xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
                                             style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" stroke="f"
                                             fillcolor="#019CDB">
                                    <w:anchorlock/>
                                    <center>
                                <![endif]--><a href="http://" pardot-region="" pardot-removable="" style="background-color:#019CDB;border-radius:25px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:175px;-webkit-text-size-adjust:none;">Discover the ways</a> <!--[if mso]>
                                </center>
                                </v:roundrect>
                                <![endif]--></div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacing" style="height: 50px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END TEXT BLOCK --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- TEXT BLOCK-->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper text-block" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff">
                                <tbody>
                                    <tr>
                                        <td class="spacing" style="height: 40px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" style="max-width: 640px; width: 100%; background: #ffffff">
                                            <tbody>
                                                <tr>
                                                    <td width="20">&nbsp;</td>
                                                    <td pardot-region="">
                                                    <p class="blue" style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            
                          ">Ut sagittis enim ut risus ultrices, ac consequat quam volutpat. Morbi rutrum euismod ipsum ut ornare. Sed vestibulum hendrerit elit, non placerat nisl consequat ac. Nunc eget tempus sem. Duis viverra eu felis suscipit scelerisque.</p>
                                                    &nbsp;
    
                                                    <p class="white__text_90" style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel enim quis enim interdum viverra ut ut augue. Donec consequat purus sed nunc.</p>
                                                    &nbsp;
    
                                                    <p class="white__text_90" style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          ">Etiam auctor, tellus id malesuada aliquet, leo mauris varius diam, a porta massa dui efficitur augue. Praesent eget luctus elit. In semper dui et euismod fermentum. Vestibulum bibendum, magna sed eleifend dapibus, orci ipsum finibus magna, eu egestas nisl erat in nulla. Aliquam et vulputate felis, ut ullamcorper augue. Etiam feugiat nibh ultrices velit tristique ornare. Proin tristique nulla at lectus hendrerit lacinia. Morbi tristique nisl turpis, in scelerisque mauris consequat vitae.</p>
                                                    &nbsp;
    
                                                    <p class="white__text_90" style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          ">King Regards,</p>
                                                    &nbsp;
    
                                                    <p class="white__text_90" style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 18px;
                            font-weight: bold;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                          ">Isabel Hagbrink,<br>
                                                    <span style="font-weight: normal; font-size: 14px;">Director of Communications</span></p>
                                                    </td>
                                                    <td width="20">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacing" style="height: 40px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END TEXT BLOCK--><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- EDITOR BLOCK-->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper text-block" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff">
                                <tbody>
                                    <tr>
                                        <td class="spacing" style="height: 40px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" style="max-width: 640px; width: 100%; background: #ffffff">
                                            <tbody>
                                                <tr>
                                                    <td width="40">&nbsp;</td>
                                                    <td pardot-region="">
                                                    <h2 class="white__text" style="
                                    color: #005091;
                                    font-size: 32px;
                                    font-weight: bold;
                                    margin: 0;
                                    font-family: Arial, Helvetica, sans-serif;
                                    text-align: center;
                                ">Notes to the editor on the beef cattle herd management method:</h2>
                                                    &nbsp;
    
                                                    <p class="white__text_90" style="
                                margin: 0;
                                color: #485B7B;
                                font-size: 16px;
                                line-height: 24px;
                                font-family: Arial, Helvetica, sans-serif;
                                text-align: left;
                              ">Ut sagittis enim ut risus ultrices, ac consequat quam volutpat. Morbi rutrum euismod ipsum ut ornare. Sed vestibulum hendrerit elit, non placerat nisl consequat ac.</p>
    
                                                    <p class="white__text_90" style="
                            margin: 0;
                            color: #485B7B;
                            font-size: 16px;
                            line-height: 24px;
                            font-family: Arial, Helvetica, sans-serif;
                            text-align: left;
                            "><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Ut sagittis enim ut risus ultrices, ac <a href="#" style="color: #005091;"> consequat</a> quam</p>
    
                                                    <p class="white__text_90" style="
                                margin: 0;
                                color: #485B7B;
                                font-size: 16px;
                                line-height: 24px;
                                font-family: Arial, Helvetica, sans-serif;
                                text-align: left;
                              "><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Morbi rutrum euismod ipsum ut ornare</p>
    
                                                    <p class="white__text_90" style="
                                margin: 0;
                                color: #485B7B;
                                font-size: 16px;
                                line-height: 24px;
                                font-family: Arial, Helvetica, sans-serif;
                                text-align: left;
                              "><img alt="" border="0" height="9" src="https://res.cloudinary.com/eae-pt/image/upload/v1625696654/icon_check_melcye.png" style="display: inline-block" width="12"> Sed hendrerit elit, non placerat nisl consequat ac From commitment to</p>
                                                    </td>
                                                    <td width="40">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="40">&nbsp;</td>
                                                    <td style="background:none; border-bottom: 1px solid #e6e6e6; height:1px; width:100%; margin:0px 0px 0px 0px;">&nbsp;</td>
                                                    <td width="40">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="40">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td width="40">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="185">&nbsp;</td>
                                                    <td align="center" width="270">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" style="max-width: 270px; background: #ffffff; text-align: center;" width="270">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center"><img pardot-region="" src="https://res.cloudinary.com/eae-pt/image/upload/v1625576775/speaker_q7vx7g.png" style="
                                          width: 107px;
                                          max-width: 100%;
                                          margin: auto;
                                          display: block;
                                        " valign="top">
                                                                <h2 class="white__text" pardot-region="" style="
                                            color: #005091;
                                            font-size: 18px;
                                            font-weight: bold;
                                            margin: 0;
                                            font-family: Arial, Helvetica, sans-serif;
                                            text-align: center;
                                        ">Stephen Breslin</h2>
    
                                                                <p class="white__text_90" pardot-region="" style="
                                        margin: 0;
                                        color: #005091;
                                        font-size: 14px;
                                        line-height: 24px;
                                        font-family: Arial, Helvetica, sans-serif;
                                        text-align: center;
                                      ">Website &amp; Digital Manager</p>
                                                                &nbsp;
    
                                                                <p class="white__text_90" pardot-region="" style="
                                        margin: 0;
                                        color: #005091;
                                        font-size: 12px;
                                        padding: 12px 0px;
                                        font-family: Arial, Helvetica, sans-serif;
                                        text-align: center;
                                        border-bottom: 1px solid #e6e6e6;
                                        border-top: 1px solid #e6e6e6;
                                      ">s.breslin@southpole.com +353 87 7763132</p>
                                                                &nbsp;
    
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" style="max-width: 159px; width: 159px; background: #ffffff; text-align: center;" width="108">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/eae-pt/image/upload/v1625748705/link_ah6mbh.png" style="display: inline-block" width="27"> </a></td>
                                                                            <td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/eae-pt/image/upload/v1625748705/linkdn_qgjyq9.png" style="display: inline-block" width="27"> </a></td>
                                                                            <td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/eae-pt/image/upload/v1625748705/instagram_vavryd.png" style="display: inline-block" width="27"> </a></td>
                                                                            <td pardot-region="" width="27"><a href="#"><img alt="" border="0" height="27" src="https://res.cloudinary.com/eae-pt/image/upload/v1625748705/link_ah6mbh.png" style="display: inline-block" width="27"> </a></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </td>
                                                    <td width="185">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacing" style="height: 40px;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END EDITOR BLOCK--><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- News -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="teaser-image-full" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff;">
                                <tbody>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 10px; font-size: 10px; height: 10px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td>
                                        <h2 class="projects-bigger-title" pardot-region="" style="
                  color: #fde9cc;
                  font-size: 107px;
                  line-height: 124px;
                  font-family: Arial, Helvetica, sans-serif;
                  margin: 0;
                ">News</h2>
    
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 173px">&nbsp;</td>
                                                    <td class="projects-horizontal-top-bar" style="background: #f39200; height: 20px; width: 427px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
    
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td><!-- CHANGE LARGE NEWS HIGHLIGHT HERE --><img alt="" src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/a4fe80ec-a3c1-5642-b13a-658b04fbfa79.png" style="display: block; width: 100%;"> <!-- END CHANGE LARGE NEWS HIGHLIGHT HERE --></td>
                                                    <td valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td class="projects-vertical-right-bar" style="background: #f39200; height: 312px; width: 80px">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
    
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 66px">&nbsp;</td>
                                                    <td class="projects-horizontal-bottom-bar" style="background: #fcedd2; height: 20px; width: 427px">&nbsp;</td>
                                                    <td style="width: 147px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
    
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 40px; height: 40px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="projects__sub-title" pardot-region="" style="
                        color: #019CDB;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 13px;
                        line-height: 18px;
                        font-weight: bold;
                      ">Carbon Credits | Climate Risks &amp; Opportunities</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 10px; height: 10px; font-size: 10px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="projects__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 34px;
                        line-height: 32px;
                        font-weight: bold;
                      ">Saving forests, protecting wildlife, and changing lives</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 10px; height: 10px; font-size: 10px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="projects__description" pardot-region="" style="
                        color: #485B7B;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 25px;
                      ">Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla. Duis metus ipsum, ullamcorper non consectetur et, euismod a leo. Mauris volutpat urna ut</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><a class="projects__link" href="http://" pardot-region="" rel="noopener noreferrer" style="
                          font-family: Arial, Helvetica, sans-serif;
                          color: #005091;
                          font-size: 18px;
                          line-height: 20px;
                          font-weight: bold;
                          text-decoration: none !important;
                        " target="_blank">Continue » </a></td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END Projects --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- TEASER IMAGE LEFT -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" pardot-repeatable="" style="max-width: 640px; width: 100%; margin: auto; background-color: #FFFFFF;" width="100%">
                                <tbody>
                                    <tr>
                                        <td class="p-mobile" style="font-size: 0px"><!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td style="width:390px; padding: 0px;" align="left" valign="top">
                  <![endif]-->
                                        <div class="deviceWidth heightAuto col-lge" style="
                      display: inline-block;
                      vertical-align: top;
                      height: 320px;
                      width: 390px;
                    "><!-- CHANGE MEDIUM TEASER LEFT HERE --><img class="hidden" height="320" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621524679/teaser-left_pzishd.png" style="
                        width: 100%;
                        max-width: 100%;
                        margin: auto;
                        display: block;
                      " valign="bottom" width="390"> <!-- END CHANGE MEDIUM TEASER LEFT HERE --> <!--[if !mso]>--> <img class="img-mobile" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126549/cow-image-mobile_zaren6.png" style="
                      display: none;
                      width: 100% !important;
                      max-width: 100%;
                      margin: auto;
                      " width="100%"> <!--<![endif]--></div>
                                        <!--[if mso]>
                    </td>
                    <td style="width:250px;" valign="top">
                      <table role="presentation" width="100%" cellpadding="0" cellspacing="0" height="320">
                        <tr>
                          <td style="background: #fdc300; padding: 25px 5px 24px 15px;">
                  <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-sml col-yellow" style="
                      background: #fdc300;
                      height: 190px;
                      padding: 20px 0;
                      display: inline-block;
                      vertical-align: top;
                      width: 250px;
                      text-align: left;
                      margin: 0 auto;
                    ">
                                        <div style="padding: 0 10px 0 15px">
                                        <h4 pardot-region="" style="
                          font-size: 21px !important;
                          color: #1e355c;
                          font-weight: bold;
                          margin: 0 0 10px;
                          line-height: 24px;
                          font-family: Arial, Helvetica, sans-serif;
                        ">Pellentesque vel enim quis enim interdum viverra</h4>
    
                                        <p pardot-region="" style="
                          font-size: 14px !important;
                          color: #1e355c;
                          margin: 0 0 20px;
                          line-height: 19px;
                          font-family: Arial, Helvetica, sans-serif;
                        ">Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla…</p>
    
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="link" role="presentation">
                                            <tbody>
                                                <tr>
                                                    <td><a href="https://www.southpole.com/" pardot-region="" style="
                                  font-size: 16px !important;
                                  font-weight: bold;
                                  color: #1e355c;
                                  text-decoration: none;
                                  font-family: Arial, Helvetica, sans-serif;
                                ">Learn more </a></td>
                                                    <td style="padding-left: 8px"><a href="#"><img alt="" heigh="8&quot;" pardot-region="" pardot-removable="" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126630/arrow-right-2_uuxdqw.png" width="5"> </a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        </div>
                                        <!--[if mso]>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  <![endif]--></td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END TEASER IMAGE LEFT --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- TEASER IMAGE RIGHT -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" pardot-repeatable="" style="max-width: 640px; width: 100%; margin: auto; background-color: #FFFFFF;" width="100%">
                                <tbody>
                                    <tr>
                                        <td class="p-mobile" style="font-size: 0px" valign="top">
                                        <div class="deviceWidth heightAuto col-lge img-mobile" style="
                      display: none;
                      vertical-align: top;
                      height: 320px;
                      width: 366px;
                    "><a href="https://www.southpole.com/"><!-- CHANGE MEDIUM TEASER RIGHT HERE --> <img src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126672/play-video-image_u1zm4v.png" style="
                          width: 100%;
                          max-width: 100%;
                          margin: auto;
                          display: block;
                        " valign="bottom"><!-- END CHANGE MEDIUM TEASER RIGHT HERE --> </a></div>
                                        <!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td style="width:250px; height: 190px" valign="top">
                          <table role="presentation" width="100%" cellpadding="0" cellspacing="0" height="320">
                            <tr>
                              <td style="background: #98c21f; padding: 25px 5px 24px 15px;" valign="bottom">
                  <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-sml col-green" style="
                      background: #98c21f;
                      height: 190px;
                      padding: 20px 0;
                      display: inline-block;
                      vertical-align: top;
                      width: 250px;
                      text-align: left;
                      margin: 0 auto;
                    ">
                                        <div style="padding: 0 10px 0 15px">
                                        <h4 pardot-region="" style="
                          font-size: 21px !important;
                          color: #1e355c;
                          font-weight: bold;
                          margin: 0 0 10px;
                          line-height: 24px;
                          font-family: Arial, Helvetica, sans-serif;
                        ">Pellentesque vel enim quis enim interdum viverra</h4>
    
                                        <p pardot-region="" style="
                          font-size: 14px !important;
                          color: #1e355c;
                          margin: 0 0 20px;
                          line-height: 19px;
                          font-family: Arial, Helvetica, sans-serif;
                        ">Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla…</p>
    
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="link" role="presentation">
                                            <tbody>
                                                <tr>
                                                    <td><a href="https://www.southpole.com/" pardot-region="" style="
                                  font-size: 16px !important;
                                  font-weight: bold;
                                  color: #1e355c;
                                  text-decoration: none;
                                  font-family: Arial, Helvetica, sans-serif;
                                ">Learn more </a></td>
                                                    <td style="padding-left: 8px"><a href="#"><img alt="" heigh="8&quot;" pardot-region="" pardot-removable="" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126630/arrow-right-2_uuxdqw.png" width="5"></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        </div>
                                        <!--[if mso]>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td style="width:390px; padding: 0px;" align="left" valign="top">
                    <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-lge hidden" style="
                      display: inline-block;
                      vertical-align: top;
                      height: 320px;
                      width: 390px;
                    "><a href="https://www.southpole.com/"><img height="320" src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126743/play-video-image-mobile_dxut0h.png" style="
                          width: 100%;
                          max-width: 100%;
                          margin: auto;
                          display: block;
                        " valign="bottom"> </a></div>
                                        <!--[if mso]>
                        </td>
                      </tr>
                    </table>
                  <![endif]--></td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- TEASER IMAGE RIGHT --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- TEASER TWO IMAGES-->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper" pardot-repeatable="" style="max-width: 640px; width: 100% !important; margin: auto; background-color: #FFFFFF;" width="640">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="p-mobile" style="font-size: 0px"><!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="width:290px; padding: 0 15px;" align="left" valign="top">
                  <![endif]-->
                                        <div class="deviceWidth heightAuto col-lge mobile-margin-bottom" style="
                      display: inline-block;
                      vertical-align: top;
                      width: 50%;
                      margin:0;
                    "><!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="width:290px; padding: 0px;" align="left" valign="top">
                  <![endif]-->
                                        <div style="
                      display: block;
                      vertical-align: top;
                      height: 160px;
                      width: 290px;
                    
                    "><img height="160" pardot-region="" src="https://res.cloudinary.com/eae-pt/image/upload/v1625571324/two_images_vdktoe.png" style="
                        width: 100%;
                        max-width: 100%;
                        margin: auto;
                        display: block;
                      " valign="bottom"></div>
                                        <!--[if mso]>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:290px; padding: 20px 0px;" align="left" valign="top" bgcolor="#fdc300">
                  <![endif]-->
    
                                        <div class="col-yellow" style="
                      background: #fdc300;
                      height: 205px;
                      padding: 20px 0;
                      display: block;
                      vertical-align: top;
                      width: 290px;
                      text-align: left;
                     
                   
                    "><!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="padding: 0 10px 0 15px">
                  <![endif]-->
                                        <div style="padding: 0 10px 0 15px">
                                        <h4 pardot-region="" style="
                          font-size: 21px !important;
                          color: #1e355c;
                          font-weight: bold;
                          margin: 0 0 10px;
                          line-height: 24px;
                          font-family: Arial, Helvetica, sans-serif;
                        ">Pellentesque vel enim quis enim interdum viverra</h4>
    
                                        <p pardot-region="" style="
                          font-size: 14px !important;
                          color: #1e355c;
                          margin: 0 0 20px;
                          line-height: 19px;
                          font-family: Arial, Helvetica, sans-serif;
                        ">Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla…</p>
    
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="link" role="presentation">
                                            <tbody>
                                                <tr>
                                                    <td><a href="https://www.southpole.com/" pardot-region="" style="
                                  font-size: 16px !important;
                                  font-weight: bold;
                                  color: #1e355c;
                                  text-decoration: none;
                                  font-family: Arial, Helvetica, sans-serif;
                                ">Learn more » </a></td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                        </td>
                    </tr>
                    </table>
                  <![endif]--></div>
                                        <!--[if mso]>
                        </td>
                    </tr>
                    </table>
                  <![endif]--></div>
                                        <!--[if mso]>
                    </td>
                    <td width="60">&nbsp;</td>
                    <td style="width:290px; padding: 0 15px;" align="left" valign="top">
                  <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-lge" style="
                      display: inline-block;
                      vertical-align: top;
                      width: 50%;
                      margin: 0;
                    "><!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="width:290px; padding: 0px;" align="left" valign="top">
                  <![endif]-->
                                        <div style="
                      display: block;
                      vertical-align: top;
                      height: 160px;
                      width: 290px;
                     
                    "><img height="160" pardot-region="" src="https://res.cloudinary.com/eae-pt/image/upload/v1625571111/two_images_2_ysxup1.png" style="
                        width: 100%;
                        max-width: 100%;
                        margin: auto;
                        display: block;
                      " valign="bottom"></div>
                                        <!--[if mso]>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:290px; padding: 20px 0px;" align="left" valign="top" bgcolor="#98C21F">
                  <![endif]-->
    
                                        <div class="col-green" style="
                      background: #98C21F;
                      height: 205px;;
                      padding: 20px 0;
                      display: block;
                      vertical-align: top;
                      width: 290px;
                      text-align: left;
                      margin: 0;
                 
                    "><!--[if mso]>
                    <table role="presentation" width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td style="padding: 0 10px 0 15px">
                  <![endif]-->
                                        <div style="padding: 0 10px 0 15px">
                                        <h4 pardot-region="" style="
                          font-size: 21px !important;
                          color: #1e355c;
                          font-weight: bold;
                          margin: 0 0 10px;
                          line-height: 24px;
                          font-family: Arial, Helvetica, sans-serif;
                        ">Pellentesque vel enim quis enim interdum viverra</h4>
    
                                        <p pardot-region="" style="
                          font-size: 14px !important;
                          color: #1e355c;
                          margin: 0 0 20px;
                          line-height: 19px;
                          font-family: Arial, Helvetica, sans-serif;
                        ">Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla…</p>
    
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="link" role="presentation">
                                            <tbody>
                                                <tr>
                                                    <td><a href="https://www.southpole.com/" pardot-region="" style="
                                  font-size: 16px !important;
                                  font-weight: bold;
                                  color: #1e355c;
                                  text-decoration: none;
                                  font-family: Arial, Helvetica, sans-serif;
                                ">Learn more » </a></td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                        </td>
                    </tr>
                    </table>
                  <![endif]--></div>
                                        <!--[if mso]>
                        </td>
                    </tr>
                    </table>
                  <![endif]--></div>
                                        <!--[if mso]>
                    </td>
                   </tr>
                   </table>
                  <![endif]--></td>
                                    </tr>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END TEASER TWO IMAGES--><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px; height:20px;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- news columns sm image text -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="news-cols-sm-img-text" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff">
                                <tbody>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="font-size: 0"><!--[if mso]>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:150px;padding:0px;" align="left" valign="top">
                <![endif]-->
                                        <div class="deviceWidth heightAuto col-sm" style="display: inline-block; vertical-align: top; width: 150px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="news-cols-sm-img-text__image">
                                            <tbody>
                                                <tr><!-- CHANGE LIST SMALL NEWS ITEM HERE -->
                                                    <td background="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg" bgcolor="#ffffff" height="130" style="background-size: cover" valign="bottom" width="150"><!-- END CHANGE LIST SMALL NEWS ITEM HERE --><!--[if gte mso 9]>
                                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                        style="width:150px;height:130px;">
                                    <v:fill type="frame"
                                            src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                            color="#ffffff"/>
                                    <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                                    <div>
                                                    <table border="0" cellpadding="0" cellspacing="0" class="img" valign="bottom" width="150px">
                                                        <tbody>
                                                            <tr>
                                                                <td class="col-lg" width="125px">&nbsp;</td>
                                                                <td class="col-sm" style="
                                width: 25px;
                                height: 110px;
                                background: rgba(255, 255, 255, 0.9);
                              " valign="bottom">&nbsp; <!--[if gte mso 9]>
                                                                <v:rect
                                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                                    fill="true"
                                                                    stroke="false"
                                                                    style="
                                    width: 25px;
                                    height: 110px;
                                    position: absolute;
                                    top: 20px;
                                    left: 125px;
                                  "
                                                                >
                                                                    <v:fill
                                                                        opacity="90%"
                                                                        color="#ffffff"
                                                                    />
                                                                </v:rect>
                                                            <![endif]--></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    <!--[if gte mso 9]>
                                </v:textbox>
                                </v:rect>
                                <![endif]--></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                <td style="width:440px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                        <tr>
                            <td>
                <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-lg" style="display: inline-block; vertical-align: top; width: 440px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="news-cols-sm-img-text__text">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 40px; height: 40px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__sub-title" pardot-region="" style="
                        color: #019CDB;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 13px;
                        line-height: 18px;
                        font-weight: bold;
                      ">Carbon Markets &amp; Climate Policy</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 21px;
                        line-height: 21px;
                        font-weight: bold;
                      ">Donec consequat purus sed nunc</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__description" pardot-region="" style="
                        color: #485B7B;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 16px;
                        line-height: 16px;
                      ">Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh.</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><a class="news-cols-sm-img-text__link" href="http://" pardot-region="" rel="noopener noreferrer" style="
                          font-family: Arial, Helvetica, sans-serif;
                          color: #005091;
                          font-size: 16px;
                          line-height: 20px;
                          font-weight: bold;
                          text-decoration: none;
                        " target="_blank">Learn more &amp; register » </a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                <![endif]--></td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="font-size: 0"><!--[if mso]>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:150px;padding:0px;" align="left" valign="top">
                <![endif]-->
                                        <div class="deviceWidth heightAuto col-sm" style="display: inline-block; vertical-align: top; width: 150px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="news-cols-sm-img-text__image">
                                            <tbody>
                                                <tr><!-- CHANGE LIST SMALL NEWS ITEM HERE -->
                                                    <td background="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg" bgcolor="#ffffff" height="130" style="background-size: cover" valign="bottom" width="150"><!-- END CHANGE LIST SMALL NEWS ITEM HERE --><!--[if gte mso 9]>
                                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                        style="width:150px;height:130px;">
                                    <v:fill type="frame"
                                            src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                            color="#ffffff"/>
                                    <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                                    <div>
                                                    <table border="0" cellpadding="0" cellspacing="0" class="img" valign="bottom" width="150px">
                                                        <tbody>
                                                            <tr>
                                                                <td class="col-lg" width="125px">&nbsp;</td>
                                                                <td class="col-sm" style="
                                width: 25px;
                                height: 110px;
                                background: rgba(255, 255, 255, 0.9);
                              " valign="bottom">&nbsp; <!--[if gte mso 9]>
                                                                <v:rect
                                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                                    fill="true"
                                                                    stroke="false"
                                                                    style="
                                    width: 25px;
                                    height: 110px;
                                    position: absolute;
                                    top: 20px;
                                    left: 125px;
                                  "
                                                                >
                                                                    <v:fill
                                                                        opacity="90%"
                                                                        color="#ffffff"
                                                                    />
                                                                </v:rect>
                                                            <![endif]--></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    <!--[if gte mso 9]>
                                </v:textbox>
                                </v:rect>
                                <![endif]--></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                <td style="width:440px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                        <tr>
                            <td>
                <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-lg" style="display: inline-block; vertical-align: top; width: 440px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="news-cols-sm-img-text__text">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 40px; height: 40px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__sub-title" pardot-region="" style="
                        color: #019CDB;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 13px;
                        line-height: 18px;
                        font-weight: bold;
                      ">Carbon Markets &amp; Climate Policy</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 21px;
                        line-height: 21px;
                        font-weight: bold;
                      ">Donec consequat purus sed nunc</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__description" pardot-region="" style="
                        color: #485B7B;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 16px;
                        line-height: 16px;
                      ">Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh.</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><a class="news-cols-sm-img-text__link" href="http://" pardot-region="" rel="noopener noreferrer" style="
                          font-family: Arial, Helvetica, sans-serif;
                          color: #005091;
                          font-size: 16px;
                          line-height: 20px;
                          font-weight: bold;
                          text-decoration: none;
                        " target="_blank">Learn more &amp; register » </a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                <![endif]--></td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="font-size: 0"><!--[if mso]>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:150px;padding:0px;" align="left" valign="top">
                <![endif]-->
                                        <div class="deviceWidth heightAuto col-sm" style="display: inline-block; vertical-align: top; width: 150px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="news-cols-sm-img-text__image">
                                            <tbody>
                                                <tr><!-- CHANGE LIST SMALL NEWS ITEM HERE -->
                                                    <td background="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg" bgcolor="#ffffff" height="130" style="background-size: cover" valign="bottom" width="150"><!-- END CHANGE LIST SMALL NEWS ITEM HERE --><!--[if gte mso 9]>
                                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                                        style="width:150px;height:130px;">
                                    <v:fill type="frame"
                                            src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/f8513400-9cc3-7578-be4f-5c0e6ebab0e8.jpg"
                                            color="#ffffff"/>
                                    <v:textbox inset="0,0,0,0">
                                <![endif]-->
                                                    <div>
                                                    <table border="0" cellpadding="0" cellspacing="0" class="img" valign="bottom" width="150px">
                                                        <tbody>
                                                            <tr>
                                                                <td class="col-lg" width="125px">&nbsp;</td>
                                                                <td class="col-sm" style="
                                width: 25px;
                                height: 110px;
                                background: rgba(255, 255, 255, 0.9);
                              " valign="bottom">&nbsp; <!--[if gte mso 9]>
                                                                <v:rect
                                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                                    fill="true"
                                                                    stroke="false"
                                                                    style="
                                    width: 25px;
                                    height: 110px;
                                    position: absolute;
                                    top: 20px;
                                    left: 125px;
                                  "
                                                                >
                                                                    <v:fill
                                                                        opacity="90%"
                                                                        color="#ffffff"
                                                                    />
                                                                </v:rect>
                                                            <![endif]--></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    <!--[if gte mso 9]>
                                </v:textbox>
                                </v:rect>
                                <![endif]--></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                <td style="width:440px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                        <tr>
                            <td>
                <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-lg" style="display: inline-block; vertical-align: top; width: 440px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="news-cols-sm-img-text__text">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 40px; height: 40px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__sub-title" pardot-region="" style="
                        color: #019CDB;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 13px;
                        line-height: 18px;
                        font-weight: bold;
                      ">Carbon Markets &amp; Climate Policy</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 21px;
                        line-height: 21px;
                        font-weight: bold;
                      ">Donec consequat purus sed nunc</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="news-cols-sm-img-text__description" pardot-region="" style="
                        color: #485B7B;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 16px;
                        line-height: 16px;
                      ">Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh.</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><a class="news-cols-sm-img-text__link" href="http://" pardot-region="" rel="noopener noreferrer" style="
                          font-family: Arial, Helvetica, sans-serif;
                          color: #005091;
                          font-size: 16px;
                          line-height: 20px;
                          font-weight: bold;
                          text-decoration: none;
                        " target="_blank">Learn more &amp; register » </a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                <![endif]--></td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END news columns sm image text --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- Projects -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="teaser-image-full" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff;">
                                <tbody>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 10px; font-size: 10px; height: 10px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td>
                                        <h2 class="projects-bigger-title" pardot-region="" style="
                  color: #fde9cc;
                  font-size: 107px;
                  line-height: 124px;
                  font-family: Arial, Helvetica, sans-serif;
                  margin: 0;
                ">Projects</h2>
    
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 173px">&nbsp;</td>
                                                    <td class="projects-horizontal-top-bar" style="background: #f39200; height: 20px; width: 427px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
    
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td><!-- CHANGE BANNER HERE --><img alt="" src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/a4fe80ec-a3c1-5642-b13a-658b04fbfa79.png" style="display: block; width: 100%;"> <!-- END CHANGE BANNER HERE --></td>
                                                    <td valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td class="projects-vertical-right-bar" style="background: #f39200; height: 312px; width: 80px">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
    
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 66px">&nbsp;</td>
                                                    <td class="projects-horizontal-bottom-bar" style="background: #fcedd2; height: 20px; width: 427px">&nbsp;</td>
                                                    <td style="width: 147px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
    
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 40px; height: 40px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="projects__sub-title" pardot-region="" style="
                        color: #019CDB;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 13px;
                        line-height: 18px;
                        font-weight: bold;
                      ">Carbon Credits | Climate Risks &amp; Opportunities</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 10px; height: 10px; font-size: 10px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="projects__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 34px;
                        line-height: 32px;
                        font-weight: bold;
                      ">Saving forests, protecting wildlife, and changing lives</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 10px; height: 10px; font-size: 10px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="projects__description" pardot-region="" style="
                        color: #485B7B;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 25px;
                      ">Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla. Duis metus ipsum, ullamcorper non consectetur et, euismod a leo. Mauris volutpat urna ut</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><a class="projects__link" href="http://" pardot-region="" rel="noopener noreferrer" style="
                          font-family: Arial, Helvetica, sans-serif;
                          color: #005091;
                          font-size: 18px;
                          line-height: 20px;
                          font-weight: bold;
                          text-decoration: none !important;
                        " target="_blank">Continue » </a></td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END Projects --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- EVENTS -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="events-cols-sm-img-text" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff">
                                <tbody>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="text-align: center">
                                        <h2 pardot-region="" style="
                  margin: 0;
                  font-size: 52px;
                  line-height: 52px;
                  color: #005091;
                  font-family: Arial, Helvetica, sans-serif;
                ">Events</h2>
                                        </td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="font-size: 0"><!--[if mso]>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:145px;padding:0px;" align="left" valign="top">
                <![endif]-->
                                        <div class="deviceWidth heightAuto col-sm" style="display: inline-block; vertical-align: top; width: 145px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="events-cols-sm-img-text__image">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#ffffff" style="background-size: cover" valign="bottom" width="145">
                                                    <div>
                                                    <table border="0" cellpadding="0" cellspacing="0" class="img" style="
                            background-color: #f39200;
                            height: 105px;
                            width: 145px;
                          " valign="bottom" width="145">
                                                        <tbody>
                                                            <tr>
                                                                <td class="col-lg" style="
                                width: 95px;
                                height: 90px;
                                padding-left: 10px;
                                padding-bottom: 15px;
                                color: #ffffff;
                                font-family: Arial, Helvetica, sans-serif;
                                font-size: 12px;
                              " valign="bottom" width="110px">
                                                                <table border="0" cellpadding="0" cellspacing="0" valign="bottom">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td pardot-region="" style="
                                      color: #ffffff;
                                      font-family: Arial, Helvetica, sans-serif;
                                      font-size: 12px;
                                      font-weight: bold;
                                    ">
                                                                            <p style="
                                        font-size: 45px;
                                        line-height: 26px;
                                        margin-bottom: 8px;
                                        margin-top: 0;
                                        color: #ffffff;
                                        font-weight: bold;
                                      ">12</p>
                                                                            May</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </td>
                                                                <td align="right" valign="bottom">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="col-sm" style="
                                      width: 40px;
                                      height: 85px;
                                      background: rgba(255, 255, 255, 0.9);
                                    " valign="bottom">&nbsp; <!--[if gte mso 9]>
                                                                                <v:rect
                                                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                                                    fill="true"
                                                                                    stroke="false"
                                                                                    style="
                                    width: 40px;
                                    height: 85px;
                                    position: absolute;
                                    top: 20px;
                                    left: 0;
                                  "
                                                                                >
                                                                                    <v:fill
                                                                                        opacity="90%"
                                                                                        color="#ffffff"
                                                                                    />
                                                                                </v:rect>
                                                                            <![endif]--></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                <td style="width:440px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                        <tr>
                            <td>
                <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-lg" style="display: inline-block; vertical-align: top; width: 440px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="events-cols-sm-img-text__text" width="100%">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 40px; height: 40px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="events__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 21px;
                        line-height: 24px;
                        font-weight: bold;
                      ">Sustainable Finance: Impact and Implications for the Real Economy</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="events__sub-title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                      ">Wed, 12 May — Online: 18:00 - 20:00 CET</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><a class="events__link" href="http://" pardot-region="" rel="noopener noreferrer" style="
                          font-family: Arial, Helvetica, sans-serif;
                          color: #005091;
                          font-size: 16px;
                          line-height: 20px;
                          font-weight: bold;
                          text-decoration: none;
                        " target="_blank">Learn more &amp; register » </a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                <![endif]--></td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="font-size: 0"><!--[if mso]>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:145px;padding:0px;" align="left" valign="top">
                <![endif]-->
                                        <div class="deviceWidth heightAuto col-sm" style="display: inline-block; vertical-align: top; width: 145px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="events-cols-sm-img-text__image">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#ffffff" style="background-size: cover" valign="bottom" width="145">
                                                    <div>
                                                    <table border="0" cellpadding="0" cellspacing="0" class="img" style="
                            background-color: #589cd7;
                            height: 105px;
                            width: 145px;
                          " valign="bottom" width="145px">
                                                        <tbody>
                                                            <tr>
                                                                <td class="col-lg" style="
                                width: 95px;
                                height: 90px;
                                padding-left: 10px;
                                padding-bottom: 15px;
                                color: #ffffff;
                                font-family: Arial, Helvetica, sans-serif;
                                font-size: 12px;
                              " valign="bottom" width="110px">
                                                                <table border="0" cellpadding="0" cellspacing="0" valign="bottom">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td pardot-region="" style="
                                      color: #ffffff;
                                      font-family: Arial, Helvetica, sans-serif;
                                      font-size: 12px;
                                      font-weight: bold;
                                    ">
                                                                            <p style="
                                        font-size: 45px;
                                        line-height: 26px;
                                        margin-bottom: 8px;
                                        margin-top: 0;
                                        color: #ffffff;
                                        font-weight: bold;
                                      ">12</p>
                                                                            May</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </td>
                                                                <td align="right" valign="bottom">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="col-sm" style="
                                      width: 40px;
                                      height: 85px;
                                      background: rgba(255, 255, 255, 0.9);
                                    " valign="bottom">&nbsp; <!--[if gte mso 9]>
                                                                                <v:rect
                                                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                                                    fill="true"
                                                                                    stroke="false"
                                                                                    style="
                                    width: 40px;
                                    height: 85px;
                                    position: absolute;
                                    top: 20px;
                                    left: 0;
                                  "
                                                                                >
                                                                                    <v:fill
                                                                                        opacity="90%"
                                                                                        color="#ffffff"
                                                                                    />
                                                                                </v:rect>
                                                                            <![endif]--></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                <td style="width:440px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                        <tr>
                            <td>
                <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-lg" style="display: inline-block; vertical-align: top; width: 440px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="events-cols-sm-img-text__text" width="100%">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 40px; height: 40px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="events__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 21px;
                        line-height: 24px;
                        font-weight: bold;
                      ">Sustainable Finance: Impact and Implications for the Real Economy</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="events__sub-title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                      ">Wed, 12 May — Online: 18:00 - 20:00 CET</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><a class="events__link" href="http://" pardot-region="" rel="noopener noreferrer" style="
                          font-family: Arial, Helvetica, sans-serif;
                          color: #005091;
                          font-size: 16px;
                          line-height: 20px;
                          font-weight: bold;
                          text-decoration: none;
                        " target="_blank">Learn more &amp; register » </a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                <![endif]--></td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td align="right" style="line-height: 40px; height: 40px; font-size: 40px"><a class="events__link" href="https://www.southpole.com/" pardot-region="" rel="noopener noreferrer" style="
                          font-family: Arial, Helvetica, sans-serif;
                          color: #005091;
                          font-size: 16px;
                          line-height: 20px;
                          font-weight: bold;
                          text-decoration: none;
                          text-align: right;
                        " target="_blank">View all events » </a></td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END EVENTS --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space --><!-- LINK -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="" style="max-width: 640px; width: 100%; background: #ffffff">
                                <tbody>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="text-align: center">
                                        <h2 pardot-region="" style="
                  margin: 0;
                  font-size: 52px;
                  line-height: 52px;
                  color: #005091;
                  font-family: Arial, Helvetica, sans-serif;
                ">In the News</h2>
                                        </td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="font-size: 0"><!--[if mso]>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:55px;padding:0px;" align="left" valign="top">
                <![endif]-->
                                        <div class="heightAuto col-sm" style="display: inline-block; vertical-align: top; width: 55px">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#ffffff" style="background-size: cover" valign="bottom" width="55">
                                                    <div>
                                                    <table border="0" cellpadding="0" cellspacing="0" class="img" height="55" style="
                            background-color: #019CDB;
                            height: 55px;
                            width: 55px;
                          " valign="bottom" width="55">
                                                        <tbody>
                                                            <tr>
                                                                <td class="col-box" height="55" style="
                                width: 27px;
                                height: 55px;
                                color: #ffffff;
                                font-family: Arial, Helvetica, sans-serif;
                                font-size: 12px;
                              " valign="bottom" width="27">&nbsp;</td>
                                                                <td align="right" class="col-wrapper" height="55" valign="bottom" width="28">
                                                                <table cellpadding="0" cellspacing="0" height="45" valign="bottom">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="col-overlay" height="45" style="
                                      background: rgba(255, 255, 255, 0.9);
                                    " valign="bottom" width="28">&nbsp; <!--[if gte mso 9]>
                                                                                <v:rect
                                                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                                                    fill="true"
                                                                                    stroke="false"
                                                                                    style="
                                    width: 28px;
                                    height: 45px;
                                    position: absolute;
                                    left: 0;
                                  "
                                                                                >
                                                                                    <v:fill
                                                                                        opacity="90%"
                                                                                        color="#ffffff"
                                                                                    />
                                                                                </v:rect>
                                                                            <![endif]--></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                <td style="width:440px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                        <tr>
                            <td>
                <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-text" style="display: inline-block; vertical-align: top; width: 440px">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 25px; height: 25px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="events__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 24px;
                        font-weight: bold;
                      ">Climate Leaders Forum - COP26 Limoncello &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="events__sub-title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                      ">Wed, 14 Oct — Online: 18:00 - 20:00 CET</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                <![endif]--></td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr pardot-repeatable="">
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="font-size: 0"><!--[if mso]>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:55px;padding:0px;" align="left" valign="top">
                <![endif]-->
                                        <div class="heightAuto col-sm" style="display: inline-block; vertical-align: top; width: 55px">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#ffffff" style="background-size: cover" valign="bottom" width="55">
                                                    <div>
                                                    <table border="0" cellpadding="0" cellspacing="0" class="img" height="55" style="
                            background-color: #019CDB;
                            height: 55px;
                            width: 55px;
                          " valign="bottom" width="55">
                                                        <tbody>
                                                            <tr>
                                                                <td class="col-box" height="55" style="
                                width: 27px;
                                height: 55px;
                                color: #ffffff;
                                font-family: Arial, Helvetica, sans-serif;
                                font-size: 12px;
                              " valign="bottom" width="27">&nbsp;</td>
                                                                <td align="right" class="col-wrapper" height="55" valign="bottom" width="28">
                                                                <table cellpadding="0" cellspacing="0" height="45" valign="bottom">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="col-overlay" height="45" style="
                                      background: rgba(255, 255, 255, 0.9);
                                    " valign="bottom" width="28">&nbsp; <!--[if gte mso 9]>
                                                                                <v:rect
                                                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                                                    fill="true"
                                                                                    stroke="false"
                                                                                    style="
                                    width: 28px;
                                    height: 45px;
                                    position: absolute;
                                    left: 0;
                                  "
                                                                                >
                                                                                    <v:fill
                                                                                        opacity="90%"
                                                                                        color="#ffffff"
                                                                                    />
                                                                                </v:rect>
                                                                            <![endif]--></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                <td style="width:440px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                        <tr>
                            <td>
                <![endif]-->
    
                                        <div class="deviceWidth heightAuto col-text" style="display: inline-block; vertical-align: top; width: 440px">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr class="space">
                                                    <td style="line-height: 25px; height: 25px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="events__title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 24px;
                        font-weight: bold;
                      ">Sustainable Finance: Impact and Implications for the Real Economy</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 5px; height: 5px; font-size: 5px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="events__sub-title" pardot-region="" style="
                        color: #005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                      ">Wed, 12 May — Online: 18:00 - 20:00 CET</td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <!--[if mso]>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                <![endif]--></td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px">&nbsp;</td>
                                        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
                                        <td style="width: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END EVENTS --><!-- space -->
    
                            <table align="center" border="0" cellpadding="0" cellspacing="0" pardot-repeatable="">
                                <tbody>
                                    <tr>
                                        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- END space -->
    
    
    <!-- EN --><!-- FOOTER -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table"
        pardot-removable=""
        style="max-width: 640px; width: 100%; background: #005091"
    >
        <tbody>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100%; background: #005091;"
                    >
                        <tbody>
                            <tr>
                                <td align="center">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding:0px;" align="left" valign="top" width="340">
                            <![endif]-->
                                    <div
                                        class="footer__logo"
                                        style="display: inline-block; vertical-align: top; width: 340px; margin-left: 40px; text-align: left;"
                                    >
                                        <img
                                            alt=""
                                            border="0"
                                            height="31"
                                            pardot-region=""
                                            src="https://go.southpole.com/l/881182/2021-12-12/2cygx/881182/16393707474aOISiAI/southpole_logo_white.png"
                                            style="display: block"
                                            width="150"
                                        />
                                    </div>
                                    <!--[if mso]>
                            </td>
                            <td style="padding:0px;" align="left" valign="top" width="240">
                            <![endif]-->
    
                                    <div
                                        class="footer__socials"
                                        style="display: inline-block; vertical-align: top; width: 240px; margin-top: 6px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__socials__logos"
                                            style="width: 100%; background: #005091;"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td
                                                        pardot-region=""
                                                        style=" width: 90px; color:#FFFFFF;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 18px;"
                                                        width="90"
                                                    >
                                                        Get in touch
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 25px;"
                                                        width="25"
                                                    >
                                                        <a
                                                            href="https://www.facebook.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cygv/881182/1639370396AwTQjfVV/facebook_white.png"
                                                                style="display: block"
                                                                width="8"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.linkedin.com/company/south-pole-global/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfl/881182/16393695850pdaBzGZ/linkedin_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://www.instagram.com/southpoleglobal/"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfg/881182/16393692748uSk1Ls0/insta_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td
                                                        align="center"
                                                        style="width: 30px;"
                                                        width="30"
                                                    >
                                                        <a
                                                            href="https://twitter.com/southpoleglobal"
                                                            ><img
                                                                alt=""
                                                                border="0"
                                                                height="18"
                                                                src="https://go.southpole.com/l/881182/2021-12-12/2cyfd/881182/1639369166PzkDmmdU/twitter_white.png"
                                                                style="display: block"
                                                                width="18"
                                                        /></a>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- FOOTER BOTTOM -->
    
    <table
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        class="footer-table-bottom"
        style="max-width: 640px; width: 100%; background: #1E355C"
    >
        <tbody>
            <tr>
                <td style="line-height: 30px; height: 30px">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table
                        align="center"
                        border="0"
                        cellpadding="0"
                        cellspacing="0"
                        style="width: 100% !important;"
                    >
                        <tbody>
                            <tr>
                                <td align="left">
                                    <!--[if mso]>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px 39px 0px;" align="left" valign="top" width="45%">
                            <![endif]-->
                                    <div
                                        class="footer__moto"
                                        style="display: inline-block; vertical-align: top; width: 240px; padding-left: 49px;"
                                    >
                                        <h2
                                            pardot-region=""
                                            style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 18px;
                        line-height: 21px;
                        font-weight: bold;
                        color: #019CDB;
                        margin: 0;"
                                        >
                                            {{{dynamic_content_476}}}
                                        </h2>
                                    </div>
                                    <!--[if mso]>
                            </td>
                          
                            <td style="padding:0px 85px 0px;" align="left" valign="top" width="55%">
                            <![endif]-->
    
                                    <div
                                        class="footer__address"
                                        style="display: inline-block; vertical-align: top; width: 140px; margin-top: 6px; padding-left: 100px;"
                                    >
                                        <table
                                            align="center"
                                            border="0"
                                            cellpadding="0"
                                            cellspacing="0"
                                            class="footer__address-table"
                                            style="width: 100%;"
                                        >
                                            <tbody>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px;
                        font-weight: bold;
                        color: #FFFFFF;"
                                                    >
                                                        South Pole Global
                                                        Headquarters
                                                    </td>
                                                </tr>
                                                <tr style="">
                                                    <td
                                                        pardot-region=""
                                                        style="font-family: Arial, Helvetica, sans-serif;
                        font-size: 14px;
                        line-height: 18px; color: #FFFFFF; padding-top: 10px;"
                                                    >
                                                        Technoparkstrasse 1 Zurich
                                                        8005 Switzerland
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="line-height: 35px; height: 35px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- END FOOTER-->
    
    <!-- DISCLAIMER -->
    
    <table
        class="bg-table"
        align="center"
        border="0"
        cellpadding="0"
        cellspacing="0"
        style="max-width: 510px; width: 100%;"
    >
        <tbody>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
            <tr style="">
                <td
                    align="center"
                    class="footer__disclaimer"
                    pardot-region=""
                    style="text-align: center; color:#005091;
                        font-family: Arial, Helvetica, sans-serif;
                        font-size: 12px;
                        line-height: 14px;"
                >
                    You are receiving this email because this email address is
                    signed up to receive the South Pole communications. You can
                    <a
                        href="{{EmailPreferenceCenter_114}}"
                        style="
                                        text-decoration: underline;
                                        color: #005091;
                                        font-family: Arial, Helvetica,
                                          sans-serif;
                                      "
                        >update your preferences here</a
                    >.
                </td>
            </tr>
            <tr class="space bg-table">
                <td style="line-height: 20px; height: 20px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <!-- DISCLAIMER -->
    `

    return templateFullBlockSnapshot;
}