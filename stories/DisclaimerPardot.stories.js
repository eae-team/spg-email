import { createDisclaimerPardot } from './DisclaimerPardot';

export default {
  title: 'Pardot Tags/Disclaimer',
};

const Template = () => createDisclaimerPardot();

export const DisclaimerComponent = Template.bind({});

