import './main.css';

export const createListBulletsIconPardot = () => {
    const listBullets = `
<!-- BULLETS ICON -->
<table
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 640px; width: 100%; background: #ffffff"
        pardot-repeatable=""
        class="events-cols-sm-img-text"
>
    <tr pardot-repeatable="">
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
   
    <tr pardot-repeatable="">
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:145px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 145px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__image">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="145"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="145"
                                        class="img"
                                        style="
                        background-color: #98C21F;
                        height: 105px;
                        width: 145px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="110px"
                                                class="col-lg"
                                                style="
                            width: 95px;
                            height: 90px;
                            padding-left: 10px;
                            padding-bottom: 15px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                                align="left"
                                        >
                                            <table
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    border="0"
                                                    valign="bottom"
                                                   
                                            >
                                                <tr>
                                                    <td>
                                                       
                                                        <img
                                                            pardot-region=""
                                                            src="https://res.cloudinary.com/eae-pt/image/upload/v1626359527/icon_bullets_xnbl1t.png"
                                                            width="66"
                                                            height="49"
                                                            alt=""
                                                            border="0"
                                                            style="display: inline-block"
                                                            class=""
                                                            />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" valign="bottom">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td
                                                            style="
                                  width: 40px;
                                  height: 85px;
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-sm"
                                                    >
                                                        &nbsp;
                                                       <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 40px;
                                height: 85px;
                                position: absolute;
                                top: 20px;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 16px;
                    line-height: 22px;
                  "
                  class="bullets__title"
                        >
                          <h2 pardot-region="" class="white__text" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 21px;
                            font-weight: bold;
                            line-height: 25px;
                            margin: 0;
                            padding: 0;
                          ">
                            5 strategic ways to approach CDP
                           </h2>
                           <p pardot-region="" class="white__text" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 18px;
                            line-height: 24px;
                            margin: 0;
                            padding: 0;
                          ">Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh. </p>
                           <a pardot-region="" href="#" class="light_blue_link" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 18px;
                            font-weight: bold;
                            line-height: 24px;
                            margin: 0;
                            padding: 0;
                            text-decoration: none;
                          ">Read Here ></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                 
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr pardot-repeatable="">
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr pardot-repeatable="">
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:145px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 145px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__image">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="145"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="145"
                                        class="img"
                                        style="
                        background-color: #F39200;
                        height: 105px;
                        width: 145px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="110px"
                                                class="col-lg"
                                                style="
                            width: 95px;
                            height: 90px;
                            padding-left: 10px;
                            padding-bottom: 15px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                                align="left"
                                        >
                                            <table
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    border="0"
                                                    valign="bottom"
                                                   
                                            >
                                                <tr>
                                                    <td>
                                                       
                                                        <img
                                                            pardot-region=""
                                                            src="https://res.cloudinary.com/eae-pt/image/upload/v1626359527/icon_lamp_ybbe61.png"
                                                            width="39"
                                                            height="66"
                                                            alt=""
                                                            border="0"
                                                            style="display: inline-block"
                                                            class=""
                                                            />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" valign="bottom">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td
                                                            style="
                                  width: 40px;
                                  height: 85px;
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-sm"
                                                    >
                                                        &nbsp;
                                                       <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 40px;
                                height: 85px;
                                position: absolute;
                                top: 20px;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td                  class="bullets__title"
                        >
                           <h2 pardot-region="" class="white__text" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 21px;
                            font-weight: bold;
                            line-height: 25px;
                            margin: 0;
                            padding: 0;
                          ">
                            5 strategic ways to approach CDP
                           </h2>
                           <p pardot-region="" class="white__text" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 18px;
                            line-height: 24px;
                            margin: 0;
                            padding: 0;
                          ">Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh. </p>
                           <a pardot-region="" href="#" class="light_blue_link" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 18px;
                            font-weight: bold;
                            line-height: 24px;
                            margin: 0;
                            padding: 0;
                            text-decoration: none;
                          ">Read Here ></a>
                           
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                 
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>

    <tr pardot-repeatable="">
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    
    <tr pardot-repeatable="">
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:145px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 145px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__image">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="145"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="145"
                                        class="img"
                                        style="
                        background-color: #019CDB;
                        height: 105px;
                        width: 145px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="110px"
                                                class="col-lg"
                                                style="
                            width: 95px;
                            height: 90px;
                            padding-left: 10px;
                            padding-bottom: 15px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                                align="left"
                                        >
                                            <table
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    border="0"
                                                    valign="bottom"
                                                   
                                            >
                                                <tr>
                                                    <td>
                                                       
                                                        <img
                                                            pardot-region=""
                                                            src="https://res.cloudinary.com/eae-pt/image/upload/v1626359527/icon_windmill_jlodop.png"
                                                            width="44"
                                                            height="66"
                                                            alt=""
                                                            border="0"
                                                            style="display: inline-block"
                                                            class=""
                                                            />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" valign="bottom">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td
                                                            style="
                                  width: 40px;
                                  height: 85px;
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-sm"
                                                    >
                                                        &nbsp;
                                                       <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 40px;
                                height: 85px;
                                position: absolute;
                                top: 20px;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                      
                  class="bullets__title"
                        >
                           <h2 pardot-region="" class="white__text" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 21px;
                            font-weight: bold;
                            line-height: 25px;
                            margin: 0;
                            padding: 0;
                          ">
                            5 strategic ways to approach CDP
                           </h2>
                           <p pardot-region="" class="white__text" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 18px;
                            line-height: 24px;
                            margin: 0;
                            padding: 0;
                          ">Mauris volutpat urna ut feugiat tempor. Praesent elit sapien, egestas vulputate orci eu, egestas laoreet nibh. </p>
                           <a pardot-region="" href="#" class="light_blue_link" style="
                            color: #005091;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 18px;
                            font-weight: bold;
                            line-height: 24px;
                            margin: 0;
                            padding: 0;
                            text-decoration: none;
                          ">Read Here ></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                 
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END BULLETS ICON -->
<!-- space -->
<table cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
  `;

    return listBullets;
};
