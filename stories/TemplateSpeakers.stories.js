import { createTemplateSpeakers } from './TemplateSpeakers';

export default {
  title: 'Pardot Templates/Template Speakers',
};

const Template = () => createTemplateSpeakers();

export const templateSpeakersComponent = Template.bind({});

