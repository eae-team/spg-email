import {createTextWithButton} from './TextWithButton';

export default {
  title: 'Blocks/Text/Text With Button',
};

const Template = () => createTextWithButton();

export const TextWithButtonComponent = Template.bind({});
