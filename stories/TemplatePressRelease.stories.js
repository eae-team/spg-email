import { createTemplatePressRelease } from './TemplatePressRelease';

export default {
  title: 'Pardot Templates/Template Press Release',
};

const Template = () => createTemplatePressRelease();

export const templatePressReleaseComponent = Template.bind({});

