import { createDisclaimer } from './Disclaimer';

export default {
  title: 'Blocks/Disclaimer',
};

const Template = () => createDisclaimer();

export const DisclaimerComponent = Template.bind({});

