import './main.css';

export const createHeaderLogoPardot = () => {
    const header = `

<!-- HEADER LOGO -->
<table
        cellpadding="0"
        cellspacing="0"
        border="0"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="header"
        pardot-removable=""
        align="center"
>
    <tr>
        <td style="padding: 18px">
            <table align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <a href="https://www.southpole.com/" target="_blank">
                            <img
                                    src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621242940/logo_wbwxyx.png"
                                    width="197"
                                    height="40"
                                    pardot-region=""
                                    alt=""
                                    border="0"
                                    style="display: block;width: 100%;height: 100%;"
                                    class="logo"
                            />
                            <!--[if !mso]>-->
                            <img
                                    src="https://mcusercontent.com/7eab1e486a04431e455889a9a/images/ecd38bf1-c2d9-adfb-d953-2f6b697bd71e.png"
                                    width="197"
                                    height="40"
                                    pardot-region=""
                                    alt=""
                                    border="0"
                                    style="display: none;width: 100%;height: 100%;"
                                    class="logo-white"
                            />
                            <!--<![endif]-->
                        </a>
                    </td>
                  
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END HEADER LOGO -->
`;

    return header;
};
