import { createPageWebinar } from './PageWebinar';

export default {
  title: 'Pages/Webinar',
};

const Template = () => createPageWebinar();

export const PageComponent = Template.bind({});

