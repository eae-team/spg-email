import { createFooterPardotBG } from './FooterPardotBG';

export default {
  title: 'Pardot Tags/Footer/Footer EN (Belgium)',
};

const Template = () => createFooterPardotBG();

export const FooterComponent = Template.bind({});
