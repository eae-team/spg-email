import './main.css';

export const createListEvent = () => {
    const listEvent = `
<!-- EVENTS -->
<table
        cellpadding="0"
        cellspacing="0"
        border="0"
        align="center"
        style="max-width: 640px; width: 100%; background: #ffffff"
        class="events-cols-sm-img-text"
>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="text-align: center">
            <h2
                    style="
              margin: 0;
              font-size: 52px;
              line-height: 52px;
              color: #005091;
              font-family: Arial, Helvetica, sans-serif;
            "
            >
                Events
            </h2>
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:145px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 145px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__image">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="145"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="145"
                                        class="img"
                                        style="
                        background-color: #f39200;
                        height: 105px;
                        width: 145px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="110px"
                                                class="col-lg"
                                                style="
                            width: 95px;
                            height: 90px;
                            padding-left: 10px;
                            padding-bottom: 15px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                        >
                                            <table
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    border="0"
                                                    valign="bottom"
                                            >
                                                <tr>
                                                    <td
                                                            style="
                                  color: #ffffff;
                                  font-family: Arial, Helvetica, sans-serif;
                                  font-size: 12px;
                                  font-weight: bold;
                                "
                                                    >
                                                        <p
                                                                style="
                                    font-size: 45px;
                                    line-height: 26px;
                                    margin-bottom: 8px;
                                    margin-top: 0;
                                    color: #ffffff;
                                    font-weight: bold;
                                  "
                                                        >
                                                            12
                                                        </p>
                                                        May
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" valign="bottom">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td
                                                            style="
                                  width: 40px;
                                  height: 85px;
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-sm"
                                                    >
                                                        &nbsp;
                                                       <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 40px;
                                height: 85px;
                                position: absolute;
                                top: 20px;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 21px;
                    line-height: 24px;
                    font-weight: bold;
                  "
                  class="events__title"
                        >
                            Sustainable Finance: Impact and Implications for the Real
                            Economy
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    line-height: 18px;
                  "
                  class="events__sub-title"
                        >
                            Wed, 12 May — Online: 18:00 - 20:00 CET
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a
                                    href="http://"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    style="
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      font-size: 16px;
                      line-height: 20px;
                      font-weight: bold;
                      text-decoration: none;
                    "
                    class="events__link"
                            >
                                Learn more &amp; register >
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 20px; height: 20px; font-size: 20px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="font-size: 0">
            <!--[if mso]>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:145px;padding:0px;" align="left" valign="top">
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 145px"
                    class="deviceWidth heightAuto col-sm"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__image">
                    <tr>
                        <td
                                bgcolor="#ffffff"
                                width="145"
                                valign="bottom"
                                style="background-size: cover"
                        >
                            <div>
                                <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        border="0"
                                        valign="bottom"
                                        width="145px"
                                        class="img"
                                        style="
                        background-color: #589cd7;
                        height: 105px;
                        width: 145px;
                      "
                                >
                                    <tr>
                                        <td
                                                width="110px"
                                                class="col-lg"
                                                style="
                            width: 95px;
                            height: 90px;
                            padding-left: 10px;
                            padding-bottom: 15px;
                            color: #ffffff;
                            font-family: Arial, Helvetica, sans-serif;
                            font-size: 12px;
                          "
                                                valign="bottom"
                                        >
                                            <table
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    border="0"
                                                    valign="bottom"
                                            >
                                                <tr>
                                                    <td
                                                            style="
                                  color: #ffffff;
                                  font-family: Arial, Helvetica, sans-serif;
                                  font-size: 12px;
                                  font-weight: bold;
                                "
                                                    >
                                                        <p
                                                                style="
                                    font-size: 45px;
                                    line-height: 26px;
                                    margin-bottom: 8px;
                                    margin-top: 0;
                                    color: #ffffff;
                                    font-weight: bold;
                                  "
                                                        >
                                                            12
                                                        </p>
                                                        May
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" valign="bottom">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td
                                                            style="
                                  width: 40px;
                                  height: 85px;
                                  background: rgba(255, 255, 255, 0.9);
                                "
                                                            valign="bottom"
                                                            class="col-sm"
                                                    >
                                                        &nbsp;
                                                     
                                            <!--[if gte mso 9]>
                                            <v:rect
                                                    xmlns:v="urn:schemas-microsoft-com:vml"
                                                    fill="true"
                                                    stroke="false"
                                                    style="
                                width: 40px;
                                height: 85px;
                                position: absolute;
                                top: 20px;
                                left: 0;
                              "
                                            >
                                                <v:fill opacity="90%" color="#ffffff"/>
                                            </v:rect>
                                            <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            <td style="width:440px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:320px;">
                    <tr>
                        <td>
            <![endif]-->
            <div
                    style="display: inline-block; vertical-align: top; width: 440px"
                    class="deviceWidth heightAuto col-lg"
            >
                <table cellpadding="0" cellspacing="0" border="0" class="events-cols-sm-img-text__text" width="100%">
                    <tr class="space bg-table">
                        <td style="line-height: 40px; height: 40px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 21px;
                    line-height: 24px;
                    font-weight: bold;
                  "
                   class="events__title"
                        >
                            Sustainable Finance: Impact and Implications for the Real
                            Economy
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 5px; height: 5px; font-size: 5px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td
                                style="
                    color: #005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 14px;
                    line-height: 18px;
                  "
                   class="events__sub-title"
                        >
                            Wed, 12 May — Online: 18:00 - 20:00 CET
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 20px; height: 20px; font-size: 20px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a
                                    href="http://"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    style="
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      font-size: 16px;
                      line-height: 20px;
                      font-weight: bold;
                      text-decoration: none;
                    "
                    class="events__link"
                            >
                                Learn more &amp; register >
                            </a>
                        </td>
                    </tr>

                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
        <td style="width: 20px">&nbsp;</td>
    </tr>


    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td align="right" style="line-height: 40px; height: 40px; font-size: 40px"><a
                href="https://www.southpole.com/"
                target="_blank"
                rel="noopener noreferrer"
                style="
                      font-family: Arial, Helvetica, sans-serif;
                      color: #005091;
                      font-size: 16px;
                      line-height: 20px;
                      font-weight: bold;
                      text-decoration: none;
                      text-align: right;
                    "
                    class="events__link"
        >
            View all events >
        </a></td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 20px">&nbsp;</td>
        <td style="line-height: 40px; height: 40px; font-size: 40px">&nbsp;</td>
        <td style="width: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END EVENTS -->
<!-- space -->
<table cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->
  `;

    return listEvent;
};
