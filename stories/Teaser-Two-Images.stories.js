import { createTeaserTwoImages } from './Teaser-Two-Images';

export default {
  title: 'Blocks/Teaser',
};

const Template = () => createTeaserTwoImages();

export const TeaserTwoImagesComponent = Template.bind({});

