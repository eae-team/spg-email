import { createFooterPardotES } from './FooterPardotES';

export default {
  title: 'Pardot Tags/Footer/Footer ES',
};

const Template = () => createFooterPardotES();

export const FooterComponent = Template.bind({});
