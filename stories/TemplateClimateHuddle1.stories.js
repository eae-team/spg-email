import { createTemplateClimateHuddle1 } from './TemplateClimateHuddle1';

export default {
  title: 'Pardot Templates/Template Climate Huddle1',
};

const Template = () => createTemplateClimateHuddle1();

export const templateClimateHuddle1Component = Template.bind({});

