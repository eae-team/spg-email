import {createFullCTA, createFullCTALightBlue} from './FullCTALightBlue';

export default {
  title: 'Blocks/Full CTA/Light Blue',
};

const Template = () => createFullCTALightBlue();

export const FullCTAComponent = Template.bind({});
