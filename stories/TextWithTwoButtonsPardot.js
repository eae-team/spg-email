import './main.css';

export const createTextWithTwoButtonsPardot = () => {
  const textWithTwoButtons = `
  <table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       pardot-repeatable=""
       class="header"
       align="center">
    <tr>
        <td style="height: 50px" class="spacing">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    width="100%"
                    style="
          text-align: center;
          width: 100%;
          margin: auto;
        "
                    class="intro"
            >
                <tbody>
                <tr>
                    <td class="intro-text" style="padding: 0px 40px">
                        <table
                                role="presentation"
                                border="0"
                                cellpadding="0"
                                cellspacing="0"
                                id="snapshot"
                        >
                            <tr>
                                <td pardot-region="">
                                    <h2
                                            style="
                        color: #005091;
                        font-size: 21px;
                        line-height: 27px;
                        font-weight: bold;
                        margin: 0;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                                    >
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel enim
                                        quis enim interdum viverra ut ut augue. Donec consequat purus sed nunc.
                                    </h2>

                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 30px">
                                    <p
                                            style="
                        margin: 0;
                        color: #485B7B;
                        font-size: 18px;
                        line-height: 24px;
                        font-family: Arial, Helvetica, sans-serif;
                        text-align: center;
                      "
                                            class="blue"
                                    >
                                        Ut sagittis enim ut risus ultrices, ac consequat quam volutpat. Morbi rutrum
                                        euismod ipsum ut ornare. Sed vestibulum hendrerit elit, non placerat nisl
                                        consequat ac. Nunc eget tempus sem. Duis viverra eu felis suscipit scelerisque.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    class=""
            >
                <tbody>
                <tr>
                    <td class="button-mobile">
                        <div style="margin-top: 30px;">
                            <!--[if mso]>
                            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                         xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
                                         style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" stroke="f"
                                         fillcolor="#019CDB">
                                <w:anchorlock/>
                                <center>
                            <![endif]-->
                            <a pardot-region="" pardot-removable="" href="http://"
                               style="background-color:#019CDB;border-radius:25px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:175px;-webkit-text-size-adjust:none;">Discover
                                the ways</a>
                            <!--[if mso]>
                            </center>
                            </v:roundrect>
                            <![endif]--></div>
                           
                    </td>
                    <td class="button-mobile">
                                <div class="button-spacing" style="margin-top: 30px; margin-left: 30px;"><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.google.com/" style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" strokecolor="#005091" fillcolor="#ffffff">
    <w:anchorlock/>
    <center style="color:#005091;font-family:sans-serif;font-size:13px;font-weight:bold;">Contact us Here</center>
  </v:roundrect>
<![endif]--><a pardot-region="" pardot-removable="" href="http://"
style="background-color:#ffffff;border:1px solid #005091;border-radius:25px;color:#005091;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:175px;-webkit-text-size-adjust:none;mso-hide:all;">Contacts us Here</a></div>
                            </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 50px" class="spacing">&nbsp;</td>
    </tr>
</table>
<!-- END HERO -->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table> 
<!-- END space -->`;

  return textWithTwoButtons;
}