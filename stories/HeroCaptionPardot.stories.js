import {createHeroCaptionPardot} from './HeroCaptionPardot';

export default {
  title: 'Pardot Tags/Hero/Caption',
};

const Template = () => createHeroCaptionPardot();

export const HeroComponent = Template.bind({});

