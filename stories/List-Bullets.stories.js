import { createListBullets } from './List-Bullets';

export default {
  title: 'Blocks/Bullets/Number',
};

const Template = () => createListBullets();

export const ListBulletsComponent = Template.bind({});
