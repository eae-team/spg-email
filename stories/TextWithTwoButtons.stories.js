import {createTextWithTwoButtons} from './TextWithTwoButtons';

export default {
  title: 'Blocks/Text/Text With Two Buttons',
};

const Template = () => createTextWithTwoButtons();

export const TextWithTwoButtonsComponent = Template.bind({});
