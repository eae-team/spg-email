import './main.css';

export const createDisclaimerPardotFR = () => {
    const disclaimer = `
    <!-- DISCLAIMER -->

                        <table align="center" class="bg-table" border="0" cellpadding="0" cellspacing="0" pardot-removable="" style="max-width: 510px; width: 100%;">
                            <tbody>
                                <tr class="space bg-table">
                                    <td style="line-height: 20px; height: 20px">&nbsp;</td>
                                </tr>
                                <tr style="">
                                    <td align="center" class="footer__disclaimer" pardot-region="" style="text-align: center; color:#005091;
                    font-family: Arial, Helvetica, sans-serif;
                    font-size: 12px;
                    line-height: 14px;">Vous recevez trop d'emails? Vous pouvez <a href="{{EmailPreferenceCenter_114}}" style="
                                    text-decoration: underline;
                                    color: #005091;
                                    font-family: Arial, Helvetica,
                                      sans-serif;
                                  ">gérer vos préférences ici</a>.</td>
                                </tr>
                                <tr class="space bg-table">
                                    <td style="line-height: 20px; height: 20px">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- DISCLAIMER -->`;

    return disclaimer;
};