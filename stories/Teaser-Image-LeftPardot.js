import './main.css';
export const createTeaserLeftPardot = () => {
  const teaserLeft = `
      <!-- TEASER IMAGE LEFT -->
      <table
        cellpadding="0"
        cellspacing="0"
        align="center"
        border="0"
        width="100%"
        style="max-width: 640px; width: 100%; margin: auto; background-color: #FFFFFF;"
        pardot-repeatable=""
        class="wrapper"
      >
        <tbody>
          <tr>
            <td style="font-size: 0px" class="p-mobile">
              <!--[if mso]>
                <table role="presentation" width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td style="width:390px; padding: 0px;" align="left" valign="top">
              <![endif]-->
              <div
                style="
                  display: inline-block;
                  vertical-align: top;
                  height: 320px;
                  width: 390px;
                "
                class="deviceWidth heightAuto col-lge"
              >
                <img
                  valign="bottom"
                  height="320"
                  pardot-region=""
                  width="390"
                  src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621524679/teaser-left_pzishd.png"
                  style="
                    width: 100%;
                    max-width: 100%;
                    margin: auto;
                    display: block;
                  "
                  class="hidden"
                />
                <!--[if !mso]>-->
                <img
                  src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126549/cow-image-mobile_zaren6.png"
                  width="100%"
                  class="img-mobile"
                  pardot-region=""
                  style="
                  display: none;
                  width: 100% !important;
                  max-width: 100%;
                  margin: auto;
                  "
                />
                <!--<![endif]-->
              </div>
              <!--[if mso]>
                </td>
                <td style="width:250px;" valign="top">
                  <table role="presentation" width="100%" cellpadding="0" cellspacing="0" height="320">
                    <tr>
                      <td style="background: #fdc300; padding: 25px 5px 24px 15px;">
              <![endif]-->
              <div
                style="
                  background: #fdc300;
                  height: 190px;
                  padding: 20px 0;
                  display: inline-block;
                  vertical-align: top;
                  width: 250px;
                  text-align: left;
                  margin: 0 auto;
                "
                class="deviceWidth heightAuto col-sml col-yellow"
              >
                <div style="padding: 0 10px 0 15px">
                  
                  <h4 pardot-region=""
                    style="
                      font-size: 21px !important;
                      color: #1e355c;
                      font-weight: bold;
                      margin: 0 0 10px;
                      line-height: 24px;
                      font-family: Arial, Helvetica, sans-serif;
                    "
                  >
                    Pellentesque vel enim quis enim interdum viverra
                  </h4>
                  <p pardot-region=""
                    style="
                      font-size: 14px !important;
                      color: #1e355c;
                      margin: 0 0 20px;
                      line-height: 19px;
                      font-family: Arial, Helvetica, sans-serif;
                    "
                  >
                   Praesent ultrices luctus cursus. Fusce a nunc quis arcu sollicitudin dapibus. Sed in ante sit amet massa varius fringilla…
                  </p>

                  <table
                    role="presentation"
                    border="0"
                    cellpadding="0"
                    cellspacing="0"
                    align="left"
                    class="link"
                  >
                    <tbody>
                      <tr>
                        <td>
                          <a pardot-region=""
                            href="https://www.southpole.com/"
                            style="
                              font-size: 16px !important;
                              font-weight: bold;
                              color: #1e355c;
                              text-decoration: none;
                              font-family: Arial, Helvetica, sans-serif;
                            "
                          >
                            Learn more
                          </a>
                        </td>
                        <td style="padding-left: 8px">
                          <a href="#">
                            <img
                              heigh='8"'
                              width="5"
                              src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1622126630/arrow-right-2_uuxdqw.png"
                              alt=""
                          /></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!--[if mso]>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              <![endif]-->
            </td>

          </tr>
        </tbody>
      </table>
      <!-- END TEASER IMAGE LEFT -->
      <!-- space -->
      <table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center" pardot-repeatable="">
        <tr>
            <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
        </tr>
      </table>
      <!-- END space -->
  `;

  return teaserLeft;
};
