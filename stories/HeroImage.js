import './main.css';

export const createHeroImage = () => {
	const header = `
  <table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       class="header"
       align="center">
    <tr>
        <td>
            <!-- HEADER-HERO : BEGIN -->
            <table
				cellpadding="0"
				cellspacing="0"
				align="center"
				border="0"
				width="100%"
				style="
					max-width: 640px;
					width: 100%;
					margin: auto;
					text-align: center;
				"
				class="deviceWidth"
            >
                <tbody>
					<tr>
						<td class="header__title" style="padding-top: 40px;">
							<h1
							style="
								margin: 0;
								font-size: 40px;
								line-height: 42px;
								color: #005091;
								font-weight: bold;
								font-family: Arial, Helvetica,
								sans-serif;
								"
							>
							Register for the Webinar
							</h1>
						</td>
					</tr>
					<tr>
						<td class="header__text"
						style="text-align: center;
						font-size: 22px;
						line-height: 28px;
						color: #005091;
						font-family: Arial, Helvetica,
						sans-serif;"
						>
							<div style="display: inline-block;">
								<p style="margin-bottom: 0; padding-bottom: 5px;">
									Improve your CDP performance in 2021
								</p>
								<!--[if !mso]>-->
								<div style="height: 2px; display: block; background-color: #005091"> </div>
								<!--<![endif]-->
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<table
								cellpadding="0"
								cellspacing="0"
								align="center"
								border="0"
								width="100%"
								
								style="
								max-width: 602px;
								width: 100%;
								margin: auto;
								text-align: center;
								"
								class="deviceWidth"
							>
								<tr>
									<td class="header__image" style="padding-top: 40px;">
									<img
										src="https://res.cloudinary.com/dy1plqxbc/image/upload/v1621857015/logo_dbsewx_d7b0sc.jpg"
										width="100%"
										height="100%"
										alt=""
										border="0"
										style="display: block; height: 100%; width: 100%; object-fit: cover;"
										
									/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<!-- END HERO INFO -->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table> 
<!-- END space -->`;

	return header;
}