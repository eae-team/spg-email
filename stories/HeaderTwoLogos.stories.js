import { createHeaderTwoLogos } from './HeaderTwoLogos';

export default {
  title: 'Blocks/Header/Two Logos',
};

const Template = () => createHeaderTwoLogos();

export const HeaderComponent = Template.bind({});

