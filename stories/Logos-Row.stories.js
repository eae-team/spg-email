import { createLogosRow } from './Logos-Row';

export default {
  title: 'Blocks/Logos',
};

const Template = () => createLogosRow();

export const LogosRowComponent = Template.bind({});