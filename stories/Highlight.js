import './main.css';

export const createHighlight = () => {
    const highlight = `
<!-- QUOTE BLOCK-->
<table cellpadding="20"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background-color: #005091"
       class=""
       align="center">

    <tr>
        <td align="center">
            <h2
                style="
                color: #FFFFFF;
                font-size: 18px;
                line-height: 24px;
                font-weight: bold;
                margin: 0;
                font-family: Arial, Helvetica, sans-serif;
                text-align: center;
                "
     
            >
            The previous version of this media release erroneously used the term carbon grazing to refer to the breed cattle heard management method of carbon abatement.
            </h2>
            <br>
            <!--[if mso]>
            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:w="urn:schemas-microsoft-com:office:word" href="http://"
                         style="height:40px;v-text-anchor:middle;width:175px;" arcsize="63%" stroke="f"
                         fillcolor="#FFFFFF">
                <w:anchorlock/>
                <center>
            <![endif]-->
            <a href="http://"
               style="background-color:#FFFFFF;
               border-radius:25px;color:#005091;
               display:inline-block;font-family:sans-serif;
               font-size:13px;font-weight:bold;line-height:40px;
               text-align:center;text-decoration:none;width:175px;
               -webkit-text-size-adjust:none;">
                    Tell us your Goal &gt;
                </a>
            <!--[if mso]>
            </center>
            </v:roundrect>
            <![endif]-->
        </td>
    </tr>

</table>
<!-- END QUOTE BLOCK-->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table>
<!-- END space -->

`;

    return highlight
}
