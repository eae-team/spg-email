import './main.css';

export const createHeroSpecial = () => {
    const header = `
<!-- HERO INFO -->
<table cellpadding="0"
       cellspacing="0"
       border="0"
       style="max-width: 640px; width: 100%; background: #ffffff"
       class="header"
       align="center">
    <tr>
        <td>
            <!-- HEADER-HERO : BEGIN -->
            <table
                    cellpadding="0"
                    cellspacing="0"
                    align="center"
                    border="0"
                    width="100%"
                    style="
                      max-width: 640px;
                      width: 100%;
                      margin: auto;
                      text-align: center;
                    "
                    class="deviceWidth"
            >
                <tbody>
                <tr>
                    <td
                            background="https://res.cloudinary.com/eae-pt/image/upload/v1626363886/herospecial_y15gva.png"
                            width="640"
                            height="563"
                            valign="top"
                            style="background-repeat: no-repeat; background-size: cover; background-position: bottom;"
                            class="deviceWidth header hero-background"
                    >
                    <!--[if mso]>
    
                            <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 640px; height:563px;" src="https://res.cloudinary.com/eae-pt/image/upload/v1626363886/herospecial_y15gva.png" />                
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 640px; height:563px;">
                            <v:fill  opacity="0%" color="#005091"  />
                            <v:textbox inset="0,0,0,0">
                            <![endif]-->
                        
                        <div>
                            <table
                                    role="presentation"
                                    border="0"
                                    cellpadding="0"
                                    cellspacing="0"
                                    align="left"
                                    width="100%"
                                    style="max-width: 600px; margin: auto"
                            >
                                <tbody>
                                <tr>
                                    <td align="left" valign="top">
                                        <table
                                                role="presentation"
                                                border="0"
                                                cellpadding="0"
                                                cellspacing="0"
                                                align="left"
                                                width="60%"
                                                class="hero-special__content"
                                        >
                                            <tbody>
                                            <tr>
                                                <td
                                                        valign="top"
                                                        style="
                                  text-align: left;
                                  padding: 60px 40px 0;
                             
                                "
                                                        class="header__title"
                                                >
                                                    <h1
                                                            style="
                                    margin: 0;
                                    font-size: 43px;
                                    line-height: 61px;
                                    color: #005091;
                                    font-weight: bold;
                                    font-family: Arial, Helvetica,
                                      sans-serif;
                                  "
                                                    >
                                                        Happy Holidays!
                                                    </h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                        valign="top"
                                                        style="
                                  text-align: center;
                                  padding: 5px 0 77px 40px;
                                  font-size: 22px;
                                  line-height: 28px;
                                  color: #005091;
                                  font-family: Arial, Helvetica,
                                    sans-serif;
                                  text-align: left;
                                "
                                                        class="header__text"
                                                >
                                                    <p class="first-paragraph" style="margin: 0;">
                                                        From everyone at South Pole,
We wish you good health and all the best for 2021.
                                                    </p><br>
                                                    <p class="second-paragraph" style="margin: 0;
                                                               font-size: 16px">
                                                            
                                                        Read on to find out how we traded  in red for green this holiday season!
                                                    </p><br>
                                                    <!--[if !mso]>-->
                                                                <a
                                                                    href="http://"
                                                                    style="background-color:#FFFFFF;
                                                    border-radius:25px;color:#005091;
                                                    display:inline-block;font-family:sans-serif;
                                                    font-size:13px;font-weight:bold;line-height:40px;
                                                    text-align:center;text-decoration:none;width:175px;
                                                    -webkit-text-size-adjust:none;"
                                                                >
                                                                Click here &gt;
                                                                </a>
                                                    <!--<![endif]-->
                                                    
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <!--[if mso]>
                            </v:textbox>
                            </v:fill>
                            </v:rect>
                            </v:image>
                             <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="https://www.google.com.au" style="height:40px;width:175px;position:absolute;top:380px;left:40px;" arcsize="63%" fillcolor="#FFFFFF" stroke="false">
                            <center style="color:#005091;-webkit-text-size-adjust:none;font-family:sans-serif;font-size:13px;font-weight:bold;">Click here &gt;</center>
                            </v:roundrect>
                            <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
</table>
<!-- END HERO INFO -->

<!-- space -->
<table class="bg-table" cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td style="line-height: 20px; font-size: 20px">&nbsp;</td>
    </tr>
</table> 
<!-- END space -->
`;

    return header;
};
