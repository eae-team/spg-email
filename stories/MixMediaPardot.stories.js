import { createMixMediaPardot} from './MixMediaPardot';

export default {
  title: 'Pardot Tags/MixMedia',
};

const Template = () => createMixMediaPardot();

export const MixMediaComponent = Template.bind({});
