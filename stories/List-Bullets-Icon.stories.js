import {createListBulletsIcon} from './List-Bullets-Icon';

export default {
  title: 'Blocks/Bullets/Icon',
};

const Template = () => createListBulletsIcon();

export const ListBulletsComponent = Template.bind({});
