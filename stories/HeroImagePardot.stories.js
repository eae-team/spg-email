import {createHeroImagePardot} from './HeroImagePardot';

export default {
  title: 'Pardot Tags/Hero/Image',
};

const Template = () => createHeroImagePardot();

export const HeroComponent = Template.bind({});

