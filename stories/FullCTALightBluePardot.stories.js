import {createFullCTA, createFullCTALightBluePardot} from './FullCTALightBluePardot';

export default {
  title: 'Pardot Tags/Full CTA/Light Blue',
};

const Template = () => createFullCTALightBluePardot();

export const FullCTAComponent = Template.bind({});
